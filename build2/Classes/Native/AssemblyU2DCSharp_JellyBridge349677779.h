﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellyBridge
struct  JellyBridge_t349677779  : public MonoBehaviour_t1158329972
{
public:
	// System.Single JellyBridge::fixedRowFraction
	float ___fixedRowFraction_2;
	// System.Boolean JellyBridge::isFirstUpdate
	bool ___isFirstUpdate_3;

public:
	inline static int32_t get_offset_of_fixedRowFraction_2() { return static_cast<int32_t>(offsetof(JellyBridge_t349677779, ___fixedRowFraction_2)); }
	inline float get_fixedRowFraction_2() const { return ___fixedRowFraction_2; }
	inline float* get_address_of_fixedRowFraction_2() { return &___fixedRowFraction_2; }
	inline void set_fixedRowFraction_2(float value)
	{
		___fixedRowFraction_2 = value;
	}

	inline static int32_t get_offset_of_isFirstUpdate_3() { return static_cast<int32_t>(offsetof(JellyBridge_t349677779, ___isFirstUpdate_3)); }
	inline bool get_isFirstUpdate_3() const { return ___isFirstUpdate_3; }
	inline bool* get_address_of_isFirstUpdate_3() { return &___isFirstUpdate_3; }
	inline void set_isFirstUpdate_3(bool value)
	{
		___isFirstUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
