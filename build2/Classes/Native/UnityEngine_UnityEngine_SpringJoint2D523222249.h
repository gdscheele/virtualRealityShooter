﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_AnchoredJoint2D4245992618.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpringJoint2D
struct  SpringJoint2D_t523222249  : public AnchoredJoint2D_t4245992618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
