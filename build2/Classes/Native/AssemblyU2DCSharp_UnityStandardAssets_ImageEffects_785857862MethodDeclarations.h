﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.EdgeDetection
struct EdgeDetection_t785857862;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern "C"  void EdgeDetection__ctor_m1879835487 (EdgeDetection_t785857862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern "C"  bool EdgeDetection_CheckResources_m3648649882 (EdgeDetection_t785857862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern "C"  void EdgeDetection_Start_m2281010411 (EdgeDetection_t785857862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern "C"  void EdgeDetection_SetCameraFlag_m633933016 (EdgeDetection_t785857862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern "C"  void EdgeDetection_OnEnable_m3754650379 (EdgeDetection_t785857862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void EdgeDetection_OnRenderImage_m847594803 (EdgeDetection_t785857862 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
