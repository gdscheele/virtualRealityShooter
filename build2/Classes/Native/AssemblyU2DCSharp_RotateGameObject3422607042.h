﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateGameObject
struct  RotateGameObject_t3422607042  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RotateGameObject::rot_speed_x
	float ___rot_speed_x_2;
	// System.Single RotateGameObject::rot_speed_y
	float ___rot_speed_y_3;
	// System.Single RotateGameObject::rot_speed_z
	float ___rot_speed_z_4;
	// System.Boolean RotateGameObject::local
	bool ___local_5;

public:
	inline static int32_t get_offset_of_rot_speed_x_2() { return static_cast<int32_t>(offsetof(RotateGameObject_t3422607042, ___rot_speed_x_2)); }
	inline float get_rot_speed_x_2() const { return ___rot_speed_x_2; }
	inline float* get_address_of_rot_speed_x_2() { return &___rot_speed_x_2; }
	inline void set_rot_speed_x_2(float value)
	{
		___rot_speed_x_2 = value;
	}

	inline static int32_t get_offset_of_rot_speed_y_3() { return static_cast<int32_t>(offsetof(RotateGameObject_t3422607042, ___rot_speed_y_3)); }
	inline float get_rot_speed_y_3() const { return ___rot_speed_y_3; }
	inline float* get_address_of_rot_speed_y_3() { return &___rot_speed_y_3; }
	inline void set_rot_speed_y_3(float value)
	{
		___rot_speed_y_3 = value;
	}

	inline static int32_t get_offset_of_rot_speed_z_4() { return static_cast<int32_t>(offsetof(RotateGameObject_t3422607042, ___rot_speed_z_4)); }
	inline float get_rot_speed_z_4() const { return ___rot_speed_z_4; }
	inline float* get_address_of_rot_speed_z_4() { return &___rot_speed_z_4; }
	inline void set_rot_speed_z_4(float value)
	{
		___rot_speed_z_4 = value;
	}

	inline static int32_t get_offset_of_local_5() { return static_cast<int32_t>(offsetof(RotateGameObject_t3422607042, ___local_5)); }
	inline bool get_local_5() const { return ___local_5; }
	inline bool* get_address_of_local_5() { return &___local_5; }
	inline void set_local_5(bool value)
	{
		___local_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
