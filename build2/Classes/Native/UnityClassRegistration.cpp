template <typename T> void RegisterClass();
template <typename T> void RegisterStrippedTypeInfo(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_CloudWebServices();
	RegisterModule_CloudWebServices();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_UnityAnalytics();
	RegisterModule_UnityAnalytics();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

}

class EditorExtension; template <> void RegisterClass<EditorExtension>();
namespace Unity { class Component; } template <> void RegisterClass<Unity::Component>();
class Behaviour; template <> void RegisterClass<Behaviour>();
class Animation; template <> void RegisterClass<Animation>();
class AudioBehaviour; template <> void RegisterClass<AudioBehaviour>();
class AudioListener; template <> void RegisterClass<AudioListener>();
class AudioSource; template <> void RegisterClass<AudioSource>();
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterClass<Camera>();
namespace UI { class Canvas; } template <> void RegisterClass<UI::Canvas>();
namespace UI { class CanvasGroup; } template <> void RegisterClass<UI::CanvasGroup>();
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterClass<Collider2D>();
class BoxCollider2D; 
class CapsuleCollider2D; 
class CircleCollider2D; template <> void RegisterClass<CircleCollider2D>();
class EdgeCollider2D; 
class PolygonCollider2D; 
class ConstantForce; 
class DirectorPlayer; template <> void RegisterClass<DirectorPlayer>();
class Animator; template <> void RegisterClass<Animator>();
class Effector2D; 
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; 
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; template <> void RegisterClass<FlareLayer>();
class GUIElement; template <> void RegisterClass<GUIElement>();
namespace TextRenderingPrivate { class GUIText; } template <> void RegisterClass<TextRenderingPrivate::GUIText>();
class GUITexture; 
class GUILayer; template <> void RegisterClass<GUILayer>();
class Halo; 
class HaloLayer; 
class Joint2D; template <> void RegisterClass<Joint2D>();
class AnchoredJoint2D; template <> void RegisterClass<AnchoredJoint2D>();
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; 
class SliderJoint2D; 
class SpringJoint2D; template <> void RegisterClass<SpringJoint2D>();
class WheelJoint2D; 
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterClass<Light>();
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterClass<MonoBehaviour>();
class NavMeshAgent; 
class NavMeshObstacle; 
class NetworkView; template <> void RegisterClass<NetworkView>();
class OffMeshLink; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class Projector; 
class ReflectionProbe; 
class Skybox; template <> void RegisterClass<Skybox>();
class Terrain; 
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterClass<UI::CanvasRenderer>();
class Collider; template <> void RegisterClass<Collider>();
class BoxCollider; template <> void RegisterClass<BoxCollider>();
class CapsuleCollider; template <> void RegisterClass<CapsuleCollider>();
class CharacterController; template <> void RegisterClass<CharacterController>();
class MeshCollider; template <> void RegisterClass<MeshCollider>();
class SphereCollider; template <> void RegisterClass<SphereCollider>();
class TerrainCollider; 
class WheelCollider; 
namespace Unity { class Joint; } template <> void RegisterClass<Unity::Joint>();
namespace Unity { class CharacterJoint; } 
namespace Unity { class ConfigurableJoint; } 
namespace Unity { class FixedJoint; } 
namespace Unity { class HingeJoint; } 
namespace Unity { class SpringJoint; } template <> void RegisterClass<Unity::SpringJoint>();
class LODGroup; 
class MeshFilter; template <> void RegisterClass<MeshFilter>();
class OcclusionArea; 
class OcclusionPortal; 
class ParticleAnimator; 
class ParticleEmitter; 
class EllipsoidParticleEmitter; 
class MeshParticleEmitter; 
class ParticleSystem; template <> void RegisterClass<ParticleSystem>();
class Renderer; template <> void RegisterClass<Renderer>();
class BillboardRenderer; 
class LineRenderer; 
class MeshRenderer; template <> void RegisterClass<MeshRenderer>();
class ParticleRenderer; 
class ParticleSystemRenderer; template <> void RegisterClass<ParticleSystemRenderer>();
class SkinnedMeshRenderer; 
class SpriteRenderer; template <> void RegisterClass<SpriteRenderer>();
class TrailRenderer; 
class Rigidbody; template <> void RegisterClass<Rigidbody>();
class Rigidbody2D; template <> void RegisterClass<Rigidbody2D>();
namespace TextRenderingPrivate { class TextMesh; } 
class Transform; template <> void RegisterClass<Transform>();
namespace UI { class RectTransform; } template <> void RegisterClass<UI::RectTransform>();
class Tree; 
class WorldAnchor; 
class WorldParticleCollider; 
class GameObject; template <> void RegisterClass<GameObject>();
class NamedObject; template <> void RegisterClass<NamedObject>();
class AssetBundle; 
class AssetBundleManifest; 
class AudioMixer; template <> void RegisterClass<AudioMixer>();
class AudioMixerController; 
class AudioMixerGroup; template <> void RegisterClass<AudioMixerGroup>();
class AudioMixerGroupController; 
class AudioMixerSnapshot; template <> void RegisterClass<AudioMixerSnapshot>();
class AudioMixerSnapshotController; 
class Avatar; 
class BillboardAsset; 
class ComputeShader; 
class Flare; 
namespace TextRendering { class Font; } template <> void RegisterClass<TextRendering::Font>();
class LightProbes; template <> void RegisterClass<LightProbes>();
class Material; template <> void RegisterClass<Material>();
class ProceduralMaterial; 
class Mesh; template <> void RegisterClass<Mesh>();
class Motion; 
class AnimationClip; 
class NavMeshData; 
class OcclusionCullingData; 
class PhysicMaterial; template <> void RegisterClass<PhysicMaterial>();
class PhysicsMaterial2D; template <> void RegisterClass<PhysicsMaterial2D>();
class PreloadData; template <> void RegisterClass<PreloadData>();
class RuntimeAnimatorController; template <> void RegisterClass<RuntimeAnimatorController>();
class AnimatorController; 
class AnimatorOverrideController; 
class SampleClip; template <> void RegisterClass<SampleClip>();
class AudioClip; template <> void RegisterClass<AudioClip>();
class Shader; template <> void RegisterClass<Shader>();
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterClass<Sprite>();
class SubstanceArchive; 
class TerrainData; 
class TextAsset; template <> void RegisterClass<TextAsset>();
class CGProgram; 
class MonoScript; template <> void RegisterClass<MonoScript>();
class Texture; template <> void RegisterClass<Texture>();
class BaseVideoTexture; 
class MovieTexture; 
class CubemapArray; 
class ProceduralTexture; 
class RenderTexture; template <> void RegisterClass<RenderTexture>();
class SparseTexture; 
class Texture2D; template <> void RegisterClass<Texture2D>();
class Cubemap; template <> void RegisterClass<Cubemap>();
class Texture2DArray; template <> void RegisterClass<Texture2DArray>();
class Texture3D; template <> void RegisterClass<Texture3D>();
class WebCamTexture; 
class GameManager; template <> void RegisterClass<GameManager>();
class GlobalGameManager; template <> void RegisterClass<GlobalGameManager>();
class AudioManager; template <> void RegisterClass<AudioManager>();
class BuildSettings; template <> void RegisterClass<BuildSettings>();
class CloudWebServicesManager; template <> void RegisterClass<CloudWebServicesManager>();
class CrashReportManager; 
class DelayedCallManager; template <> void RegisterClass<DelayedCallManager>();
class GraphicsSettings; template <> void RegisterClass<GraphicsSettings>();
class InputManager; template <> void RegisterClass<InputManager>();
class MasterServerInterface; template <> void RegisterClass<MasterServerInterface>();
class MonoManager; template <> void RegisterClass<MonoManager>();
class NavMeshProjectSettings; 
class NetworkManager; template <> void RegisterClass<NetworkManager>();
class Physics2DSettings; template <> void RegisterClass<Physics2DSettings>();
class PhysicsManager; template <> void RegisterClass<PhysicsManager>();
class PlayerSettings; template <> void RegisterClass<PlayerSettings>();
class QualitySettings; template <> void RegisterClass<QualitySettings>();
class ResourceManager; template <> void RegisterClass<ResourceManager>();
class RuntimeInitializeOnLoadManager; template <> void RegisterClass<RuntimeInitializeOnLoadManager>();
class ScriptMapper; template <> void RegisterClass<ScriptMapper>();
class TagManager; template <> void RegisterClass<TagManager>();
class TimeManager; template <> void RegisterClass<TimeManager>();
class UnityAdsManager; 
class UnityAnalyticsManager; template <> void RegisterClass<UnityAnalyticsManager>();
class UnityConnectSettings; template <> void RegisterClass<UnityConnectSettings>();
class LevelGameManager; template <> void RegisterClass<LevelGameManager>();
class LightmapSettings; template <> void RegisterClass<LightmapSettings>();
class NavMeshSettings; 
class OcclusionCullingSettings; 
class RenderSettings; template <> void RegisterClass<RenderSettings>();
class NScreenBridge; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 94 non stripped classes
	//0. Behaviour
	RegisterClass<Behaviour>();
	//1. Unity::Component
	RegisterClass<Unity::Component>();
	//2. EditorExtension
	RegisterClass<EditorExtension>();
	//3. Camera
	RegisterClass<Camera>();
	//4. GameObject
	RegisterClass<GameObject>();
	//5. RenderSettings
	RegisterClass<RenderSettings>();
	//6. LevelGameManager
	RegisterClass<LevelGameManager>();
	//7. GameManager
	RegisterClass<GameManager>();
	//8. QualitySettings
	RegisterClass<QualitySettings>();
	//9. GlobalGameManager
	RegisterClass<GlobalGameManager>();
	//10. MeshFilter
	RegisterClass<MeshFilter>();
	//11. Renderer
	RegisterClass<Renderer>();
	//12. Skybox
	RegisterClass<Skybox>();
	//13. GUILayer
	RegisterClass<GUILayer>();
	//14. Light
	RegisterClass<Light>();
	//15. Mesh
	RegisterClass<Mesh>();
	//16. NamedObject
	RegisterClass<NamedObject>();
	//17. MonoBehaviour
	RegisterClass<MonoBehaviour>();
	//18. NetworkView
	RegisterClass<NetworkView>();
	//19. UI::RectTransform
	RegisterClass<UI::RectTransform>();
	//20. Transform
	RegisterClass<Transform>();
	//21. Shader
	RegisterClass<Shader>();
	//22. Material
	RegisterClass<Material>();
	//23. Sprite
	RegisterClass<Sprite>();
	//24. SpriteRenderer
	RegisterClass<SpriteRenderer>();
	//25. Texture
	RegisterClass<Texture>();
	//26. Texture2D
	RegisterClass<Texture2D>();
	//27. Texture3D
	RegisterClass<Texture3D>();
	//28. RenderTexture
	RegisterClass<RenderTexture>();
	//29. ParticleSystem
	RegisterClass<ParticleSystem>();
	//30. Rigidbody
	RegisterClass<Rigidbody>();
	//31. Unity::Joint
	RegisterClass<Unity::Joint>();
	//32. Unity::SpringJoint
	RegisterClass<Unity::SpringJoint>();
	//33. Collider
	RegisterClass<Collider>();
	//34. SphereCollider
	RegisterClass<SphereCollider>();
	//35. CapsuleCollider
	RegisterClass<CapsuleCollider>();
	//36. CharacterController
	RegisterClass<CharacterController>();
	//37. Rigidbody2D
	RegisterClass<Rigidbody2D>();
	//38. Collider2D
	RegisterClass<Collider2D>();
	//39. CircleCollider2D
	RegisterClass<CircleCollider2D>();
	//40. Joint2D
	RegisterClass<Joint2D>();
	//41. AnchoredJoint2D
	RegisterClass<AnchoredJoint2D>();
	//42. SpringJoint2D
	RegisterClass<SpringJoint2D>();
	//43. AudioClip
	RegisterClass<AudioClip>();
	//44. SampleClip
	RegisterClass<SampleClip>();
	//45. AudioSource
	RegisterClass<AudioSource>();
	//46. AudioBehaviour
	RegisterClass<AudioBehaviour>();
	//47. AudioMixer
	RegisterClass<AudioMixer>();
	//48. Animation
	RegisterClass<Animation>();
	//49. Animator
	RegisterClass<Animator>();
	//50. DirectorPlayer
	RegisterClass<DirectorPlayer>();
	//51. TextRenderingPrivate::GUIText
	RegisterClass<TextRenderingPrivate::GUIText>();
	//52. GUIElement
	RegisterClass<GUIElement>();
	//53. TextRendering::Font
	RegisterClass<TextRendering::Font>();
	//54. UI::Canvas
	RegisterClass<UI::Canvas>();
	//55. UI::CanvasGroup
	RegisterClass<UI::CanvasGroup>();
	//56. UI::CanvasRenderer
	RegisterClass<UI::CanvasRenderer>();
	//57. AudioMixerGroup
	RegisterClass<AudioMixerGroup>();
	//58. MeshRenderer
	RegisterClass<MeshRenderer>();
	//59. PhysicMaterial
	RegisterClass<PhysicMaterial>();
	//60. PhysicsMaterial2D
	RegisterClass<PhysicsMaterial2D>();
	//61. RuntimeAnimatorController
	RegisterClass<RuntimeAnimatorController>();
	//62. PreloadData
	RegisterClass<PreloadData>();
	//63. Cubemap
	RegisterClass<Cubemap>();
	//64. Texture2DArray
	RegisterClass<Texture2DArray>();
	//65. TimeManager
	RegisterClass<TimeManager>();
	//66. AudioManager
	RegisterClass<AudioManager>();
	//67. InputManager
	RegisterClass<InputManager>();
	//68. Physics2DSettings
	RegisterClass<Physics2DSettings>();
	//69. GraphicsSettings
	RegisterClass<GraphicsSettings>();
	//70. TextAsset
	RegisterClass<TextAsset>();
	//71. PhysicsManager
	RegisterClass<PhysicsManager>();
	//72. TagManager
	RegisterClass<TagManager>();
	//73. ScriptMapper
	RegisterClass<ScriptMapper>();
	//74. DelayedCallManager
	RegisterClass<DelayedCallManager>();
	//75. MonoScript
	RegisterClass<MonoScript>();
	//76. MonoManager
	RegisterClass<MonoManager>();
	//77. PlayerSettings
	RegisterClass<PlayerSettings>();
	//78. BuildSettings
	RegisterClass<BuildSettings>();
	//79. ResourceManager
	RegisterClass<ResourceManager>();
	//80. NetworkManager
	RegisterClass<NetworkManager>();
	//81. MasterServerInterface
	RegisterClass<MasterServerInterface>();
	//82. RuntimeInitializeOnLoadManager
	RegisterClass<RuntimeInitializeOnLoadManager>();
	//83. CloudWebServicesManager
	RegisterClass<CloudWebServicesManager>();
	//84. UnityAnalyticsManager
	RegisterClass<UnityAnalyticsManager>();
	//85. UnityConnectSettings
	RegisterClass<UnityConnectSettings>();
	//86. MeshCollider
	RegisterClass<MeshCollider>();
	//87. BoxCollider
	RegisterClass<BoxCollider>();
	//88. AudioListener
	RegisterClass<AudioListener>();
	//89. FlareLayer
	RegisterClass<FlareLayer>();
	//90. LightmapSettings
	RegisterClass<LightmapSettings>();
	//91. ParticleSystemRenderer
	RegisterClass<ParticleSystemRenderer>();
	//92. LightProbes
	RegisterClass<LightProbes>();
	//93. AudioMixerSnapshot
	RegisterClass<AudioMixerSnapshot>();

}
