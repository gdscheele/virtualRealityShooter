﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3604979259MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3297592950(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3818951736 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3981716339_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m4155558082(__this, ___arg10, method) ((  bool (*) (Func_2_t3818951736 *, KeyValuePair_2_t1251105099 , const MethodInfo*))Func_2_Invoke_m2824785861_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m87495915(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3818951736 *, KeyValuePair_2_t1251105099 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1864708834_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3808155104(__this, ___result0, method) ((  bool (*) (Func_2_t3818951736 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1795741251_gshared)(__this, ___result0, method)
