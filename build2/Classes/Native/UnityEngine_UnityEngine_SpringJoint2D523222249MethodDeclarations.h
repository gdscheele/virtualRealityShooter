﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpringJoint2D
struct SpringJoint2D_t523222249;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SpringJoint2D::set_autoConfigureDistance(System.Boolean)
extern "C"  void SpringJoint2D_set_autoConfigureDistance_m1818807762 (SpringJoint2D_t523222249 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_distance(System.Single)
extern "C"  void SpringJoint2D_set_distance_m4153495635 (SpringJoint2D_t523222249 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_dampingRatio(System.Single)
extern "C"  void SpringJoint2D_set_dampingRatio_m1938211045 (SpringJoint2D_t523222249 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SpringJoint2D::get_frequency()
extern "C"  float SpringJoint2D_get_frequency_m3679103859 (SpringJoint2D_t523222249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpringJoint2D::set_frequency(System.Single)
extern "C"  void SpringJoint2D_set_frequency_m1093855900 (SpringJoint2D_t523222249 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
