﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3465217523.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3901407784.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2916142869.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1836015611.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4146040027.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3401316463.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects_430511954.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects2215595694.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1171761296.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3310062628.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1008153775.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1046072227.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects1381705816.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3322560050.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects3949418959.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_ImageEffects4170634026.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Acti2794485791.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Activ639433180.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Auto1108113546.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Auto4021787953.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Auto3608854452.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Auto2592441618.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_AutoM245510835.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Came2688848816.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Curv2107922160.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Drag2127994057.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Drag4075247181.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Dynam859033236.h"
#include "AssemblyU2DCSharp_EventSystemChecker3549249260.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_FOVK1823436477.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_FOVK1277509062.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_FOVK1597325334.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_FPSCo584591591.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Foll2968437806.h"
#include "AssemblyU2DCSharp_ForcedReset935499500.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Lerp3525149852.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Lerp1181024807.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Obje2090656575.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Obje2926400505.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Parti190286178.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Part3769115865.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Platf935951820.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Plat1911586150.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Simp1317702990.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Simple32383032.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Smoo2548964113.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Time2871423837.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Timed769078069.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Timed420114983.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Time1135469853.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_TimedO56702769.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Timed707185157.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Time1764217217.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Time3133065407.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2375210762.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2985503331.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Waypo318924311.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2206407592.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp1659392090.h"
#include "AssemblyU2DCSharp_MathUtils4163796929.h"
#include "AssemblyU2DCSharp_eOrientationMode1001868672.h"
#include "AssemblyU2DCSharp_SplineController2578011787.h"
#include "AssemblyU2DCSharp_eEndPointsMode2085807520.h"
#include "AssemblyU2DCSharp_eWrapMode2466495092.h"
#include "AssemblyU2DCSharp_OnEndCallback2203472397.h"
#include "AssemblyU2DCSharp_SplineInterpolator4279526764.h"
#include "AssemblyU2DCSharp_SplineInterpolator_SplineNode2139886252.h"
#include "AssemblyU2DCSharp_StartGame415325182.h"
#include "AssemblyU2DCSharp_pointsScript3201420336.h"
#include "AssemblyU2DCSharp_shootScript955866588.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (SSAOSamples_t3465217523)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[4] = 
{
	SSAOSamples_t3465217523::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (SepiaTone_t3901407784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (SunShafts_t2916142869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[14] = 
{
	SunShafts_t2916142869::get_offset_of_resolution_5(),
	SunShafts_t2916142869::get_offset_of_screenBlendMode_6(),
	SunShafts_t2916142869::get_offset_of_sunTransform_7(),
	SunShafts_t2916142869::get_offset_of_radialBlurIterations_8(),
	SunShafts_t2916142869::get_offset_of_sunColor_9(),
	SunShafts_t2916142869::get_offset_of_sunThreshold_10(),
	SunShafts_t2916142869::get_offset_of_sunShaftBlurRadius_11(),
	SunShafts_t2916142869::get_offset_of_sunShaftIntensity_12(),
	SunShafts_t2916142869::get_offset_of_maxRadius_13(),
	SunShafts_t2916142869::get_offset_of_useDepthTexture_14(),
	SunShafts_t2916142869::get_offset_of_sunShaftsShader_15(),
	SunShafts_t2916142869::get_offset_of_sunShaftsMaterial_16(),
	SunShafts_t2916142869::get_offset_of_simpleClearShader_17(),
	SunShafts_t2916142869::get_offset_of_simpleClearMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (SunShaftsResolution_t1836015611)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2103[4] = 
{
	SunShaftsResolution_t1836015611::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (ShaftsScreenBlendMode_t4146040027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2104[3] = 
{
	ShaftsScreenBlendMode_t4146040027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TiltShift_t3401316463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[7] = 
{
	TiltShift_t3401316463::get_offset_of_mode_5(),
	TiltShift_t3401316463::get_offset_of_quality_6(),
	TiltShift_t3401316463::get_offset_of_blurArea_7(),
	TiltShift_t3401316463::get_offset_of_maxBlurSize_8(),
	TiltShift_t3401316463::get_offset_of_downsample_9(),
	TiltShift_t3401316463::get_offset_of_tiltShiftShader_10(),
	TiltShift_t3401316463::get_offset_of_tiltShiftMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (TiltShiftMode_t430511954)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2106[3] = 
{
	TiltShiftMode_t430511954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (TiltShiftQuality_t2215595694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2107[4] = 
{
	TiltShiftQuality_t2215595694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (Tonemapping_t1171761296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[13] = 
{
	Tonemapping_t1171761296::get_offset_of_type_5(),
	Tonemapping_t1171761296::get_offset_of_adaptiveTextureSize_6(),
	Tonemapping_t1171761296::get_offset_of_remapCurve_7(),
	Tonemapping_t1171761296::get_offset_of_curveTex_8(),
	Tonemapping_t1171761296::get_offset_of_exposureAdjustment_9(),
	Tonemapping_t1171761296::get_offset_of_middleGrey_10(),
	Tonemapping_t1171761296::get_offset_of_white_11(),
	Tonemapping_t1171761296::get_offset_of_adaptionSpeed_12(),
	Tonemapping_t1171761296::get_offset_of_tonemapper_13(),
	Tonemapping_t1171761296::get_offset_of_validRenderTextureFormat_14(),
	Tonemapping_t1171761296::get_offset_of_tonemapMaterial_15(),
	Tonemapping_t1171761296::get_offset_of_rt_16(),
	Tonemapping_t1171761296::get_offset_of_rtFormat_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (TonemapperType_t3310062628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[8] = 
{
	TonemapperType_t3310062628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (AdaptiveTexSize_t1008153775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[8] = 
{
	AdaptiveTexSize_t1008153775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Triangles_t1046072227), -1, sizeof(Triangles_t1046072227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2111[2] = 
{
	Triangles_t1046072227_StaticFields::get_offset_of_meshes_0(),
	Triangles_t1046072227_StaticFields::get_offset_of_currentTris_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (Twirl_t1381705816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	Twirl_t1381705816::get_offset_of_radius_4(),
	Twirl_t1381705816::get_offset_of_angle_5(),
	Twirl_t1381705816::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (VignetteAndChromaticAberration_t3322560050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[14] = 
{
	VignetteAndChromaticAberration_t3322560050::get_offset_of_mode_5(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_intensity_6(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromaticAberration_7(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_axialAberration_8(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blur_9(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurSpread_10(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_luminanceDependency_11(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_blurDistance_12(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_vignetteShader_13(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_separableBlurShader_14(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_chromAberrationShader_15(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_VignetteMaterial_16(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_SeparableBlurMaterial_17(),
	VignetteAndChromaticAberration_t3322560050::get_offset_of_m_ChromAberrationMaterial_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (AberrationMode_t3949418959)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2114[3] = 
{
	AberrationMode_t3949418959::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (Vortex_t4170634026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[3] = 
{
	Vortex_t4170634026::get_offset_of_radius_4(),
	Vortex_t4170634026::get_offset_of_angle_5(),
	Vortex_t4170634026::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (ActivateTrigger_t2794485791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[5] = 
{
	ActivateTrigger_t2794485791::get_offset_of_action_2(),
	ActivateTrigger_t2794485791::get_offset_of_target_3(),
	ActivateTrigger_t2794485791::get_offset_of_source_4(),
	ActivateTrigger_t2794485791::get_offset_of_triggerCount_5(),
	ActivateTrigger_t2794485791::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (Mode_t639433180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2117[7] = 
{
	Mode_t639433180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (AutoMobileShaderSwitch_t1108113546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[1] = 
{
	AutoMobileShaderSwitch_t1108113546::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (ReplacementDefinition_t4021787953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[2] = 
{
	ReplacementDefinition_t4021787953::get_offset_of_original_0(),
	ReplacementDefinition_t4021787953::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (ReplacementList_t3608854452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	ReplacementList_t3608854452::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (AutoMoveAndRotate_t2592441618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[4] = 
{
	AutoMoveAndRotate_t2592441618::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t2592441618::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t2592441618::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t2592441618::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (Vector3andSpace_t245510835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[2] = 
{
	Vector3andSpace_t245510835::get_offset_of_value_0(),
	Vector3andSpace_t245510835::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (CameraRefocus_t2688848816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[5] = 
{
	CameraRefocus_t2688848816::get_offset_of_Camera_0(),
	CameraRefocus_t2688848816::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t2688848816::get_offset_of_Parent_2(),
	CameraRefocus_t2688848816::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t2688848816::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (CurveControlledBob_t2107922160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[9] = 
{
	CurveControlledBob_t2107922160::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2107922160::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2107922160::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2107922160::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2107922160::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2107922160::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2107922160::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2107922160::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2107922160::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (DragRigidbody_t2127994057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t2127994057::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (U3CDragObjectU3Ec__Iterator0_t4075247181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[9] = 
{
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3ColdDragU3E__0_0(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3ColdAngularDragU3E__1_1(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3CmainCameraU3E__2_2(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U3CrayU3E__3_3(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_distance_4(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24this_5(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24current_6(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24disposing_7(),
	U3CDragObjectU3Ec__Iterator0_t4075247181::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (DynamicShadowSettings_t859033236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[11] = 
{
	DynamicShadowSettings_t859033236::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t859033236::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t859033236::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t859033236::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t859033236::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t859033236::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t859033236::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t859033236::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t859033236::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t859033236::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t859033236::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (EventSystemChecker_t3549249260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (FOVKick_t1823436477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[6] = 
{
	FOVKick_t1823436477::get_offset_of_Camera_0(),
	FOVKick_t1823436477::get_offset_of_originalFov_1(),
	FOVKick_t1823436477::get_offset_of_FOVIncrease_2(),
	FOVKick_t1823436477::get_offset_of_TimeToIncrease_3(),
	FOVKick_t1823436477::get_offset_of_TimeToDecrease_4(),
	FOVKick_t1823436477::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (U3CFOVKickUpU3Ec__Iterator0_t1277509062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[5] = 
{
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24this_1(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24current_2(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24disposing_3(),
	U3CFOVKickUpU3Ec__Iterator0_t1277509062::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (U3CFOVKickDownU3Ec__Iterator1_t1597325334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[5] = 
{
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U3CtU3E__0_0(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24this_1(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24current_2(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24disposing_3(),
	U3CFOVKickDownU3Ec__Iterator1_t1597325334::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (FPSCounter_t584591591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[6] = 
{
	0,
	FPSCounter_t584591591::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t584591591::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t584591591::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t584591591::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (FollowTarget_t2968437806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[2] = 
{
	FollowTarget_t2968437806::get_offset_of_target_2(),
	FollowTarget_t2968437806::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (ForcedReset_t935499500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (LerpControlledBob_t3525149852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[3] = 
{
	LerpControlledBob_t3525149852::get_offset_of_BobDuration_0(),
	LerpControlledBob_t3525149852::get_offset_of_BobAmount_1(),
	LerpControlledBob_t3525149852::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (U3CDoBobCycleU3Ec__Iterator0_t1181024807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[5] = 
{
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U3CtU3E__0_0(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24this_1(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24current_2(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24disposing_3(),
	U3CDoBobCycleU3Ec__Iterator0_t1181024807::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (ObjectResetter_t2090656575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	ObjectResetter_t2090656575::get_offset_of_originalPosition_2(),
	ObjectResetter_t2090656575::get_offset_of_originalRotation_3(),
	ObjectResetter_t2090656575::get_offset_of_originalStructure_4(),
	ObjectResetter_t2090656575::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (U3CResetCoroutineU3Ec__Iterator0_t2926400505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[7] = 
{
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_delay_0(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24locvar0_1(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24locvar1_2(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24this_3(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24current_4(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24disposing_5(),
	U3CResetCoroutineU3Ec__Iterator0_t2926400505::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (ParticleSystemDestroyer_t190286178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[4] = 
{
	ParticleSystemDestroyer_t190286178::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t190286178::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t190286178::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t190286178::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (U3CStartU3Ec__Iterator0_t3769115865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[10] = 
{
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U3CsystemsU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar1_2(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U3CstopTimeU3E__1_3(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar2_4(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24locvar3_5(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24this_6(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24current_7(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24disposing_8(),
	U3CStartU3Ec__Iterator0_t3769115865::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (PlatformSpecificContent_t935951820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[4] = 
{
	PlatformSpecificContent_t935951820::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t935951820::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t935951820::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t935951820::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (BuildTargetGroup_t1911586150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2142[3] = 
{
	BuildTargetGroup_t1911586150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (SimpleActivatorMenu_t1317702990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[3] = 
{
	SimpleActivatorMenu_t1317702990::get_offset_of_camSwitchButton_2(),
	SimpleActivatorMenu_t1317702990::get_offset_of_objects_3(),
	SimpleActivatorMenu_t1317702990::get_offset_of_m_CurrentActiveObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (SimpleMouseRotator_t32383032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[10] = 
{
	SimpleMouseRotator_t32383032::get_offset_of_rotationRange_2(),
	SimpleMouseRotator_t32383032::get_offset_of_rotationSpeed_3(),
	SimpleMouseRotator_t32383032::get_offset_of_dampingTime_4(),
	SimpleMouseRotator_t32383032::get_offset_of_autoZeroVerticalOnMobile_5(),
	SimpleMouseRotator_t32383032::get_offset_of_autoZeroHorizontalOnMobile_6(),
	SimpleMouseRotator_t32383032::get_offset_of_relative_7(),
	SimpleMouseRotator_t32383032::get_offset_of_m_TargetAngles_8(),
	SimpleMouseRotator_t32383032::get_offset_of_m_FollowAngles_9(),
	SimpleMouseRotator_t32383032::get_offset_of_m_FollowVelocity_10(),
	SimpleMouseRotator_t32383032::get_offset_of_m_OriginalRotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (SmoothFollow_t2548964113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[5] = 
{
	SmoothFollow_t2548964113::get_offset_of_target_2(),
	SmoothFollow_t2548964113::get_offset_of_distance_3(),
	SmoothFollow_t2548964113::get_offset_of_height_4(),
	SmoothFollow_t2548964113::get_offset_of_rotationDamping_5(),
	SmoothFollow_t2548964113::get_offset_of_heightDamping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (TimedObjectActivator_t2871423837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[1] = 
{
	TimedObjectActivator_t2871423837::get_offset_of_entries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (Action_t769078069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2147[6] = 
{
	Action_t769078069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (Entry_t420114983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[3] = 
{
	Entry_t420114983::get_offset_of_target_0(),
	Entry_t420114983::get_offset_of_action_1(),
	Entry_t420114983::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (Entries_t1135469853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	Entries_t1135469853::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (U3CActivateU3Ec__Iterator0_t56702769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[4] = 
{
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_entry_0(),
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_U24current_1(),
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_U24disposing_2(),
	U3CActivateU3Ec__Iterator0_t56702769::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (U3CDeactivateU3Ec__Iterator1_t707185157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[4] = 
{
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_entry_0(),
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_U24current_1(),
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_U24disposing_2(),
	U3CDeactivateU3Ec__Iterator1_t707185157::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (U3CReloadLevelU3Ec__Iterator2_t1764217217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[4] = 
{
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_entry_0(),
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_U24current_1(),
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_U24disposing_2(),
	U3CReloadLevelU3Ec__Iterator2_t1764217217::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (TimedObjectDestructor_t3133065407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	TimedObjectDestructor_t3133065407::get_offset_of_m_TimeOut_2(),
	TimedObjectDestructor_t3133065407::get_offset_of_m_DetachChildren_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (WaypointCircuit_t2375210762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[16] = 
{
	WaypointCircuit_t2375210762::get_offset_of_waypointList_2(),
	WaypointCircuit_t2375210762::get_offset_of_smoothRoute_3(),
	WaypointCircuit_t2375210762::get_offset_of_numPoints_4(),
	WaypointCircuit_t2375210762::get_offset_of_points_5(),
	WaypointCircuit_t2375210762::get_offset_of_distances_6(),
	WaypointCircuit_t2375210762::get_offset_of_editorVisualisationSubsteps_7(),
	WaypointCircuit_t2375210762::get_offset_of_U3CLengthU3Ek__BackingField_8(),
	WaypointCircuit_t2375210762::get_offset_of_p0n_9(),
	WaypointCircuit_t2375210762::get_offset_of_p1n_10(),
	WaypointCircuit_t2375210762::get_offset_of_p2n_11(),
	WaypointCircuit_t2375210762::get_offset_of_p3n_12(),
	WaypointCircuit_t2375210762::get_offset_of_i_13(),
	WaypointCircuit_t2375210762::get_offset_of_P0_14(),
	WaypointCircuit_t2375210762::get_offset_of_P1_15(),
	WaypointCircuit_t2375210762::get_offset_of_P2_16(),
	WaypointCircuit_t2375210762::get_offset_of_P3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (WaypointList_t2985503331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[2] = 
{
	WaypointList_t2985503331::get_offset_of_circuit_0(),
	WaypointList_t2985503331::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (RoutePoint_t318924311)+ sizeof (Il2CppObject), sizeof(RoutePoint_t318924311 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[2] = 
{
	RoutePoint_t318924311::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoutePoint_t318924311::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (WaypointProgressTracker_t2206407592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[15] = 
{
	WaypointProgressTracker_t2206407592::get_offset_of_circuit_2(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForTargetOffset_3(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForTargetFactor_4(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForSpeedOffset_5(),
	WaypointProgressTracker_t2206407592::get_offset_of_lookAheadForSpeedFactor_6(),
	WaypointProgressTracker_t2206407592::get_offset_of_progressStyle_7(),
	WaypointProgressTracker_t2206407592::get_offset_of_pointToPointThreshold_8(),
	WaypointProgressTracker_t2206407592::get_offset_of_U3CtargetPointU3Ek__BackingField_9(),
	WaypointProgressTracker_t2206407592::get_offset_of_U3CspeedPointU3Ek__BackingField_10(),
	WaypointProgressTracker_t2206407592::get_offset_of_U3CprogressPointU3Ek__BackingField_11(),
	WaypointProgressTracker_t2206407592::get_offset_of_target_12(),
	WaypointProgressTracker_t2206407592::get_offset_of_progressDistance_13(),
	WaypointProgressTracker_t2206407592::get_offset_of_progressNum_14(),
	WaypointProgressTracker_t2206407592::get_offset_of_lastPosition_15(),
	WaypointProgressTracker_t2206407592::get_offset_of_speed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (ProgressStyle_t1659392090)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2158[3] = 
{
	ProgressStyle_t1659392090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (MathUtils_t4163796929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (eOrientationMode_t1001868672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[3] = 
{
	eOrientationMode_t1001868672::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (SplineController_t2578011787), -1, sizeof(SplineController_t2578011787_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[11] = 
{
	SplineController_t2578011787::get_offset_of_SplineRoot_2(),
	SplineController_t2578011787::get_offset_of_Duration_3(),
	SplineController_t2578011787::get_offset_of_OrientationMode_4(),
	SplineController_t2578011787::get_offset_of_WrapMode_5(),
	SplineController_t2578011787::get_offset_of_AutoStart_6(),
	SplineController_t2578011787::get_offset_of_AutoClose_7(),
	SplineController_t2578011787::get_offset_of_HideOnExecute_8(),
	SplineController_t2578011787::get_offset_of_mSplineInterp_9(),
	SplineController_t2578011787::get_offset_of_mTransforms_10(),
	SplineController_t2578011787_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	SplineController_t2578011787_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (eEndPointsMode_t2085807520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[4] = 
{
	eEndPointsMode_t2085807520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (eWrapMode_t2466495092)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2163[3] = 
{
	eWrapMode_t2466495092::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (OnEndCallback_t2203472397), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (SplineInterpolator_t4279526764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[7] = 
{
	SplineInterpolator_t4279526764::get_offset_of_mEndPointsMode_2(),
	SplineInterpolator_t4279526764::get_offset_of_mNodes_3(),
	SplineInterpolator_t4279526764::get_offset_of_mState_4(),
	SplineInterpolator_t4279526764::get_offset_of_mRotations_5(),
	SplineInterpolator_t4279526764::get_offset_of_mOnEndCallback_6(),
	SplineInterpolator_t4279526764::get_offset_of_mCurrentTime_7(),
	SplineInterpolator_t4279526764::get_offset_of_mCurrentIdx_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (SplineNode_t2139886252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[4] = 
{
	SplineNode_t2139886252::get_offset_of_Point_0(),
	SplineNode_t2139886252::get_offset_of_Rot_1(),
	SplineNode_t2139886252::get_offset_of_Time_2(),
	SplineNode_t2139886252::get_offset_of_EaseIO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (StartGame_t415325182), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (pointsScript_t3201420336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (shootScript_t955866588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[3] = 
{
	shootScript_t955866588::get_offset_of_Rock1A_2(),
	shootScript_t955866588::get_offset_of_speed_3(),
	shootScript_t955866588::get_offset_of_rocksShot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2170[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D51B2AA051AFFF21EBC28102EA2F57BEF007038AE_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_2(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_3(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_4(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_5(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
