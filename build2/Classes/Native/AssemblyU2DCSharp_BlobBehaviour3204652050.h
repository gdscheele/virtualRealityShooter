﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JellySprite
struct JellySprite_t2775849197;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlobBehaviour
struct  BlobBehaviour_t3204652050  : public MonoBehaviour_t1158329972
{
public:
	// System.Single BlobBehaviour::m_MinBounceTime
	float ___m_MinBounceTime_2;
	// System.Single BlobBehaviour::m_MaxBounceTime
	float ___m_MaxBounceTime_3;
	// System.Single BlobBehaviour::m_MinJumpForce
	float ___m_MinJumpForce_4;
	// System.Single BlobBehaviour::m_MaxJumpForce
	float ___m_MaxJumpForce_5;
	// UnityEngine.Vector2 BlobBehaviour::m_MinJumpVector
	Vector2_t2243707579  ___m_MinJumpVector_6;
	// UnityEngine.Vector2 BlobBehaviour::m_MaxJumpVector
	Vector2_t2243707579  ___m_MaxJumpVector_7;
	// UnityEngine.LayerMask BlobBehaviour::m_GroundLayer
	LayerMask_t3188175821  ___m_GroundLayer_8;
	// JellySprite BlobBehaviour::m_JellySprite
	JellySprite_t2775849197 * ___m_JellySprite_9;
	// System.Single BlobBehaviour::m_BounceTimer
	float ___m_BounceTimer_10;

public:
	inline static int32_t get_offset_of_m_MinBounceTime_2() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_MinBounceTime_2)); }
	inline float get_m_MinBounceTime_2() const { return ___m_MinBounceTime_2; }
	inline float* get_address_of_m_MinBounceTime_2() { return &___m_MinBounceTime_2; }
	inline void set_m_MinBounceTime_2(float value)
	{
		___m_MinBounceTime_2 = value;
	}

	inline static int32_t get_offset_of_m_MaxBounceTime_3() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_MaxBounceTime_3)); }
	inline float get_m_MaxBounceTime_3() const { return ___m_MaxBounceTime_3; }
	inline float* get_address_of_m_MaxBounceTime_3() { return &___m_MaxBounceTime_3; }
	inline void set_m_MaxBounceTime_3(float value)
	{
		___m_MaxBounceTime_3 = value;
	}

	inline static int32_t get_offset_of_m_MinJumpForce_4() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_MinJumpForce_4)); }
	inline float get_m_MinJumpForce_4() const { return ___m_MinJumpForce_4; }
	inline float* get_address_of_m_MinJumpForce_4() { return &___m_MinJumpForce_4; }
	inline void set_m_MinJumpForce_4(float value)
	{
		___m_MinJumpForce_4 = value;
	}

	inline static int32_t get_offset_of_m_MaxJumpForce_5() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_MaxJumpForce_5)); }
	inline float get_m_MaxJumpForce_5() const { return ___m_MaxJumpForce_5; }
	inline float* get_address_of_m_MaxJumpForce_5() { return &___m_MaxJumpForce_5; }
	inline void set_m_MaxJumpForce_5(float value)
	{
		___m_MaxJumpForce_5 = value;
	}

	inline static int32_t get_offset_of_m_MinJumpVector_6() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_MinJumpVector_6)); }
	inline Vector2_t2243707579  get_m_MinJumpVector_6() const { return ___m_MinJumpVector_6; }
	inline Vector2_t2243707579 * get_address_of_m_MinJumpVector_6() { return &___m_MinJumpVector_6; }
	inline void set_m_MinJumpVector_6(Vector2_t2243707579  value)
	{
		___m_MinJumpVector_6 = value;
	}

	inline static int32_t get_offset_of_m_MaxJumpVector_7() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_MaxJumpVector_7)); }
	inline Vector2_t2243707579  get_m_MaxJumpVector_7() const { return ___m_MaxJumpVector_7; }
	inline Vector2_t2243707579 * get_address_of_m_MaxJumpVector_7() { return &___m_MaxJumpVector_7; }
	inline void set_m_MaxJumpVector_7(Vector2_t2243707579  value)
	{
		___m_MaxJumpVector_7 = value;
	}

	inline static int32_t get_offset_of_m_GroundLayer_8() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_GroundLayer_8)); }
	inline LayerMask_t3188175821  get_m_GroundLayer_8() const { return ___m_GroundLayer_8; }
	inline LayerMask_t3188175821 * get_address_of_m_GroundLayer_8() { return &___m_GroundLayer_8; }
	inline void set_m_GroundLayer_8(LayerMask_t3188175821  value)
	{
		___m_GroundLayer_8 = value;
	}

	inline static int32_t get_offset_of_m_JellySprite_9() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_JellySprite_9)); }
	inline JellySprite_t2775849197 * get_m_JellySprite_9() const { return ___m_JellySprite_9; }
	inline JellySprite_t2775849197 ** get_address_of_m_JellySprite_9() { return &___m_JellySprite_9; }
	inline void set_m_JellySprite_9(JellySprite_t2775849197 * value)
	{
		___m_JellySprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_JellySprite_9, value);
	}

	inline static int32_t get_offset_of_m_BounceTimer_10() { return static_cast<int32_t>(offsetof(BlobBehaviour_t3204652050, ___m_BounceTimer_10)); }
	inline float get_m_BounceTimer_10() const { return ___m_BounceTimer_10; }
	inline float* get_address_of_m_BounceTimer_10() { return &___m_BounceTimer_10; }
	inline void set_m_BounceTimer_10(float value)
	{
		___m_BounceTimer_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
