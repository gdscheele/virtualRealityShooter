﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Converter_2_gen106372939MethodDeclarations.h"

// System.Void System.Converter`2<UnityEngine.Component,UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define Converter_2__ctor_m200806311(__this, ___object0, ___method1, method) ((  void (*) (Converter_2_t1214379198 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Converter_2__ctor_m2798627395_gshared)(__this, ___object0, ___method1, method)
// TOutput System.Converter`2<UnityEngine.Component,UnityEngine.Transform>::Invoke(TInput)
#define Converter_2_Invoke_m2610140886(__this, ___input0, method) ((  Transform_t3275118058 * (*) (Converter_2_t1214379198 *, Component_t3819376471 *, const MethodInfo*))Converter_2_Invoke_m77799585_gshared)(__this, ___input0, method)
// System.IAsyncResult System.Converter`2<UnityEngine.Component,UnityEngine.Transform>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
#define Converter_2_BeginInvoke_m2396992901(__this, ___input0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Converter_2_t1214379198 *, Component_t3819376471 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Converter_2_BeginInvoke_m898151494_gshared)(__this, ___input0, ___callback1, ___object2, method)
// TOutput System.Converter`2<UnityEngine.Component,UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Converter_2_EndInvoke_m3698020438(__this, ___result0, method) ((  Transform_t3275118058 * (*) (Converter_2_t1214379198 *, Il2CppObject *, const MethodInfo*))Converter_2_EndInvoke_m1606718561_gshared)(__this, ___result0, method)
