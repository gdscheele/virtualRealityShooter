﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TiledPage/<CalculateTilesByDistance>c__AnonStorey0
struct U3CCalculateTilesByDistanceU3Ec__AnonStorey0_t2727860256;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21251105099.h"

// System.Void TiledPage/<CalculateTilesByDistance>c__AnonStorey0::.ctor()
extern "C"  void U3CCalculateTilesByDistanceU3Ec__AnonStorey0__ctor_m3969643317 (U3CCalculateTilesByDistanceU3Ec__AnonStorey0_t2727860256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TiledPage/<CalculateTilesByDistance>c__AnonStorey0::<>m__0(System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>)
extern "C"  bool U3CCalculateTilesByDistanceU3Ec__AnonStorey0_U3CU3Em__0_m2337730583 (U3CCalculateTilesByDistanceU3Ec__AnonStorey0_t2727860256 * __this, KeyValuePair_2_t1251105099  ___pair0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
