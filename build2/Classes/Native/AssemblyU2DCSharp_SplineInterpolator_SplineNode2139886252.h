﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineInterpolator/SplineNode
struct  SplineNode_t2139886252  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 SplineInterpolator/SplineNode::Point
	Vector3_t2243707580  ___Point_0;
	// UnityEngine.Quaternion SplineInterpolator/SplineNode::Rot
	Quaternion_t4030073918  ___Rot_1;
	// System.Single SplineInterpolator/SplineNode::Time
	float ___Time_2;
	// UnityEngine.Vector2 SplineInterpolator/SplineNode::EaseIO
	Vector2_t2243707579  ___EaseIO_3;

public:
	inline static int32_t get_offset_of_Point_0() { return static_cast<int32_t>(offsetof(SplineNode_t2139886252, ___Point_0)); }
	inline Vector3_t2243707580  get_Point_0() const { return ___Point_0; }
	inline Vector3_t2243707580 * get_address_of_Point_0() { return &___Point_0; }
	inline void set_Point_0(Vector3_t2243707580  value)
	{
		___Point_0 = value;
	}

	inline static int32_t get_offset_of_Rot_1() { return static_cast<int32_t>(offsetof(SplineNode_t2139886252, ___Rot_1)); }
	inline Quaternion_t4030073918  get_Rot_1() const { return ___Rot_1; }
	inline Quaternion_t4030073918 * get_address_of_Rot_1() { return &___Rot_1; }
	inline void set_Rot_1(Quaternion_t4030073918  value)
	{
		___Rot_1 = value;
	}

	inline static int32_t get_offset_of_Time_2() { return static_cast<int32_t>(offsetof(SplineNode_t2139886252, ___Time_2)); }
	inline float get_Time_2() const { return ___Time_2; }
	inline float* get_address_of_Time_2() { return &___Time_2; }
	inline void set_Time_2(float value)
	{
		___Time_2 = value;
	}

	inline static int32_t get_offset_of_EaseIO_3() { return static_cast<int32_t>(offsetof(SplineNode_t2139886252, ___EaseIO_3)); }
	inline Vector2_t2243707579  get_EaseIO_3() const { return ___EaseIO_3; }
	inline Vector2_t2243707579 * get_address_of_EaseIO_3() { return &___EaseIO_3; }
	inline void set_EaseIO_3(Vector2_t2243707579  value)
	{
		___EaseIO_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
