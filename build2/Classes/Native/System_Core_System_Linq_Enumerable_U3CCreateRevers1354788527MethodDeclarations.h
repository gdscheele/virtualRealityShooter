﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerator_1_t3066806327;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m554420423_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m554420423(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m554420423_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t1296315204  U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1567733982_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1567733982(__this, method) ((  KeyValuePair_2_t1296315204  (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1567733982_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m3390527505_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m3390527505(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m3390527505_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m199486390_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m199486390(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m199486390_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m933296271_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m933296271(__this, method) ((  Il2CppObject* (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m933296271_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::MoveNext()
extern "C"  bool U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m2441559789_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m2441559789(__this, method) ((  bool (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m2441559789_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Dispose()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m3728056010_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m3728056010(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m3728056010_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Reset()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m2000560704_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m2000560704(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t1354788527 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m2000560704_gshared)(__this, method)
