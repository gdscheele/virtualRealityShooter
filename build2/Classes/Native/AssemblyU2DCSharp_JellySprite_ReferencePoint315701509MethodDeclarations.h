﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySprite/ReferencePoint
struct ReferencePoint_t315701509;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// UnityEngine.SphereCollider
struct SphereCollider_t1662511355;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void JellySprite/ReferencePoint::.ctor(UnityEngine.Rigidbody2D)
extern "C"  void ReferencePoint__ctor_m2498190136 (ReferencePoint_t315701509 * __this, Rigidbody2D_t502193897 * ___body0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/ReferencePoint::.ctor(UnityEngine.Rigidbody)
extern "C"  void ReferencePoint__ctor_m2053207926 (ReferencePoint_t315701509 * __this, Rigidbody_t4233889191 * ___body0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 JellySprite/ReferencePoint::get_InitialOffset()
extern "C"  Vector3_t2243707580  ReferencePoint_get_InitialOffset_m2882438660 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/ReferencePoint::set_InitialOffset(UnityEngine.Vector3)
extern "C"  void ReferencePoint_set_InitialOffset_m3994761735 (ReferencePoint_t315701509 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D JellySprite/ReferencePoint::get_Body2D()
extern "C"  Rigidbody2D_t502193897 * ReferencePoint_get_Body2D_m122834398 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody JellySprite/ReferencePoint::get_Body3D()
extern "C"  Rigidbody_t4233889191 * ReferencePoint_get_Body3D_m1379199807 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CircleCollider2D JellySprite/ReferencePoint::get_Collider2D()
extern "C"  CircleCollider2D_t13116344 * ReferencePoint_get_Collider2D_m1862612355 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SphereCollider JellySprite/ReferencePoint::get_Collider()
extern "C"  SphereCollider_t1662511355 * ReferencePoint_get_Collider_m2210651748 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JellySprite/ReferencePoint::get_IsDummy()
extern "C"  bool ReferencePoint_get_IsDummy_m3690667791 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JellySprite/ReferencePoint::get_Radius()
extern "C"  float ReferencePoint_get_Radius_m2786130671 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject JellySprite/ReferencePoint::get_GameObject()
extern "C"  GameObject_t1756533147 * ReferencePoint_get_GameObject_m2927000193 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform JellySprite/ReferencePoint::get_transform()
extern "C"  Transform_t3275118058 * ReferencePoint_get_transform_m4100349357 (ReferencePoint_t315701509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/ReferencePoint::SetKinematic(System.Boolean)
extern "C"  void ReferencePoint_SetKinematic_m2466302304 (ReferencePoint_t315701509 * __this, bool ___kinematic0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
