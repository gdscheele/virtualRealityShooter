﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;

#include "AssemblyU2DCSharp_GvrBasePointer2150122635.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrReticlePointer
struct  GvrReticlePointer_t2836438988  : public GvrBasePointer_t2150122635
{
public:
	// System.Int32 GvrReticlePointer::reticleSegments
	int32_t ___reticleSegments_2;
	// System.Single GvrReticlePointer::reticleGrowthSpeed
	float ___reticleGrowthSpeed_3;
	// UnityEngine.Material GvrReticlePointer::materialComp
	Material_t193706927 * ___materialComp_4;
	// System.Single GvrReticlePointer::reticleInnerAngle
	float ___reticleInnerAngle_5;
	// System.Single GvrReticlePointer::reticleOuterAngle
	float ___reticleOuterAngle_6;
	// System.Single GvrReticlePointer::reticleDistanceInMeters
	float ___reticleDistanceInMeters_7;
	// System.Single GvrReticlePointer::reticleInnerDiameter
	float ___reticleInnerDiameter_13;
	// System.Single GvrReticlePointer::reticleOuterDiameter
	float ___reticleOuterDiameter_14;

public:
	inline static int32_t get_offset_of_reticleSegments_2() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleSegments_2)); }
	inline int32_t get_reticleSegments_2() const { return ___reticleSegments_2; }
	inline int32_t* get_address_of_reticleSegments_2() { return &___reticleSegments_2; }
	inline void set_reticleSegments_2(int32_t value)
	{
		___reticleSegments_2 = value;
	}

	inline static int32_t get_offset_of_reticleGrowthSpeed_3() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleGrowthSpeed_3)); }
	inline float get_reticleGrowthSpeed_3() const { return ___reticleGrowthSpeed_3; }
	inline float* get_address_of_reticleGrowthSpeed_3() { return &___reticleGrowthSpeed_3; }
	inline void set_reticleGrowthSpeed_3(float value)
	{
		___reticleGrowthSpeed_3 = value;
	}

	inline static int32_t get_offset_of_materialComp_4() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___materialComp_4)); }
	inline Material_t193706927 * get_materialComp_4() const { return ___materialComp_4; }
	inline Material_t193706927 ** get_address_of_materialComp_4() { return &___materialComp_4; }
	inline void set_materialComp_4(Material_t193706927 * value)
	{
		___materialComp_4 = value;
		Il2CppCodeGenWriteBarrier(&___materialComp_4, value);
	}

	inline static int32_t get_offset_of_reticleInnerAngle_5() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleInnerAngle_5)); }
	inline float get_reticleInnerAngle_5() const { return ___reticleInnerAngle_5; }
	inline float* get_address_of_reticleInnerAngle_5() { return &___reticleInnerAngle_5; }
	inline void set_reticleInnerAngle_5(float value)
	{
		___reticleInnerAngle_5 = value;
	}

	inline static int32_t get_offset_of_reticleOuterAngle_6() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleOuterAngle_6)); }
	inline float get_reticleOuterAngle_6() const { return ___reticleOuterAngle_6; }
	inline float* get_address_of_reticleOuterAngle_6() { return &___reticleOuterAngle_6; }
	inline void set_reticleOuterAngle_6(float value)
	{
		___reticleOuterAngle_6 = value;
	}

	inline static int32_t get_offset_of_reticleDistanceInMeters_7() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleDistanceInMeters_7)); }
	inline float get_reticleDistanceInMeters_7() const { return ___reticleDistanceInMeters_7; }
	inline float* get_address_of_reticleDistanceInMeters_7() { return &___reticleDistanceInMeters_7; }
	inline void set_reticleDistanceInMeters_7(float value)
	{
		___reticleDistanceInMeters_7 = value;
	}

	inline static int32_t get_offset_of_reticleInnerDiameter_13() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleInnerDiameter_13)); }
	inline float get_reticleInnerDiameter_13() const { return ___reticleInnerDiameter_13; }
	inline float* get_address_of_reticleInnerDiameter_13() { return &___reticleInnerDiameter_13; }
	inline void set_reticleInnerDiameter_13(float value)
	{
		___reticleInnerDiameter_13 = value;
	}

	inline static int32_t get_offset_of_reticleOuterDiameter_14() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t2836438988, ___reticleOuterDiameter_14)); }
	inline float get_reticleOuterDiameter_14() const { return ___reticleOuterDiameter_14; }
	inline float* get_address_of_reticleOuterDiameter_14() { return &___reticleOuterDiameter_14; }
	inline void set_reticleOuterDiameter_14(float value)
	{
		___reticleOuterDiameter_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
