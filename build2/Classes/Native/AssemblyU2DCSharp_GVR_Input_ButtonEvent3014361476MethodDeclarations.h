﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.ButtonEvent
struct ButtonEvent_t3014361476;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.ButtonEvent::.ctor()
extern "C"  void ButtonEvent__ctor_m1676339476 (ButtonEvent_t3014361476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
