﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TranslateScrollEffect
struct TranslateScrollEffect_t636656150;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseScrollEffect_UpdateData3671771611.h"

// System.Void TranslateScrollEffect::.ctor()
extern "C"  void TranslateScrollEffect__ctor_m2625199231 (TranslateScrollEffect_t636656150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TranslateScrollEffect::ApplyEffect(BaseScrollEffect/UpdateData)
extern "C"  void TranslateScrollEffect_ApplyEffect_m3146858813 (TranslateScrollEffect_t636656150 * __this, UpdateData_t3671771611  ___updateData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
