﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoPlayerReference
struct VideoPlayerReference_t1150574547;

#include "codegen/il2cpp-codegen.h"

// System.Void VideoPlayerReference::.ctor()
extern "C"  void VideoPlayerReference__ctor_m943420494 (VideoPlayerReference_t1150574547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoPlayerReference::Awake()
extern "C"  void VideoPlayerReference_Awake_m2826416547 (VideoPlayerReference_t1150574547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
