﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrubberEvents
struct ScrubberEvents_t2429506345;
// VideoControlsManager
struct VideoControlsManager_t3010523296;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VideoControlsManager3010523296.h"

// System.Void ScrubberEvents::.ctor()
extern "C"  void ScrubberEvents__ctor_m1667084696 (ScrubberEvents_t2429506345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrubberEvents::set_ControlManager(VideoControlsManager)
extern "C"  void ScrubberEvents_set_ControlManager_m4017509979 (ScrubberEvents_t2429506345 * __this, VideoControlsManager_t3010523296 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrubberEvents::Start()
extern "C"  void ScrubberEvents_Start_m3106664808 (ScrubberEvents_t2429506345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrubberEvents::Update()
extern "C"  void ScrubberEvents_Update_m4051939089 (ScrubberEvents_t2429506345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
