﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t3497673348;
// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellySprite/JellyCollider
struct  JellyCollider_t1548403138  : public Il2CppObject
{
public:
	// UnityEngine.Collider JellySprite/JellyCollider::<Collider>k__BackingField
	Collider_t3497673348 * ___U3CColliderU3Ek__BackingField_0;
	// JellySpriteReferencePoint JellySprite/JellyCollider::<ReferencePoint>k__BackingField
	JellySpriteReferencePoint_t291100992 * ___U3CReferencePointU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CColliderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JellyCollider_t1548403138, ___U3CColliderU3Ek__BackingField_0)); }
	inline Collider_t3497673348 * get_U3CColliderU3Ek__BackingField_0() const { return ___U3CColliderU3Ek__BackingField_0; }
	inline Collider_t3497673348 ** get_address_of_U3CColliderU3Ek__BackingField_0() { return &___U3CColliderU3Ek__BackingField_0; }
	inline void set_U3CColliderU3Ek__BackingField_0(Collider_t3497673348 * value)
	{
		___U3CColliderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CColliderU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CReferencePointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JellyCollider_t1548403138, ___U3CReferencePointU3Ek__BackingField_1)); }
	inline JellySpriteReferencePoint_t291100992 * get_U3CReferencePointU3Ek__BackingField_1() const { return ___U3CReferencePointU3Ek__BackingField_1; }
	inline JellySpriteReferencePoint_t291100992 ** get_address_of_U3CReferencePointU3Ek__BackingField_1() { return &___U3CReferencePointU3Ek__BackingField_1; }
	inline void set_U3CReferencePointU3Ek__BackingField_1(JellySpriteReferencePoint_t291100992 * value)
	{
		___U3CReferencePointU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReferencePointU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
