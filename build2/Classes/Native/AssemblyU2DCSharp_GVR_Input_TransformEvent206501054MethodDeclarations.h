﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GVR.Input.TransformEvent
struct TransformEvent_t206501054;

#include "codegen/il2cpp-codegen.h"

// System.Void GVR.Input.TransformEvent::.ctor()
extern "C"  void TransformEvent__ctor_m4078659930 (TransformEvent_t206501054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
