﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotateGameObject
struct RotateGameObject_t3422607042;

#include "codegen/il2cpp-codegen.h"

// System.Void RotateGameObject::.ctor()
extern "C"  void RotateGameObject__ctor_m61980329 (RotateGameObject_t3422607042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateGameObject::Start()
extern "C"  void RotateGameObject_Start_m3940004033 (RotateGameObject_t3422607042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateGameObject::FixedUpdate()
extern "C"  void RotateGameObject_FixedUpdate_m676083286 (RotateGameObject_t3422607042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
