﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySprite/JellyCollision
struct JellyCollision_t1959843398;
// UnityEngine.Collision
struct Collision_t2876846408;
// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "AssemblyU2DCSharp_JellySpriteReferencePoint291100992.h"

// System.Void JellySprite/JellyCollision::.ctor()
extern "C"  void JellyCollision__ctor_m161245207 (JellyCollision_t1959843398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collision JellySprite/JellyCollision::get_Collision()
extern "C"  Collision_t2876846408 * JellyCollision_get_Collision_m4048111960 (JellyCollision_t1959843398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollision::set_Collision(UnityEngine.Collision)
extern "C"  void JellyCollision_set_Collision_m3456702365 (JellyCollision_t1959843398 * __this, Collision_t2876846408 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JellySpriteReferencePoint JellySprite/JellyCollision::get_ReferencePoint()
extern "C"  JellySpriteReferencePoint_t291100992 * JellyCollision_get_ReferencePoint_m2616076656 (JellyCollision_t1959843398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollision::set_ReferencePoint(JellySpriteReferencePoint)
extern "C"  void JellyCollision_set_ReferencePoint_m3265206505 (JellyCollision_t1959843398 * __this, JellySpriteReferencePoint_t291100992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
