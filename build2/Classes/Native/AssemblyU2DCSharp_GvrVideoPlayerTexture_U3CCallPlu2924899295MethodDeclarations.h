﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1
struct U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::.ctor()
extern "C"  void U3CCallPluginAtEndOfFramesU3Ec__Iterator1__ctor_m436051200 (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::MoveNext()
extern "C"  bool U3CCallPluginAtEndOfFramesU3Ec__Iterator1_MoveNext_m617372636 (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCallPluginAtEndOfFramesU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3774199866 (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCallPluginAtEndOfFramesU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3319191906 (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::Dispose()
extern "C"  void U3CCallPluginAtEndOfFramesU3Ec__Iterator1_Dispose_m1181112657 (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrVideoPlayerTexture/<CallPluginAtEndOfFrames>c__Iterator1::Reset()
extern "C"  void U3CCallPluginAtEndOfFramesU3Ec__Iterator1_Reset_m2827171363 (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
