﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// JellySprite
struct JellySprite_t2775849197;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellyCameraFollow
struct  JellyCameraFollow_t4240209918  : public MonoBehaviour_t1158329972
{
public:
	// JellySprite JellyCameraFollow::m_FollowSprite
	JellySprite_t2775849197 * ___m_FollowSprite_2;

public:
	inline static int32_t get_offset_of_m_FollowSprite_2() { return static_cast<int32_t>(offsetof(JellyCameraFollow_t4240209918, ___m_FollowSprite_2)); }
	inline JellySprite_t2775849197 * get_m_FollowSprite_2() const { return ___m_FollowSprite_2; }
	inline JellySprite_t2775849197 ** get_address_of_m_FollowSprite_2() { return &___m_FollowSprite_2; }
	inline void set_m_FollowSprite_2(JellySprite_t2775849197 * value)
	{
		___m_FollowSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_FollowSprite_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
