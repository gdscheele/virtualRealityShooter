﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct ReadOnlyCollection_1_t1482100896;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IList_1_t1837255805;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>[]
struct KeyValuePair_2U5BU5D_t3257983789;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerator_1_t3066806327;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2585304574_gshared (ReadOnlyCollection_1_t1482100896 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2585304574(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2585304574_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3572537534_gshared (ReadOnlyCollection_1_t1482100896 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3572537534(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, KeyValuePair_2_t1296315204 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3572537534_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1039847282_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1039847282(__this, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1039847282_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4204234299_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4204234299(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4204234299_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1493955139_gshared (ReadOnlyCollection_1_t1482100896 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1493955139(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, KeyValuePair_2_t1296315204 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1493955139_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2205380215_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2205380215(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2205380215_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  KeyValuePair_2_t1296315204  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2435088283_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2435088283(__this, ___index0, method) ((  KeyValuePair_2_t1296315204  (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2435088283_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3761209828_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3761209828(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3761209828_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2046545756_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2046545756(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2046545756_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4197465165_gshared (ReadOnlyCollection_1_t1482100896 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4197465165(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4197465165_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m204734188_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m204734188(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m204734188_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2052018349_gshared (ReadOnlyCollection_1_t1482100896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2052018349(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1482100896 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2052018349_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1306580685_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1306580685(__this, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1306580685_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m69170733_gshared (ReadOnlyCollection_1_t1482100896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m69170733(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m69170733_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2580697303_gshared (ReadOnlyCollection_1_t1482100896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2580697303(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1482100896 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2580697303_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1855335518_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1855335518(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1855335518_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2393244828_gshared (ReadOnlyCollection_1_t1482100896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2393244828(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2393244828_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m810404020_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m810404020(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m810404020_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2596256705_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2596256705(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2596256705_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m752091297_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m752091297(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m752091297_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3417201942_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3417201942(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3417201942_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m765881685_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m765881685(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m765881685_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m927232148_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m927232148(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m927232148_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m964094943_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m964094943(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m964094943_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3385006260_gshared (ReadOnlyCollection_1_t1482100896 * __this, KeyValuePair_2_t1296315204  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3385006260(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1482100896 *, KeyValuePair_2_t1296315204 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3385006260_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2679981890_gshared (ReadOnlyCollection_1_t1482100896 * __this, KeyValuePair_2U5BU5D_t3257983789* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2679981890(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1482100896 *, KeyValuePair_2U5BU5D_t3257983789*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2679981890_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1132980933_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1132980933(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1132980933_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2691996660_gshared (ReadOnlyCollection_1_t1482100896 * __this, KeyValuePair_2_t1296315204  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2691996660(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1482100896 *, KeyValuePair_2_t1296315204 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2691996660_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3874263933_gshared (ReadOnlyCollection_1_t1482100896 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3874263933(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1482100896 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3874263933_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t1296315204  ReadOnlyCollection_1_get_Item_m3551621647_gshared (ReadOnlyCollection_1_t1482100896 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3551621647(__this, ___index0, method) ((  KeyValuePair_2_t1296315204  (*) (ReadOnlyCollection_1_t1482100896 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3551621647_gshared)(__this, ___index0, method)
