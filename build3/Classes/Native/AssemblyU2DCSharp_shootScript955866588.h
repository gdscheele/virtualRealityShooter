﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// shootScript
struct  shootScript_t955866588  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject shootScript::Rock1A
	GameObject_t1756533147 * ___Rock1A_2;
	// System.Single shootScript::speed
	float ___speed_3;
	// System.Int32 shootScript::rocksShot
	int32_t ___rocksShot_4;

public:
	inline static int32_t get_offset_of_Rock1A_2() { return static_cast<int32_t>(offsetof(shootScript_t955866588, ___Rock1A_2)); }
	inline GameObject_t1756533147 * get_Rock1A_2() const { return ___Rock1A_2; }
	inline GameObject_t1756533147 ** get_address_of_Rock1A_2() { return &___Rock1A_2; }
	inline void set_Rock1A_2(GameObject_t1756533147 * value)
	{
		___Rock1A_2 = value;
		Il2CppCodeGenWriteBarrier(&___Rock1A_2, value);
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(shootScript_t955866588, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_rocksShot_4() { return static_cast<int32_t>(offsetof(shootScript_t955866588, ___rocksShot_4)); }
	inline int32_t get_rocksShot_4() const { return ___rocksShot_4; }
	inline int32_t* get_address_of_rocksShot_4() { return &___rocksShot_4; }
	inline void set_rocksShot_4(int32_t value)
	{
		___rocksShot_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
