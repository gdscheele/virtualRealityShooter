﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineInterpolator/SplineNode
struct SplineNode_t2139886252;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_SplineInterpolator_SplineNode2139886252.h"

// System.Void SplineInterpolator/SplineNode::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,UnityEngine.Vector2)
extern "C"  void SplineNode__ctor_m1981198026 (SplineNode_t2139886252 * __this, Vector3_t2243707580  ___p0, Quaternion_t4030073918  ___q1, float ___t2, Vector2_t2243707579  ___io3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator/SplineNode::.ctor(SplineInterpolator/SplineNode)
extern "C"  void SplineNode__ctor_m2778610539 (SplineNode_t2139886252 * __this, SplineNode_t2139886252 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
