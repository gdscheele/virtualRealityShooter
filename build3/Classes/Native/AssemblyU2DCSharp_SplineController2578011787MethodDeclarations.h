﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineController
struct SplineController_t2578011787;
// SplineInterpolator
struct SplineInterpolator_t4279526764;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Component
struct Component_t3819376471;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SplineInterpolator4279526764.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void SplineController::.ctor()
extern "C"  void SplineController__ctor_m2329684916 (SplineController_t2578011787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineController::OnDrawGizmos()
extern "C"  void SplineController_OnDrawGizmos_m2050165998 (SplineController_t2578011787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineController::Start()
extern "C"  void SplineController_Start_m2003066840 (SplineController_t2578011787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineController::SetupSplineInterpolator(SplineInterpolator,UnityEngine.Transform[])
extern "C"  void SplineController_SetupSplineInterpolator_m2579312766 (SplineController_t2578011787 * __this, SplineInterpolator_t4279526764 * ___interp0, TransformU5BU5D_t3764228911* ___trans1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] SplineController::GetTransforms()
extern "C"  TransformU5BU5D_t3764228911* SplineController_GetTransforms_m865675773 (SplineController_t2578011787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineController::DisableTransforms()
extern "C"  void SplineController_DisableTransforms_m3636309633 (SplineController_t2578011787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineController::FollowSpline()
extern "C"  void SplineController_FollowSpline_m3591273766 (SplineController_t2578011787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform SplineController::<GetTransforms>m__0(UnityEngine.Component)
extern "C"  Transform_t3275118058 * SplineController_U3CGetTransformsU3Em__0_m2974319792 (Il2CppObject * __this /* static, unused */, Component_t3819376471 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SplineController::<GetTransforms>m__1(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  int32_t SplineController_U3CGetTransformsU3Em__1_m3470473179 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___a0, Transform_t3275118058 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
