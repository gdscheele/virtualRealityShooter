﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Joint2D
struct Joint2D_t854621618;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"

// System.Void UnityEngine.Joint2D::set_connectedBody(UnityEngine.Rigidbody2D)
extern "C"  void Joint2D_set_connectedBody_m4013462451 (Joint2D_t854621618 * __this, Rigidbody2D_t502193897 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::set_enableCollision(System.Boolean)
extern "C"  void Joint2D_set_enableCollision_m588727020 (Joint2D_t854621618 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
