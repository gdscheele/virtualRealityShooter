﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PointController
struct PointController_t705534758;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EndEffects
struct  EndEffects_t1529040885  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 EndEffects::donePosition
	Vector3_t2243707580  ___donePosition_2;
	// PointController EndEffects::pc
	PointController_t705534758 * ___pc_3;
	// UnityEngine.GameObject EndEffects::endCanvas
	GameObject_t1756533147 * ___endCanvas_4;
	// UnityEngine.GameObject EndEffects::endTextObject
	GameObject_t1756533147 * ___endTextObject_5;
	// UnityEngine.UI.Text EndEffects::endText
	Text_t356221433 * ___endText_6;
	// UnityEngine.GameObject EndEffects::scoreTextObject
	GameObject_t1756533147 * ___scoreTextObject_7;

public:
	inline static int32_t get_offset_of_donePosition_2() { return static_cast<int32_t>(offsetof(EndEffects_t1529040885, ___donePosition_2)); }
	inline Vector3_t2243707580  get_donePosition_2() const { return ___donePosition_2; }
	inline Vector3_t2243707580 * get_address_of_donePosition_2() { return &___donePosition_2; }
	inline void set_donePosition_2(Vector3_t2243707580  value)
	{
		___donePosition_2 = value;
	}

	inline static int32_t get_offset_of_pc_3() { return static_cast<int32_t>(offsetof(EndEffects_t1529040885, ___pc_3)); }
	inline PointController_t705534758 * get_pc_3() const { return ___pc_3; }
	inline PointController_t705534758 ** get_address_of_pc_3() { return &___pc_3; }
	inline void set_pc_3(PointController_t705534758 * value)
	{
		___pc_3 = value;
		Il2CppCodeGenWriteBarrier(&___pc_3, value);
	}

	inline static int32_t get_offset_of_endCanvas_4() { return static_cast<int32_t>(offsetof(EndEffects_t1529040885, ___endCanvas_4)); }
	inline GameObject_t1756533147 * get_endCanvas_4() const { return ___endCanvas_4; }
	inline GameObject_t1756533147 ** get_address_of_endCanvas_4() { return &___endCanvas_4; }
	inline void set_endCanvas_4(GameObject_t1756533147 * value)
	{
		___endCanvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___endCanvas_4, value);
	}

	inline static int32_t get_offset_of_endTextObject_5() { return static_cast<int32_t>(offsetof(EndEffects_t1529040885, ___endTextObject_5)); }
	inline GameObject_t1756533147 * get_endTextObject_5() const { return ___endTextObject_5; }
	inline GameObject_t1756533147 ** get_address_of_endTextObject_5() { return &___endTextObject_5; }
	inline void set_endTextObject_5(GameObject_t1756533147 * value)
	{
		___endTextObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___endTextObject_5, value);
	}

	inline static int32_t get_offset_of_endText_6() { return static_cast<int32_t>(offsetof(EndEffects_t1529040885, ___endText_6)); }
	inline Text_t356221433 * get_endText_6() const { return ___endText_6; }
	inline Text_t356221433 ** get_address_of_endText_6() { return &___endText_6; }
	inline void set_endText_6(Text_t356221433 * value)
	{
		___endText_6 = value;
		Il2CppCodeGenWriteBarrier(&___endText_6, value);
	}

	inline static int32_t get_offset_of_scoreTextObject_7() { return static_cast<int32_t>(offsetof(EndEffects_t1529040885, ___scoreTextObject_7)); }
	inline GameObject_t1756533147 * get_scoreTextObject_7() const { return ___scoreTextObject_7; }
	inline GameObject_t1756533147 ** get_address_of_scoreTextObject_7() { return &___scoreTextObject_7; }
	inline void set_scoreTextObject_7(GameObject_t1756533147 * value)
	{
		___scoreTextObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___scoreTextObject_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
