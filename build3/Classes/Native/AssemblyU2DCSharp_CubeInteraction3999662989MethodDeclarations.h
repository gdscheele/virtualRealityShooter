﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CubeInteraction
struct CubeInteraction_t3999662989;

#include "codegen/il2cpp-codegen.h"

// System.Void CubeInteraction::.ctor()
extern "C"  void CubeInteraction__ctor_m2503234674 (CubeInteraction_t3999662989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeInteraction::Start()
extern "C"  void CubeInteraction_Start_m2248225938 (CubeInteraction_t3999662989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeInteraction::Update()
extern "C"  void CubeInteraction_Update_m530588341 (CubeInteraction_t3999662989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CubeInteraction::onClick()
extern "C"  void CubeInteraction_onClick_m816386259 (CubeInteraction_t3999662989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
