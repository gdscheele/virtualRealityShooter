﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProjectileCollision
struct ProjectileCollision_t1347523025;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void ProjectileCollision::.ctor()
extern "C"  void ProjectileCollision__ctor_m1034886156 (ProjectileCollision_t1347523025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProjectileCollision::Start()
extern "C"  void ProjectileCollision_Start_m281748396 (ProjectileCollision_t1347523025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProjectileCollision::Update()
extern "C"  void ProjectileCollision_Update_m1636543985 (ProjectileCollision_t1347523025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProjectileCollision::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void ProjectileCollision_OnCollisionEnter_m2367082182 (ProjectileCollision_t1347523025 * __this, Collision_t2876846408 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
