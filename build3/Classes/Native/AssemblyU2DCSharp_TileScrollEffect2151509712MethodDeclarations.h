﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TileScrollEffect
struct TileScrollEffect_t2151509712;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseScrollEffect_UpdateData3671771611.h"

// System.Void TileScrollEffect::.ctor()
extern "C"  void TileScrollEffect__ctor_m3629573613 (TileScrollEffect_t2151509712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TileScrollEffect::ApplyEffect(BaseScrollEffect/UpdateData)
extern "C"  void TileScrollEffect_ApplyEffect_m4293967987 (TileScrollEffect_t2151509712 * __this, UpdateData_t3671771611  ___updateData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
