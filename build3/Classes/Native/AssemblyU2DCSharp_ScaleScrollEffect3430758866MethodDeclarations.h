﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScaleScrollEffect
struct ScaleScrollEffect_t3430758866;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseScrollEffect_UpdateData3671771611.h"

// System.Void ScaleScrollEffect::.ctor()
extern "C"  void ScaleScrollEffect__ctor_m84586081 (ScaleScrollEffect_t3430758866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScaleScrollEffect::ApplyEffect(BaseScrollEffect/UpdateData)
extern "C"  void ScaleScrollEffect_ApplyEffect_m2926102799 (ScaleScrollEffect_t3430758866 * __this, UpdateData_t3671771611  ___updateData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
