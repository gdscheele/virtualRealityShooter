﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProjectileController
struct ProjectileController_t1739291571;

#include "codegen/il2cpp-codegen.h"

// System.Void ProjectileController::.ctor()
extern "C"  void ProjectileController__ctor_m3312069614 (ProjectileController_t1739291571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProjectileController::Start()
extern "C"  void ProjectileController_Start_m820904562 (ProjectileController_t1739291571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProjectileController::Update()
extern "C"  void ProjectileController_Update_m2289265599 (ProjectileController_t1739291571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProjectileController::fireProjectile()
extern "C"  void ProjectileController_fireProjectile_m1822741247 (ProjectileController_t1739291571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
