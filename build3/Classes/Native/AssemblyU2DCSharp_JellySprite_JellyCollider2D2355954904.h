﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider2D
struct Collider2D_t646061738;
// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellySprite/JellyCollider2D
struct  JellyCollider2D_t2355954904  : public Il2CppObject
{
public:
	// UnityEngine.Collider2D JellySprite/JellyCollider2D::<Collider2D>k__BackingField
	Collider2D_t646061738 * ___U3CCollider2DU3Ek__BackingField_0;
	// JellySpriteReferencePoint JellySprite/JellyCollider2D::<ReferencePoint>k__BackingField
	JellySpriteReferencePoint_t291100992 * ___U3CReferencePointU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCollider2DU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JellyCollider2D_t2355954904, ___U3CCollider2DU3Ek__BackingField_0)); }
	inline Collider2D_t646061738 * get_U3CCollider2DU3Ek__BackingField_0() const { return ___U3CCollider2DU3Ek__BackingField_0; }
	inline Collider2D_t646061738 ** get_address_of_U3CCollider2DU3Ek__BackingField_0() { return &___U3CCollider2DU3Ek__BackingField_0; }
	inline void set_U3CCollider2DU3Ek__BackingField_0(Collider2D_t646061738 * value)
	{
		___U3CCollider2DU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCollider2DU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CReferencePointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JellyCollider2D_t2355954904, ___U3CReferencePointU3Ek__BackingField_1)); }
	inline JellySpriteReferencePoint_t291100992 * get_U3CReferencePointU3Ek__BackingField_1() const { return ___U3CReferencePointU3Ek__BackingField_1; }
	inline JellySpriteReferencePoint_t291100992 ** get_address_of_U3CReferencePointU3Ek__BackingField_1() { return &___U3CReferencePointU3Ek__BackingField_1; }
	inline void set_U3CReferencePointU3Ek__BackingField_1(JellySpriteReferencePoint_t291100992 * value)
	{
		___U3CReferencePointU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReferencePointU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
