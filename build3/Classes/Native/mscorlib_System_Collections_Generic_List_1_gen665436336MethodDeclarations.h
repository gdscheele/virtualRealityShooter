﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct List_1_t665436336;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerable_1_t1588442249;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerator_1_t3066806327;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct ICollection_1_t2248390509;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct ReadOnlyCollection_1_t1482100896;
// System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>[]
struct KeyValuePair_2U5BU5D_t3257983789;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct Predicate_1_t4034252615;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct Comparison_1_t2558054055;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat200166010.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m3991926524_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1__ctor_m3991926524(__this, method) ((  void (*) (List_1_t665436336 *, const MethodInfo*))List_1__ctor_m3991926524_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1307647351_gshared (List_1_t665436336 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1307647351(__this, ___collection0, method) ((  void (*) (List_1_t665436336 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1307647351_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1047402933_gshared (List_1_t665436336 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1047402933(__this, ___capacity0, method) ((  void (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1__ctor_m1047402933_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.cctor()
extern "C"  void List_1__cctor_m2075515423_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2075515423(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2075515423_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3752528422_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3752528422(__this, method) ((  Il2CppObject* (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3752528422_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1126103164_gshared (List_1_t665436336 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1126103164(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t665436336 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1126103164_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1104078161_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1104078161(__this, method) ((  Il2CppObject * (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1104078161_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3185677184_gshared (List_1_t665436336 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3185677184(__this, ___item0, method) ((  int32_t (*) (List_1_t665436336 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3185677184_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3222402690_gshared (List_1_t665436336 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3222402690(__this, ___item0, method) ((  bool (*) (List_1_t665436336 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3222402690_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1080364306_gshared (List_1_t665436336 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1080364306(__this, ___item0, method) ((  int32_t (*) (List_1_t665436336 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1080364306_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3190122077_gshared (List_1_t665436336 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3190122077(__this, ___index0, ___item1, method) ((  void (*) (List_1_t665436336 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3190122077_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1007484837_gshared (List_1_t665436336 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1007484837(__this, ___item0, method) ((  void (*) (List_1_t665436336 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1007484837_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m882502473_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m882502473(__this, method) ((  bool (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m882502473_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m406741476_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m406741476(__this, method) ((  bool (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m406741476_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1301213506_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1301213506(__this, method) ((  Il2CppObject * (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1301213506_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2912713513_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2912713513(__this, method) ((  bool (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2912713513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2051441520_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2051441520(__this, method) ((  bool (*) (List_1_t665436336 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2051441520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m914506405_gshared (List_1_t665436336 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m914506405(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m914506405_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2267600758_gshared (List_1_t665436336 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2267600758(__this, ___index0, ___value1, method) ((  void (*) (List_1_t665436336 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2267600758_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Add(T)
extern "C"  void List_1_Add_m1388759889_gshared (List_1_t665436336 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define List_1_Add_m1388759889(__this, ___item0, method) ((  void (*) (List_1_t665436336 *, KeyValuePair_2_t1296315204 , const MethodInfo*))List_1_Add_m1388759889_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3885547782_gshared (List_1_t665436336 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3885547782(__this, ___newCount0, method) ((  void (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3885547782_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2889084526_gshared (List_1_t665436336 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2889084526(__this, ___collection0, method) ((  void (*) (List_1_t665436336 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2889084526_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2479349518_gshared (List_1_t665436336 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2479349518(__this, ___enumerable0, method) ((  void (*) (List_1_t665436336 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2479349518_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3825146973_gshared (List_1_t665436336 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3825146973(__this, ___collection0, method) ((  void (*) (List_1_t665436336 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3825146973_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1482100896 * List_1_AsReadOnly_m3805240416_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3805240416(__this, method) ((  ReadOnlyCollection_1_t1482100896 * (*) (List_1_t665436336 *, const MethodInfo*))List_1_AsReadOnly_m3805240416_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Clear()
extern "C"  void List_1_Clear_m3912377573_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_Clear_m3912377573(__this, method) ((  void (*) (List_1_t665436336 *, const MethodInfo*))List_1_Clear_m3912377573_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m321238599_gshared (List_1_t665436336 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define List_1_Contains_m321238599(__this, ___item0, method) ((  bool (*) (List_1_t665436336 *, KeyValuePair_2_t1296315204 , const MethodInfo*))List_1_Contains_m321238599_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1996324513_gshared (List_1_t665436336 * __this, KeyValuePair_2U5BU5D_t3257983789* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1996324513(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t665436336 *, KeyValuePair_2U5BU5D_t3257983789*, int32_t, const MethodInfo*))List_1_CopyTo_m1996324513_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t1296315204  List_1_Find_m3608719967_gshared (List_1_t665436336 * __this, Predicate_1_t4034252615 * ___match0, const MethodInfo* method);
#define List_1_Find_m3608719967(__this, ___match0, method) ((  KeyValuePair_2_t1296315204  (*) (List_1_t665436336 *, Predicate_1_t4034252615 *, const MethodInfo*))List_1_Find_m3608719967_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3635970300_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4034252615 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3635970300(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4034252615 *, const MethodInfo*))List_1_CheckMatch_m3635970300_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m642018631_gshared (List_1_t665436336 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4034252615 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m642018631(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t665436336 *, int32_t, int32_t, Predicate_1_t4034252615 *, const MethodInfo*))List_1_GetIndex_m642018631_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t200166010  List_1_GetEnumerator_m3378615184_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3378615184(__this, method) ((  Enumerator_t200166010  (*) (List_1_t665436336 *, const MethodInfo*))List_1_GetEnumerator_m3378615184_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1551284705_gshared (List_1_t665436336 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1551284705(__this, ___item0, method) ((  int32_t (*) (List_1_t665436336 *, KeyValuePair_2_t1296315204 , const MethodInfo*))List_1_IndexOf_m1551284705_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2844154784_gshared (List_1_t665436336 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2844154784(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t665436336 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2844154784_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3910376949_gshared (List_1_t665436336 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3910376949(__this, ___index0, method) ((  void (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3910376949_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4113858158_gshared (List_1_t665436336 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___item1, const MethodInfo* method);
#define List_1_Insert_m4113858158(__this, ___index0, ___item1, method) ((  void (*) (List_1_t665436336 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))List_1_Insert_m4113858158_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1834227267_gshared (List_1_t665436336 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1834227267(__this, ___collection0, method) ((  void (*) (List_1_t665436336 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1834227267_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m3132812384_gshared (List_1_t665436336 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define List_1_Remove_m3132812384(__this, ___item0, method) ((  bool (*) (List_1_t665436336 *, KeyValuePair_2_t1296315204 , const MethodInfo*))List_1_Remove_m3132812384_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3534108760_gshared (List_1_t665436336 * __this, Predicate_1_t4034252615 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3534108760(__this, ___match0, method) ((  int32_t (*) (List_1_t665436336 *, Predicate_1_t4034252615 *, const MethodInfo*))List_1_RemoveAll_m3534108760_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3536955314_gshared (List_1_t665436336 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3536955314(__this, ___index0, method) ((  void (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3536955314_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Reverse()
extern "C"  void List_1_Reverse_m1983271778_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_Reverse_m1983271778(__this, method) ((  void (*) (List_1_t665436336 *, const MethodInfo*))List_1_Reverse_m1983271778_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Sort()
extern "C"  void List_1_Sort_m319643778_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_Sort_m319643778(__this, method) ((  void (*) (List_1_t665436336 *, const MethodInfo*))List_1_Sort_m319643778_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1701845671_gshared (List_1_t665436336 * __this, Comparison_1_t2558054055 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1701845671(__this, ___comparison0, method) ((  void (*) (List_1_t665436336 *, Comparison_1_t2558054055 *, const MethodInfo*))List_1_Sort_m1701845671_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t3257983789* List_1_ToArray_m3723124043_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_ToArray_m3723124043(__this, method) ((  KeyValuePair_2U5BU5D_t3257983789* (*) (List_1_t665436336 *, const MethodInfo*))List_1_ToArray_m3723124043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3919673097_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3919673097(__this, method) ((  void (*) (List_1_t665436336 *, const MethodInfo*))List_1_TrimExcess_m3919673097_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3837712535_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3837712535(__this, method) ((  int32_t (*) (List_1_t665436336 *, const MethodInfo*))List_1_get_Capacity_m3837712535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1713660878_gshared (List_1_t665436336 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1713660878(__this, ___value0, method) ((  void (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1713660878_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m2420905600_gshared (List_1_t665436336 * __this, const MethodInfo* method);
#define List_1_get_Count_m2420905600(__this, method) ((  int32_t (*) (List_1_t665436336 *, const MethodInfo*))List_1_get_Count_m2420905600_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t1296315204  List_1_get_Item_m3508391446_gshared (List_1_t665436336 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3508391446(__this, ___index0, method) ((  KeyValuePair_2_t1296315204  (*) (List_1_t665436336 *, int32_t, const MethodInfo*))List_1_get_Item_m3508391446_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1497658729_gshared (List_1_t665436336 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1497658729(__this, ___index0, ___value1, method) ((  void (*) (List_1_t665436336 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))List_1_set_Item_m1497658729_gshared)(__this, ___index0, ___value1, method)
