﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellyDetectCollision
struct JellyDetectCollision_t2540312241;
// JellySprite/JellyCollision
struct JellyCollision_t1959843398;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JellySprite_JellyCollision1959843398.h"

// System.Void JellyDetectCollision::.ctor()
extern "C"  void JellyDetectCollision__ctor_m648891500 (JellyDetectCollision_t2540312241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellyDetectCollision::Start()
extern "C"  void JellyDetectCollision_Start_m1376856156 (JellyDetectCollision_t2540312241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellyDetectCollision::Update()
extern "C"  void JellyDetectCollision_Update_m2632067133 (JellyDetectCollision_t2540312241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellyDetectCollision::OnJellyCollisionEnter(JellySprite/JellyCollision)
extern "C"  void JellyDetectCollision_OnJellyCollisionEnter_m2105646933 (JellyDetectCollision_t2540312241 * __this, JellyCollision_t1959843398 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
