﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SplineInterpolator
struct SplineInterpolator_t4279526764;
// OnEndCallback
struct OnEndCallback_t2203472397;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnEndCallback2203472397.h"
#include "AssemblyU2DCSharp_eWrapMode2466495092.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void SplineInterpolator::.ctor()
extern "C"  void SplineInterpolator__ctor_m2360003061 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::Awake()
extern "C"  void SplineInterpolator_Awake_m2993849640 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::StartInterpolation(OnEndCallback,System.Boolean,eWrapMode)
extern "C"  void SplineInterpolator_StartInterpolation_m1204025973 (SplineInterpolator_t4279526764 * __this, OnEndCallback_t2203472397 * ___endCallback0, bool ___bRotations1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::Reset()
extern "C"  void SplineInterpolator_Reset_m3158361412 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::AddPoint(UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,UnityEngine.Vector2)
extern "C"  void SplineInterpolator_AddPoint_m3048133387 (SplineInterpolator_t4279526764 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___quat1, float ___timeInSeconds2, Vector2_t2243707579  ___easeInOut3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::SetInput()
extern "C"  void SplineInterpolator_SetInput_m1132661849 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::SetExplicitMode()
extern "C"  void SplineInterpolator_SetExplicitMode_m3732626524 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::SetAutoCloseMode(System.Single)
extern "C"  void SplineInterpolator_SetAutoCloseMode_m3141915810 (SplineInterpolator_t4279526764 * __this, float ___joiningPointTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SplineInterpolator::Update()
extern "C"  void SplineInterpolator_Update_m3640191798 (SplineInterpolator_t4279526764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion SplineInterpolator::GetSquad(System.Int32,System.Single)
extern "C"  Quaternion_t4030073918  SplineInterpolator_GetSquad_m61030101 (SplineInterpolator_t4279526764 * __this, int32_t ___idxFirstPoint0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineInterpolator::GetHermiteInternal(System.Int32,System.Single)
extern "C"  Vector3_t2243707580  SplineInterpolator_GetHermiteInternal_m3340692244 (SplineInterpolator_t4279526764 * __this, int32_t ___idxFirstPoint0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SplineInterpolator::GetHermiteAtTime(System.Single)
extern "C"  Vector3_t2243707580  SplineInterpolator_GetHermiteAtTime_m3030429876 (SplineInterpolator_t4279526764 * __this, float ___timeParam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
