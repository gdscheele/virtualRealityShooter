﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySprite/JellyCollider
struct JellyCollider_t1548403138;
// UnityEngine.Collider
struct Collider_t3497673348;
// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_JellySpriteReferencePoint291100992.h"

// System.Void JellySprite/JellyCollider::.ctor()
extern "C"  void JellyCollider__ctor_m1540443547 (JellyCollider_t1548403138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider JellySprite/JellyCollider::get_Collider()
extern "C"  Collider_t3497673348 * JellyCollider_get_Collider_m209016914 (JellyCollider_t1548403138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollider::set_Collider(UnityEngine.Collider)
extern "C"  void JellyCollider_set_Collider_m2259923861 (JellyCollider_t1548403138 * __this, Collider_t3497673348 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JellySpriteReferencePoint JellySprite/JellyCollider::get_ReferencePoint()
extern "C"  JellySpriteReferencePoint_t291100992 * JellyCollider_get_ReferencePoint_m2642290490 (JellyCollider_t1548403138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollider::set_ReferencePoint(JellySpriteReferencePoint)
extern "C"  void JellyCollider_set_ReferencePoint_m1353394317 (JellyCollider_t1548403138 * __this, JellySpriteReferencePoint_t291100992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
