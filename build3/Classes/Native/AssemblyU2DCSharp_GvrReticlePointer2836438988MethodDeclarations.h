﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrReticlePointer
struct GvrReticlePointer_t2836438988;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

// System.Void GvrReticlePointer::.ctor()
extern "C"  void GvrReticlePointer__ctor_m2416667207 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::Start()
extern "C"  void GvrReticlePointer_Start_m2978962707 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::Update()
extern "C"  void GvrReticlePointer_Update_m4104086346 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnInputModuleEnabled()
extern "C"  void GvrReticlePointer_OnInputModuleEnabled_m2632683031 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnInputModuleDisabled()
extern "C"  void GvrReticlePointer_OnInputModuleDisabled_m2458651700 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnPointerEnter(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Ray,System.Boolean)
extern "C"  void GvrReticlePointer_OnPointerEnter_m3884404516 (GvrReticlePointer_t2836438988 * __this, GameObject_t1756533147 * ___targetObject0, Vector3_t2243707580  ___intersectionPosition1, Ray_t2469606224  ___intersectionRay2, bool ___isInteractive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnPointerHover(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Ray,System.Boolean)
extern "C"  void GvrReticlePointer_OnPointerHover_m1074116024 (GvrReticlePointer_t2836438988 * __this, GameObject_t1756533147 * ___targetObject0, Vector3_t2243707580  ___intersectionPosition1, Ray_t2469606224  ___intersectionRay2, bool ___isInteractive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnPointerExit(UnityEngine.GameObject)
extern "C"  void GvrReticlePointer_OnPointerExit_m2968041145 (GvrReticlePointer_t2836438988 * __this, GameObject_t1756533147 * ___targetObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnPointerClickDown()
extern "C"  void GvrReticlePointer_OnPointerClickDown_m1239275741 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::OnPointerClickUp()
extern "C"  void GvrReticlePointer_OnPointerClickUp_m3441393552 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GvrReticlePointer::GetMaxPointerDistance()
extern "C"  float GvrReticlePointer_GetMaxPointerDistance_m505711371 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::GetPointerRadius(System.Single&,System.Single&)
extern "C"  void GvrReticlePointer_GetPointerRadius_m2532597208 (GvrReticlePointer_t2836438988 * __this, float* ___innerRadius0, float* ___outerRadius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::CreateReticleVertices()
extern "C"  void GvrReticlePointer_CreateReticleVertices_m2589246972 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::UpdateDiameters()
extern "C"  void GvrReticlePointer_UpdateDiameters_m731949732 (GvrReticlePointer_t2836438988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrReticlePointer::SetPointerTarget(UnityEngine.Vector3,System.Boolean)
extern "C"  void GvrReticlePointer_SetPointerTarget_m838886897 (GvrReticlePointer_t2836438988 * __this, Vector3_t2243707580  ___target0, bool ___interactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
