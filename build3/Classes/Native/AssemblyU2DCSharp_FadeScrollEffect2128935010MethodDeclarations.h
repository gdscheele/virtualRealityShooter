﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FadeScrollEffect
struct FadeScrollEffect_t2128935010;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseScrollEffect_UpdateData3671771611.h"

// System.Void FadeScrollEffect::.ctor()
extern "C"  void FadeScrollEffect__ctor_m3546210939 (FadeScrollEffect_t2128935010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeScrollEffect::ApplyEffect(BaseScrollEffect/UpdateData)
extern "C"  void FadeScrollEffect_ApplyEffect_m2972107489 (FadeScrollEffect_t2128935010 * __this, UpdateData_t3671771611  ___updateData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
