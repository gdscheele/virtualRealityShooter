﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySprite
struct JellySprite_t2775849197;
// System.Collections.Generic.List`1<JellySprite/ReferencePoint>
struct List_1_t3979789937;
// JellySprite/ReferencePoint
struct ReferencePoint_t315701509;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_JellySprite_ReferencePoint315701509.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void JellySprite::.ctor()
extern "C"  void JellySprite__ctor_m2359109712 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JellySprite/ReferencePoint> JellySprite::get_ReferencePoints()
extern "C"  List_1_t3979789937 * JellySprite_get_ReferencePoints_m133331215 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JellySprite/ReferencePoint JellySprite::get_CentralPoint()
extern "C"  ReferencePoint_t315701509 * JellySprite_get_CentralPoint_m2594572448 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::Awake()
extern "C"  void JellySprite_Awake_m2120918137 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::Start()
extern "C"  void JellySprite_Start_m3178806728 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CalculateInitialOffsets()
extern "C"  void JellySprite_CalculateInitialOffsets_m600934780 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::OnEnable()
extern "C"  void JellySprite_OnEnable_m1942063240 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::OnDisable()
extern "C"  void JellySprite_OnDisable_m3304568185 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::OnDestroy()
extern "C"  void JellySprite_OnDestroy_m1287803931 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CreateRigidBodiesCircle(UnityEngine.Bounds)
extern "C"  void JellySprite_CreateRigidBodiesCircle_m142766551 (JellySprite_t2775849197 * __this, Bounds_t3033363703  ___spriteBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CreateRigidBodiesTriangle(UnityEngine.Bounds)
extern "C"  void JellySprite_CreateRigidBodiesTriangle_m3753807563 (JellySprite_t2775849197 * __this, Bounds_t3033363703  ___spriteBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CreateRigidBodiesRectangle(UnityEngine.Bounds)
extern "C"  void JellySprite_CreateRigidBodiesRectangle_m215354892 (JellySprite_t2775849197 * __this, Bounds_t3033363703  ___spriteBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CreateRigidBodiesGrid(UnityEngine.Bounds)
extern "C"  void JellySprite_CreateRigidBodiesGrid_m321410521 (JellySprite_t2775849197 * __this, Bounds_t3033363703  ___spriteBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CreateRigidBodiesFree(UnityEngine.Bounds)
extern "C"  void JellySprite_CreateRigidBodiesFree_m4289078761 (JellySprite_t2775849197 * __this, Bounds_t3033363703  ___spriteBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::UpdateRotationLock()
extern "C"  void JellySprite_UpdateRotationLock_m1890468362 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JellySprite/ReferencePoint JellySprite::AddReferencePoint(UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  ReferencePoint_t315701509 * JellySprite_AddReferencePoint_m1917592925 (JellySprite_t2775849197 * __this, Vector3_t2243707580  ___position0, float ___radius1, bool ___lockRotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::AttachPoint(JellySprite/ReferencePoint,JellySprite/ReferencePoint)
extern "C"  void JellySprite_AttachPoint_m491801963 (JellySprite_t2775849197 * __this, ReferencePoint_t315701509 * ___point10, ReferencePoint_t315701509 * ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::CalculateWeightingValues()
extern "C"  void JellySprite_CalculateWeightingValues_m1720914872 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::SetupCollisions()
extern "C"  void JellySprite_SetupCollisions_m743690548 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::InitVertices(UnityEngine.Bounds)
extern "C"  void JellySprite_InitVertices_m3042368905 (JellySprite_t2775849197 * __this, Bounds_t3033363703  ___spriteBounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::ReInitMaterial()
extern "C"  void JellySprite_ReInitMaterial_m3846626712 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::UpdateTextureCoords()
extern "C"  void JellySprite_UpdateTextureCoords_m2860714582 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::SetFlipHorizontal(System.Boolean)
extern "C"  void JellySprite_SetFlipHorizontal_m3694124926 (JellySprite_t2775849197 * __this, bool ___flipHorizontal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::SetFlipVertical(System.Boolean)
extern "C"  void JellySprite_SetFlipVertical_m2321015120 (JellySprite_t2775849197 * __this, bool ___flipVertical0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::SetPosition(UnityEngine.Vector3,System.Boolean)
extern "C"  void JellySprite_SetPosition_m1222794237 (JellySprite_t2775849197 * __this, Vector3_t2243707580  ___position0, bool ___resetVelocity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::Reset(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void JellySprite_Reset_m2761196965 (JellySprite_t2775849197 * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::SetKinematic(System.Boolean,System.Boolean)
extern "C"  void JellySprite_SetKinematic_m2062437929 (JellySprite_t2775849197 * __this, bool ___isKinematic0, bool ___centralPointOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::Rotate(System.Single)
extern "C"  void JellySprite_Rotate_m1750019638 (JellySprite_t2775849197 * __this, float ___angleChange0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JellySprite::IsGrounded(UnityEngine.LayerMask,System.Int32)
extern "C"  bool JellySprite_IsGrounded_m515094615 (JellySprite_t2775849197 * __this, LayerMask_t3188175821  ___groundLayer0, int32_t ___minGroundedBodies1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::FixupTextureCoordinates(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void JellySprite_FixupTextureCoordinates_m4228557170 (JellySprite_t2775849197 * __this, Vector2_t2243707579  ___minTextureCoords0, Vector2_t2243707579  ___maxTextureCoords1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::ResizeAttachPoints()
extern "C"  void JellySprite_ResizeAttachPoints_m3706436568 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::OnCopyToFreeModeSelected()
extern "C"  void JellySprite_OnCopyToFreeModeSelected_m648833273 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::InitMesh()
extern "C"  void JellySprite_InitMesh_m1332708805 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::UpdateMesh()
extern "C"  void JellySprite_UpdateMesh_m3557699958 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JellySprite::GetCentralPointIndex()
extern "C"  int32_t JellySprite_GetCentralPointIndex_m3278311617 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::UpdateAttachPoints()
extern "C"  void JellySprite_UpdateAttachPoints_m1767547113 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::AddForce(UnityEngine.Vector2)
extern "C"  void JellySprite_AddForce_m2243191952 (JellySprite_t2775849197 * __this, Vector2_t2243707579  ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void JellySprite_AddForceAtPosition_m1545373808 (JellySprite_t2775849197 * __this, Vector2_t2243707579  ___force0, Vector2_t2243707579  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::RefreshMesh()
extern "C"  void JellySprite_RefreshMesh_m788877314 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::InitMass()
extern "C"  void JellySprite_InitMass_m1332703512 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::UpdateJoints()
extern "C"  void JellySprite_UpdateJoints_m992556626 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::Scale(System.Single,System.Boolean)
extern "C"  void JellySprite_Scale_m61914782 (JellySprite_t2775849197 * __this, float ___scaleRatio0, bool ___scaleAttachedObjects1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::AddAttachPoint(UnityEngine.Transform)
extern "C"  void JellySprite_AddAttachPoint_m2368533445 (JellySprite_t2775849197 * __this, Transform_t3275118058 * ___newAttachedObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::WakeUp()
extern "C"  void JellySprite_WakeUp_m2396871545 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::Update()
extern "C"  void JellySprite_Update_m440125005 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::AddFreeModeBodyDefinition(UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  void JellySprite_AddFreeModeBodyDefinition_m1336448481 (JellySprite_t2775849197 * __this, Vector2_t2243707579  ___position0, float ___radius1, bool ___kinematic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::DrawSphereWithCentreConnection(UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  void JellySprite_DrawSphereWithCentreConnection_m104048573 (JellySprite_t2775849197 * __this, Vector3_t2243707580  ___position0, float ___radius1, bool ___kinematic2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::DrawCentreConnection(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void JellySprite_DrawCentreConnection_m184623345 (JellySprite_t2775849197 * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___centre1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::DrawCentreConnectionWithoutRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void JellySprite_DrawCentreConnectionWithoutRotation_m2135469593 (JellySprite_t2775849197 * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___centre1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite::OnDrawGizmosSelected()
extern "C"  void JellySprite_OnDrawGizmosSelected_m268023833 (JellySprite_t2775849197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
