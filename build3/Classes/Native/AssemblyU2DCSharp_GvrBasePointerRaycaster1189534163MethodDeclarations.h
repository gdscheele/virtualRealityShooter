﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrBasePointerRaycaster
struct GvrBasePointerRaycaster_t1189534163;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"

// System.Void GvrBasePointerRaycaster::.ctor()
extern "C"  void GvrBasePointerRaycaster__ctor_m4174019434 (GvrBasePointerRaycaster_t1189534163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GvrBasePointerRaycaster::get_MaxPointerDistance()
extern "C"  float GvrBasePointerRaycaster_get_MaxPointerDistance_m771532845 (GvrBasePointerRaycaster_t1189534163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GvrBasePointerRaycaster::IsPointerAvailable()
extern "C"  bool GvrBasePointerRaycaster_IsPointerAvailable_m2792971214 (GvrBasePointerRaycaster_t1189534163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray GvrBasePointerRaycaster::GetLastRay()
extern "C"  Ray_t2469606224  GvrBasePointerRaycaster_GetLastRay_m1048711842 (GvrBasePointerRaycaster_t1189534163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray GvrBasePointerRaycaster::GetRay()
extern "C"  Ray_t2469606224  GvrBasePointerRaycaster_GetRay_m2992454774 (GvrBasePointerRaycaster_t1189534163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
