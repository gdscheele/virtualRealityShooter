﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PointController
struct PointController_t705534758;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileCollision
struct  ProjectileCollision_t1347523025  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ProjectileCollision::camera
	GameObject_t1756533147 * ___camera_2;
	// PointController ProjectileCollision::pointController
	PointController_t705534758 * ___pointController_3;
	// System.Boolean ProjectileCollision::deleted
	bool ___deleted_4;

public:
	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(ProjectileCollision_t1347523025, ___camera_2)); }
	inline GameObject_t1756533147 * get_camera_2() const { return ___camera_2; }
	inline GameObject_t1756533147 ** get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(GameObject_t1756533147 * value)
	{
		___camera_2 = value;
		Il2CppCodeGenWriteBarrier(&___camera_2, value);
	}

	inline static int32_t get_offset_of_pointController_3() { return static_cast<int32_t>(offsetof(ProjectileCollision_t1347523025, ___pointController_3)); }
	inline PointController_t705534758 * get_pointController_3() const { return ___pointController_3; }
	inline PointController_t705534758 ** get_address_of_pointController_3() { return &___pointController_3; }
	inline void set_pointController_3(PointController_t705534758 * value)
	{
		___pointController_3 = value;
		Il2CppCodeGenWriteBarrier(&___pointController_3, value);
	}

	inline static int32_t get_offset_of_deleted_4() { return static_cast<int32_t>(offsetof(ProjectileCollision_t1347523025, ___deleted_4)); }
	inline bool get_deleted_4() const { return ___deleted_4; }
	inline bool* get_address_of_deleted_4() { return &___deleted_4; }
	inline void set_deleted_4(bool value)
	{
		___deleted_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
