﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlobEyes
struct  BlobEyes_t1430531255  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.SpriteRenderer BlobEyes::m_SpriteRenderer
	SpriteRenderer_t1209076198 * ___m_SpriteRenderer_2;
	// UnityEngine.Sprite BlobEyes::m_ClosedSprite
	Sprite_t309593783 * ___m_ClosedSprite_3;
	// UnityEngine.Sprite BlobEyes::m_OpenSprite
	Sprite_t309593783 * ___m_OpenSprite_4;
	// System.Single BlobEyes::m_MinBlinkInterval
	float ___m_MinBlinkInterval_5;
	// System.Single BlobEyes::m_MaxBlinkInterval
	float ___m_MaxBlinkInterval_6;
	// System.Single BlobEyes::m_MinBlinkTime
	float ___m_MinBlinkTime_7;
	// System.Single BlobEyes::m_MaxBlinkTime
	float ___m_MaxBlinkTime_8;
	// UnityEngine.GameObject BlobEyes::m_PupilLeft
	GameObject_t1756533147 * ___m_PupilLeft_9;
	// UnityEngine.GameObject BlobEyes::m_PupilRight
	GameObject_t1756533147 * ___m_PupilRight_10;
	// System.Single BlobEyes::m_EyeRadius
	float ___m_EyeRadius_11;
	// System.Single BlobEyes::m_BlinkTimer
	float ___m_BlinkTimer_12;
	// System.Boolean BlobEyes::m_Blinking
	bool ___m_Blinking_13;
	// UnityEngine.Vector3 BlobEyes::m_LookDirection
	Vector3_t2243707580  ___m_LookDirection_14;
	// UnityEngine.Vector3 BlobEyes::m_PupilLeftCentre
	Vector3_t2243707580  ___m_PupilLeftCentre_15;
	// UnityEngine.Vector3 BlobEyes::m_PupilRightCentre
	Vector3_t2243707580  ___m_PupilRightCentre_16;
	// UnityEngine.Transform BlobEyes::m_LookTarget
	Transform_t3275118058 * ___m_LookTarget_17;

public:
	inline static int32_t get_offset_of_m_SpriteRenderer_2() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_SpriteRenderer_2)); }
	inline SpriteRenderer_t1209076198 * get_m_SpriteRenderer_2() const { return ___m_SpriteRenderer_2; }
	inline SpriteRenderer_t1209076198 ** get_address_of_m_SpriteRenderer_2() { return &___m_SpriteRenderer_2; }
	inline void set_m_SpriteRenderer_2(SpriteRenderer_t1209076198 * value)
	{
		___m_SpriteRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpriteRenderer_2, value);
	}

	inline static int32_t get_offset_of_m_ClosedSprite_3() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_ClosedSprite_3)); }
	inline Sprite_t309593783 * get_m_ClosedSprite_3() const { return ___m_ClosedSprite_3; }
	inline Sprite_t309593783 ** get_address_of_m_ClosedSprite_3() { return &___m_ClosedSprite_3; }
	inline void set_m_ClosedSprite_3(Sprite_t309593783 * value)
	{
		___m_ClosedSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_ClosedSprite_3, value);
	}

	inline static int32_t get_offset_of_m_OpenSprite_4() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_OpenSprite_4)); }
	inline Sprite_t309593783 * get_m_OpenSprite_4() const { return ___m_OpenSprite_4; }
	inline Sprite_t309593783 ** get_address_of_m_OpenSprite_4() { return &___m_OpenSprite_4; }
	inline void set_m_OpenSprite_4(Sprite_t309593783 * value)
	{
		___m_OpenSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_OpenSprite_4, value);
	}

	inline static int32_t get_offset_of_m_MinBlinkInterval_5() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_MinBlinkInterval_5)); }
	inline float get_m_MinBlinkInterval_5() const { return ___m_MinBlinkInterval_5; }
	inline float* get_address_of_m_MinBlinkInterval_5() { return &___m_MinBlinkInterval_5; }
	inline void set_m_MinBlinkInterval_5(float value)
	{
		___m_MinBlinkInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxBlinkInterval_6() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_MaxBlinkInterval_6)); }
	inline float get_m_MaxBlinkInterval_6() const { return ___m_MaxBlinkInterval_6; }
	inline float* get_address_of_m_MaxBlinkInterval_6() { return &___m_MaxBlinkInterval_6; }
	inline void set_m_MaxBlinkInterval_6(float value)
	{
		___m_MaxBlinkInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_MinBlinkTime_7() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_MinBlinkTime_7)); }
	inline float get_m_MinBlinkTime_7() const { return ___m_MinBlinkTime_7; }
	inline float* get_address_of_m_MinBlinkTime_7() { return &___m_MinBlinkTime_7; }
	inline void set_m_MinBlinkTime_7(float value)
	{
		___m_MinBlinkTime_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxBlinkTime_8() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_MaxBlinkTime_8)); }
	inline float get_m_MaxBlinkTime_8() const { return ___m_MaxBlinkTime_8; }
	inline float* get_address_of_m_MaxBlinkTime_8() { return &___m_MaxBlinkTime_8; }
	inline void set_m_MaxBlinkTime_8(float value)
	{
		___m_MaxBlinkTime_8 = value;
	}

	inline static int32_t get_offset_of_m_PupilLeft_9() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_PupilLeft_9)); }
	inline GameObject_t1756533147 * get_m_PupilLeft_9() const { return ___m_PupilLeft_9; }
	inline GameObject_t1756533147 ** get_address_of_m_PupilLeft_9() { return &___m_PupilLeft_9; }
	inline void set_m_PupilLeft_9(GameObject_t1756533147 * value)
	{
		___m_PupilLeft_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_PupilLeft_9, value);
	}

	inline static int32_t get_offset_of_m_PupilRight_10() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_PupilRight_10)); }
	inline GameObject_t1756533147 * get_m_PupilRight_10() const { return ___m_PupilRight_10; }
	inline GameObject_t1756533147 ** get_address_of_m_PupilRight_10() { return &___m_PupilRight_10; }
	inline void set_m_PupilRight_10(GameObject_t1756533147 * value)
	{
		___m_PupilRight_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_PupilRight_10, value);
	}

	inline static int32_t get_offset_of_m_EyeRadius_11() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_EyeRadius_11)); }
	inline float get_m_EyeRadius_11() const { return ___m_EyeRadius_11; }
	inline float* get_address_of_m_EyeRadius_11() { return &___m_EyeRadius_11; }
	inline void set_m_EyeRadius_11(float value)
	{
		___m_EyeRadius_11 = value;
	}

	inline static int32_t get_offset_of_m_BlinkTimer_12() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_BlinkTimer_12)); }
	inline float get_m_BlinkTimer_12() const { return ___m_BlinkTimer_12; }
	inline float* get_address_of_m_BlinkTimer_12() { return &___m_BlinkTimer_12; }
	inline void set_m_BlinkTimer_12(float value)
	{
		___m_BlinkTimer_12 = value;
	}

	inline static int32_t get_offset_of_m_Blinking_13() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_Blinking_13)); }
	inline bool get_m_Blinking_13() const { return ___m_Blinking_13; }
	inline bool* get_address_of_m_Blinking_13() { return &___m_Blinking_13; }
	inline void set_m_Blinking_13(bool value)
	{
		___m_Blinking_13 = value;
	}

	inline static int32_t get_offset_of_m_LookDirection_14() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_LookDirection_14)); }
	inline Vector3_t2243707580  get_m_LookDirection_14() const { return ___m_LookDirection_14; }
	inline Vector3_t2243707580 * get_address_of_m_LookDirection_14() { return &___m_LookDirection_14; }
	inline void set_m_LookDirection_14(Vector3_t2243707580  value)
	{
		___m_LookDirection_14 = value;
	}

	inline static int32_t get_offset_of_m_PupilLeftCentre_15() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_PupilLeftCentre_15)); }
	inline Vector3_t2243707580  get_m_PupilLeftCentre_15() const { return ___m_PupilLeftCentre_15; }
	inline Vector3_t2243707580 * get_address_of_m_PupilLeftCentre_15() { return &___m_PupilLeftCentre_15; }
	inline void set_m_PupilLeftCentre_15(Vector3_t2243707580  value)
	{
		___m_PupilLeftCentre_15 = value;
	}

	inline static int32_t get_offset_of_m_PupilRightCentre_16() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_PupilRightCentre_16)); }
	inline Vector3_t2243707580  get_m_PupilRightCentre_16() const { return ___m_PupilRightCentre_16; }
	inline Vector3_t2243707580 * get_address_of_m_PupilRightCentre_16() { return &___m_PupilRightCentre_16; }
	inline void set_m_PupilRightCentre_16(Vector3_t2243707580  value)
	{
		___m_PupilRightCentre_16 = value;
	}

	inline static int32_t get_offset_of_m_LookTarget_17() { return static_cast<int32_t>(offsetof(BlobEyes_t1430531255, ___m_LookTarget_17)); }
	inline Transform_t3275118058 * get_m_LookTarget_17() const { return ___m_LookTarget_17; }
	inline Transform_t3275118058 ** get_address_of_m_LookTarget_17() { return &___m_LookTarget_17; }
	inline void set_m_LookTarget_17(Transform_t3275118058 * value)
	{
		___m_LookTarget_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_LookTarget_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
