﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellyBridge
struct JellyBridge_t349677779;

#include "codegen/il2cpp-codegen.h"

// System.Void JellyBridge::.ctor()
extern "C"  void JellyBridge__ctor_m485867748 (JellyBridge_t349677779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellyBridge::Update()
extern "C"  void JellyBridge_Update_m3578506495 (JellyBridge_t349677779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
