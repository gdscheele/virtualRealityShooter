﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>
struct SortedDictionary_2_t893614575;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>
struct Node_t3770910803;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary2262176075.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1649657696_gshared (Enumerator_t2262176075 * __this, SortedDictionary_2_t893614575 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m1649657696(__this, ___dic0, method) ((  void (*) (Enumerator_t2262176075 *, SortedDictionary_2_t893614575 *, const MethodInfo*))Enumerator__ctor_m1649657696_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2926187759_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2926187759(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2926187759_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2449588468_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2449588468(__this, method) ((  Il2CppObject * (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2449588468_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3150796154_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3150796154(__this, method) ((  Il2CppObject * (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3150796154_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3365767886_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3365767886(__this, method) ((  Il2CppObject * (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3365767886_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1428461118_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1428461118(__this, method) ((  void (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1428461118_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1296315204  Enumerator_get_Current_m911506682_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m911506682(__this, method) ((  KeyValuePair_2_t1296315204  (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_get_Current_m911506682_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m143426854_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m143426854(__this, method) ((  bool (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_MoveNext_m143426854_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3308716969_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3308716969(__this, method) ((  void (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_Dispose_m3308716969_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Single,System.Object>::get_CurrentNode()
extern "C"  Node_t3770910803 * Enumerator_get_CurrentNode_m1051348614_gshared (Enumerator_t2262176075 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentNode_m1051348614(__this, method) ((  Node_t3770910803 * (*) (Enumerator_t2262176075 *, const MethodInfo*))Enumerator_get_CurrentNode_m1051348614_gshared)(__this, method)
