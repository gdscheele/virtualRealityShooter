﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.ColorCorrectionCurves
struct ColorCorrectionCurves_t3523901841;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern "C"  void ColorCorrectionCurves__ctor_m511465672 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern "C"  void ColorCorrectionCurves_Start_m126764864 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern "C"  void ColorCorrectionCurves_Awake_m4177498773 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern "C"  bool ColorCorrectionCurves_CheckResources_m3289829527 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern "C"  void ColorCorrectionCurves_UpdateParameters_m2656372395 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern "C"  void ColorCorrectionCurves_UpdateTextures_m1584591877 (ColorCorrectionCurves_t3523901841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ColorCorrectionCurves_OnRenderImage_m2845115816 (ColorCorrectionCurves_t3523901841 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
