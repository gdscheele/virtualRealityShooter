﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct Predicate_1_t4034252615;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4286863759_gshared (Predicate_1_t4034252615 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m4286863759(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t4034252615 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m4286863759_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3428923695_gshared (Predicate_1_t4034252615 * __this, KeyValuePair_2_t1296315204  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m3428923695(__this, ___obj0, method) ((  bool (*) (Predicate_1_t4034252615 *, KeyValuePair_2_t1296315204 , const MethodInfo*))Predicate_1_Invoke_m3428923695_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2733795630_gshared (Predicate_1_t4034252615 * __this, KeyValuePair_2_t1296315204  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m2733795630(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t4034252615 *, KeyValuePair_2_t1296315204 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2733795630_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4162272901_gshared (Predicate_1_t4034252615 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m4162272901(__this, ___result0, method) ((  bool (*) (Predicate_1_t4034252615 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m4162272901_gshared)(__this, ___result0, method)
