﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrPointerManager
struct GvrPointerManager_t2205699129;
// IGvrPointer
struct IGvrPointer_t3900708915;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrPointerManager::.ctor()
extern "C"  void GvrPointerManager__ctor_m2938703452 (GvrPointerManager_t2205699129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IGvrPointer GvrPointerManager::get_Pointer()
extern "C"  Il2CppObject * GvrPointerManager_get_Pointer_m1624108672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrPointerManager::set_Pointer(IGvrPointer)
extern "C"  void GvrPointerManager_set_Pointer_m1915017235 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrPointerManager::OnPointerCreated(IGvrPointer)
extern "C"  void GvrPointerManager_OnPointerCreated_m162543247 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___createdPointer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrPointerManager::Awake()
extern "C"  void GvrPointerManager_Awake_m4178990469 (GvrPointerManager_t2205699129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrPointerManager::OnDestroy()
extern "C"  void GvrPointerManager_OnDestroy_m3537033295 (GvrPointerManager_t2205699129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
