﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t3857795355;

#include "AssemblyU2DCSharp_JellySprite2775849197.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityJellySprite
struct  UnityJellySprite_t1914834834  : public JellySprite_t2775849197
{
public:
	// UnityEngine.Sprite UnityJellySprite::m_Sprite
	Sprite_t309593783 * ___m_Sprite_57;

public:
	inline static int32_t get_offset_of_m_Sprite_57() { return static_cast<int32_t>(offsetof(UnityJellySprite_t1914834834, ___m_Sprite_57)); }
	inline Sprite_t309593783 * get_m_Sprite_57() const { return ___m_Sprite_57; }
	inline Sprite_t309593783 ** get_address_of_m_Sprite_57() { return &___m_Sprite_57; }
	inline void set_m_Sprite_57(Sprite_t309593783 * value)
	{
		___m_Sprite_57 = value;
		Il2CppCodeGenWriteBarrier(&___m_Sprite_57, value);
	}
};

struct UnityJellySprite_t1914834834_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Material> UnityJellySprite::s_MaterialList
	List_1_t3857795355 * ___s_MaterialList_58;

public:
	inline static int32_t get_offset_of_s_MaterialList_58() { return static_cast<int32_t>(offsetof(UnityJellySprite_t1914834834_StaticFields, ___s_MaterialList_58)); }
	inline List_1_t3857795355 * get_s_MaterialList_58() const { return ___s_MaterialList_58; }
	inline List_1_t3857795355 ** get_address_of_s_MaterialList_58() { return &___s_MaterialList_58; }
	inline void set_s_MaterialList_58(List_1_t3857795355 * value)
	{
		___s_MaterialList_58 = value;
		Il2CppCodeGenWriteBarrier(&___s_MaterialList_58, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
