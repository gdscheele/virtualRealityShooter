﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnEndCallback
struct OnEndCallback_t2203472397;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void OnEndCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnEndCallback__ctor_m2669111996 (OnEndCallback_t2203472397 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnEndCallback::Invoke()
extern "C"  void OnEndCallback_Invoke_m148920118 (OnEndCallback_t2203472397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnEndCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnEndCallback_BeginInvoke_m3693661803 (OnEndCallback_t2203472397 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnEndCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnEndCallback_EndInvoke_m3805920814 (OnEndCallback_t2203472397 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
