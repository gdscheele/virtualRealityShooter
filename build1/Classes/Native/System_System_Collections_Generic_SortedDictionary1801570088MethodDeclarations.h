﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary1846780193MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define NodeHelper__ctor_m2556573180(__this, ___cmp0, method) ((  void (*) (NodeHelper_t1801570088 *, Il2CppObject*, const MethodInfo*))NodeHelper__ctor_m3658842787_gshared)(__this, ___cmp0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::.cctor()
#define NodeHelper__cctor_m4259049880(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NodeHelper__cctor_m4163778587_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::Compare(TKey,System.Collections.Generic.RBTree/Node)
#define NodeHelper_Compare_m2922102391(__this, ___key0, ___node1, method) ((  int32_t (*) (NodeHelper_t1801570088 *, float, Node_t2499136326 *, const MethodInfo*))NodeHelper_Compare_m852104552_gshared)(__this, ___key0, ___node1, method)
// System.Collections.Generic.RBTree/Node System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::CreateNode(TKey)
#define NodeHelper_CreateNode_m3736922117(__this, ___key0, method) ((  Node_t2499136326 * (*) (NodeHelper_t1801570088 *, float, const MethodInfo*))NodeHelper_CreateNode_m4169738924_gshared)(__this, ___key0, method)
// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::GetHelper(System.Collections.Generic.IComparer`1<TKey>)
#define NodeHelper_GetHelper_m1742178096(__this /* static, unused */, ___cmp0, method) ((  NodeHelper_t1801570088 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))NodeHelper_GetHelper_m1812397633_gshared)(__this /* static, unused */, ___cmp0, method)
