﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// shootScript
struct shootScript_t955866588;

#include "codegen/il2cpp-codegen.h"

// System.Void shootScript::.ctor()
extern "C"  void shootScript__ctor_m1778238031 (shootScript_t955866588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shootScript::Start()
extern "C"  void shootScript_Start_m4046050659 (shootScript_t955866588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shootScript::Update()
extern "C"  void shootScript_Update_m3414958138 (shootScript_t955866588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void shootScript::shootRock()
extern "C"  void shootScript_shootRock_m1990796453 (shootScript_t955866588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
