﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.BlurOptimized
struct BlurOptimized_t2905677972;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern "C"  void BlurOptimized__ctor_m3670768283 (BlurOptimized_t2905677972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern "C"  bool BlurOptimized_CheckResources_m3621608060 (BlurOptimized_t2905677972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern "C"  void BlurOptimized_OnDisable_m3536944964 (BlurOptimized_t2905677972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void BlurOptimized_OnRenderImage_m1433189855 (BlurOptimized_t2905677972 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
