﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t2375210762;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2985503331;
// UnityStandardAssets.Utility.WaypointProgressTracker
struct WaypointProgressTracker_t2206407592;
// VideoControlsManager
struct VideoControlsManager_t3010523296;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t673526704;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.UI.RawImage[]
struct RawImageU5BU5D_t3856578040;
// UnityEngine.UI.Slider[]
struct SliderU5BU5D_t1144817634;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;
// ScrubberEvents[]
struct ScrubberEventsU5BU5D_t3322287636;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// VideoControlsManager/<DoAppear>c__Iterator0
struct U3CDoAppearU3Ec__Iterator0_t2331568912;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// System.Object
struct Il2CppObject;
// VideoControlsManager/<DoFade>c__Iterator1
struct U3CDoFadeU3Ec__Iterator1_t4193299950;
// VideoPlayerReference
struct VideoPlayerReference_t1150574547;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2375210762.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2375210762MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2985503331MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2985503331.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Waypo318924311.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Waypo318924311MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gizmos2256232573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2206407592.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp2206407592MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp1659392090.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Utility_Wayp1659392090MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoControlsManager3010523296.h"
#include "AssemblyU2DCSharp_VideoControlsManager3010523296MethodDeclarations.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture673526704.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_ScrubberEvents2429506345MethodDeclarations.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture673526704MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScrubberEvents2429506345.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Int64909078037.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoAppear2331568912MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoAppear2331568912.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoFadeU34193299950MethodDeclarations.h"
#include "AssemblyU2DCSharp_VideoControlsManager_U3CDoFadeU34193299950.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_VideoPlayerReference1150574547.h"
#include "AssemblyU2DCSharp_VideoPlayerReference1150574547MethodDeclarations.h"

// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3978412804(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.UI.Text>()
#define Component_GetComponentsInChildren_TisText_t356221433_m324169492(__this, method) ((  TextU5BU5D_t4216439300* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared (Component_t3819376471 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3229891463(__this, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.UI.RawImage>(System.Boolean)
#define Component_GetComponentsInChildren_TisRawImage_t2749640213_m652723431(__this, p0, method) ((  RawImageU5BU5D_t3856578040* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.UI.Slider>(System.Boolean)
#define Component_GetComponentsInChildren_TisSlider_t297367283_m1167155165(__this, p0, method) ((  SliderU5BU5D_t1144817634* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.UI.Image>()
#define Component_GetComponentsInChildren_TisImage_t2042527209_m1009123198(__this, method) ((  ImageU5BU5D_t590162004* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.RectTransform>(System.Boolean)
#define Component_GetComponentsInChildren_TisRectTransform_t3349966182_m1564438138(__this, p0, method) ((  RectTransformU5BU5D_t3948421699* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<ScrubberEvents>(System.Boolean)
#define Component_GetComponentsInChildren_TisScrubberEvents_t2429506345_m429218110(__this, p0, method) ((  ScrubberEventsU5BU5D_t3322287636* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3229891463_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
#define Component_GetComponent_TisCanvasGroup_t3296560743_m846564447(__this, method) ((  CanvasGroup_t3296560743 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m3356814266_gshared (Component_t3819376471 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m3356814266(__this, p0, method) ((  Il2CppObject * (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m3356814266_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponentInChildren<VideoControlsManager>(System.Boolean)
#define Component_GetComponentInChildren_TisVideoControlsManager_t3010523296_m453748968(__this, p0, method) ((  VideoControlsManager_t3010523296 * (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m3356814266_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.Utility.WaypointCircuit::.ctor()
extern Il2CppClass* WaypointList_t2985503331_il2cpp_TypeInfo_var;
extern const uint32_t WaypointCircuit__ctor_m2047496194_MetadataUsageId;
extern "C"  void WaypointCircuit__ctor_m2047496194 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointCircuit__ctor_m2047496194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WaypointList_t2985503331 * L_0 = (WaypointList_t2985503331 *)il2cpp_codegen_object_new(WaypointList_t2985503331_il2cpp_TypeInfo_var);
		WaypointList__ctor_m2332832734(L_0, /*hidden argument*/NULL);
		__this->set_waypointList_2(L_0);
		__this->set_smoothRoute_3((bool)1);
		__this->set_editorVisualisationSubsteps_7((100.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.Utility.WaypointCircuit::get_Length()
extern "C"  float WaypointCircuit_get_Length_m1426297875 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CLengthU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::set_Length(System.Single)
extern "C"  void WaypointCircuit_set_Length_m3662637174 (WaypointCircuit_t2375210762 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CLengthU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit::get_Waypoints()
extern "C"  TransformU5BU5D_t3764228911* WaypointCircuit_get_Waypoints_m1083332871 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	{
		WaypointList_t2985503331 * L_0 = __this->get_waypointList_2();
		NullCheck(L_0);
		TransformU5BU5D_t3764228911* L_1 = L_0->get_items_1();
		return L_1;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::Awake()
extern "C"  void WaypointCircuit_Awake_m2250443785 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	{
		TransformU5BU5D_t3764228911* L_0 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		WaypointCircuit_CachePositionsAndDistances_m3412157573(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		TransformU5BU5D_t3764228911* L_1 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		__this->set_numPoints_4((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))));
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointCircuit::GetRoutePoint(System.Single)
extern "C"  RoutePoint_t318924311  WaypointCircuit_GetRoutePoint_m1379232192 (WaypointCircuit_t2375210762 * __this, float ___dist0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = ___dist0;
		Vector3_t2243707580  L_1 = WaypointCircuit_GetRoutePosition_m1847105981(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ___dist0;
		Vector3_t2243707580  L_3 = WaypointCircuit_GetRoutePosition_m1847105981(__this, ((float)((float)L_2+(float)(0.1f))), /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t2243707580  L_4 = V_1;
		Vector3_t2243707580  L_5 = V_0;
		Vector3_t2243707580  L_6 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		Vector3_t2243707580  L_8 = Vector3_get_normalized_m936072361((&V_2), /*hidden argument*/NULL);
		RoutePoint_t318924311  L_9;
		memset(&L_9, 0, sizeof(L_9));
		RoutePoint__ctor_m3125093342(&L_9, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::GetRoutePosition(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t WaypointCircuit_GetRoutePosition_m1847105981_MetadataUsageId;
extern "C"  Vector3_t2243707580  WaypointCircuit_GetRoutePosition_m1847105981 (WaypointCircuit_t2375210762 * __this, float ___dist0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointCircuit_GetRoutePosition_m1847105981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = WaypointCircuit_get_Length_m1426297875(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		SingleU5BU5D_t577127397* L_1 = __this->get_distances_6();
		SingleU5BU5D_t577127397* L_2 = __this->get_distances_6();
		NullCheck(L_2);
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))-(int32_t)1));
		float L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		WaypointCircuit_set_Length_m3662637174(__this, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		float L_5 = ___dist0;
		float L_6 = WaypointCircuit_get_Length_m1426297875(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Repeat_m943844734(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		___dist0 = L_7;
		goto IL_0040;
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0040:
	{
		SingleU5BU5D_t577127397* L_9 = __this->get_distances_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		float L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		float L_13 = ___dist0;
		if ((((float)L_12) < ((float)L_13)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_14 = V_0;
		int32_t L_15 = __this->get_numPoints_4();
		int32_t L_16 = __this->get_numPoints_4();
		__this->set_p1n_10(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)1))+(int32_t)L_15))%(int32_t)L_16)));
		int32_t L_17 = V_0;
		__this->set_p2n_11(L_17);
		SingleU5BU5D_t577127397* L_18 = __this->get_distances_6();
		int32_t L_19 = __this->get_p1n_10();
		NullCheck(L_18);
		int32_t L_20 = L_19;
		float L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		SingleU5BU5D_t577127397* L_22 = __this->get_distances_6();
		int32_t L_23 = __this->get_p2n_11();
		NullCheck(L_22);
		int32_t L_24 = L_23;
		float L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		float L_26 = ___dist0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_27 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, L_21, L_25, L_26, /*hidden argument*/NULL);
		__this->set_i_13(L_27);
		bool L_28 = __this->get_smoothRoute_3();
		if (!L_28)
		{
			goto IL_016c;
		}
	}
	{
		int32_t L_29 = V_0;
		int32_t L_30 = __this->get_numPoints_4();
		int32_t L_31 = __this->get_numPoints_4();
		__this->set_p0n_9(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29-(int32_t)2))+(int32_t)L_30))%(int32_t)L_31)));
		int32_t L_32 = V_0;
		int32_t L_33 = __this->get_numPoints_4();
		__this->set_p3n_12(((int32_t)((int32_t)((int32_t)((int32_t)L_32+(int32_t)1))%(int32_t)L_33)));
		int32_t L_34 = __this->get_p2n_11();
		int32_t L_35 = __this->get_numPoints_4();
		__this->set_p2n_11(((int32_t)((int32_t)L_34%(int32_t)L_35)));
		Vector3U5BU5D_t1172311765* L_36 = __this->get_points_5();
		int32_t L_37 = __this->get_p0n_9();
		NullCheck(L_36);
		__this->set_P0_14((*(Vector3_t2243707580 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))));
		Vector3U5BU5D_t1172311765* L_38 = __this->get_points_5();
		int32_t L_39 = __this->get_p1n_10();
		NullCheck(L_38);
		__this->set_P1_15((*(Vector3_t2243707580 *)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))));
		Vector3U5BU5D_t1172311765* L_40 = __this->get_points_5();
		int32_t L_41 = __this->get_p2n_11();
		NullCheck(L_40);
		__this->set_P2_16((*(Vector3_t2243707580 *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))));
		Vector3U5BU5D_t1172311765* L_42 = __this->get_points_5();
		int32_t L_43 = __this->get_p3n_12();
		NullCheck(L_42);
		__this->set_P3_17((*(Vector3_t2243707580 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43)))));
		Vector3_t2243707580  L_44 = __this->get_P0_14();
		Vector3_t2243707580  L_45 = __this->get_P1_15();
		Vector3_t2243707580  L_46 = __this->get_P2_16();
		Vector3_t2243707580  L_47 = __this->get_P3_17();
		float L_48 = __this->get_i_13();
		Vector3_t2243707580  L_49 = WaypointCircuit_CatmullRom_m3336761625(__this, L_44, L_45, L_46, L_47, L_48, /*hidden argument*/NULL);
		return L_49;
	}

IL_016c:
	{
		int32_t L_50 = V_0;
		int32_t L_51 = __this->get_numPoints_4();
		int32_t L_52 = __this->get_numPoints_4();
		__this->set_p1n_10(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_50-(int32_t)1))+(int32_t)L_51))%(int32_t)L_52)));
		int32_t L_53 = V_0;
		__this->set_p2n_11(L_53);
		Vector3U5BU5D_t1172311765* L_54 = __this->get_points_5();
		int32_t L_55 = __this->get_p1n_10();
		NullCheck(L_54);
		Vector3U5BU5D_t1172311765* L_56 = __this->get_points_5();
		int32_t L_57 = __this->get_p2n_11();
		NullCheck(L_56);
		float L_58 = __this->get_i_13();
		Vector3_t2243707580  L_59 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, (*(Vector3_t2243707580 *)((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_55)))), (*(Vector3_t2243707580 *)((L_56)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_57)))), L_58, /*hidden argument*/NULL);
		return L_59;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  WaypointCircuit_CatmullRom_m3336761625 (WaypointCircuit_t2375210762 * __this, Vector3_t2243707580  ___p00, Vector3_t2243707580  ___p11, Vector3_t2243707580  ___p22, Vector3_t2243707580  ___p33, float ___i4, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___p11;
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (2.0f), L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___p00;
		Vector3_t2243707580  L_3 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = ___p22;
		Vector3_t2243707580  L_5 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ___i4;
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = ___p00;
		Vector3_t2243707580  L_10 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (2.0f), L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = ___p11;
		Vector3_t2243707580  L_12 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (5.0f), L_11, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = ___p22;
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (4.0f), L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = ___p33;
		Vector3_t2243707580  L_18 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		float L_19 = ___i4;
		Vector3_t2243707580  L_20 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		float L_21 = ___i4;
		Vector3_t2243707580  L_22 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_8, L_22, /*hidden argument*/NULL);
		Vector3_t2243707580  L_24 = ___p00;
		Vector3_t2243707580  L_25 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = ___p11;
		Vector3_t2243707580  L_27 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (3.0f), L_26, /*hidden argument*/NULL);
		Vector3_t2243707580  L_28 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		Vector3_t2243707580  L_29 = ___p22;
		Vector3_t2243707580  L_30 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (3.0f), L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		Vector3_t2243707580  L_32 = ___p33;
		Vector3_t2243707580  L_33 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		float L_34 = ___i4;
		Vector3_t2243707580  L_35 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		float L_36 = ___i4;
		Vector3_t2243707580  L_37 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		float L_38 = ___i4;
		Vector3_t2243707580  L_39 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_23, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (0.5f), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::CachePositionsAndDistances()
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t WaypointCircuit_CachePositionsAndDistances_m3412157573_MetadataUsageId;
extern "C"  void WaypointCircuit_CachePositionsAndDistances_m3412157573 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointCircuit_CachePositionsAndDistances_m3412157573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	Transform_t3275118058 * V_2 = NULL;
	Transform_t3275118058 * V_3 = NULL;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		TransformU5BU5D_t3764228911* L_0 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		__this->set_points_5(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)1)))));
		TransformU5BU5D_t3764228911* L_1 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		__this->set_distances_6(((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))+(int32_t)1)))));
		V_0 = (0.0f);
		V_1 = 0;
		goto IL_00ce;
	}

IL_0037:
	{
		TransformU5BU5D_t3764228911* L_2 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		TransformU5BU5D_t3764228911* L_4 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		NullCheck(L_2);
		int32_t L_5 = ((int32_t)((int32_t)L_3%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		Transform_t3275118058 * L_6 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		TransformU5BU5D_t3764228911* L_7 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		TransformU5BU5D_t3764228911* L_9 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		NullCheck(L_7);
		int32_t L_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_8+(int32_t)1))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))));
		Transform_t3275118058 * L_11 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		Transform_t3275118058 * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ca;
		}
	}
	{
		Transform_t3275118058 * L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ca;
		}
	}
	{
		Transform_t3275118058 * L_16 = V_2;
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		Transform_t3275118058 * L_18 = V_3;
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		Vector3U5BU5D_t1172311765* L_20 = __this->get_points_5();
		int32_t L_21 = V_1;
		NullCheck(L_20);
		TransformU5BU5D_t3764228911* L_22 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		TransformU5BU5D_t3764228911* L_24 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		NullCheck(L_22);
		int32_t L_25 = ((int32_t)((int32_t)L_23%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))))));
		Transform_t3275118058 * L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_position_m1104419803(L_26, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))) = L_27;
		SingleU5BU5D_t577127397* L_28 = __this->get_distances_6();
		int32_t L_29 = V_1;
		float L_30 = V_0;
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_29), (float)L_30);
		float L_31 = V_0;
		Vector3_t2243707580  L_32 = V_4;
		Vector3_t2243707580  L_33 = V_5;
		Vector3_t2243707580  L_34 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		float L_35 = Vector3_get_magnitude_m860342598((&V_6), /*hidden argument*/NULL);
		V_0 = ((float)((float)L_31+(float)L_35));
	}

IL_00ca:
	{
		int32_t L_36 = V_1;
		V_1 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_37 = V_1;
		Vector3U5BU5D_t1172311765* L_38 = __this->get_points_5();
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmos()
extern "C"  void WaypointCircuit_OnDrawGizmos_m1914750460 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	{
		WaypointCircuit_DrawGizmos_m1294898092(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmosSelected()
extern "C"  void WaypointCircuit_OnDrawGizmosSelected_m367979945 (WaypointCircuit_t2375210762 * __this, const MethodInfo* method)
{
	{
		WaypointCircuit_DrawGizmos_m1294898092(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit::DrawGizmos(System.Boolean)
extern "C"  void WaypointCircuit_DrawGizmos_m1294898092 (WaypointCircuit_t2375210762 * __this, bool ___selected0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t2020392075  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	{
		WaypointList_t2985503331 * L_0 = __this->get_waypointList_2();
		NullCheck(L_0);
		L_0->set_circuit_0(__this);
		TransformU5BU5D_t3764228911* L_1 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0120;
		}
	}
	{
		TransformU5BU5D_t3764228911* L_2 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		__this->set_numPoints_4((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		WaypointCircuit_CachePositionsAndDistances_m3412157573(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t577127397* L_3 = __this->get_distances_6();
		SingleU5BU5D_t577127397* L_4 = __this->get_distances_6();
		NullCheck(L_4);
		NullCheck(L_3);
		int32_t L_5 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)1));
		float L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		WaypointCircuit_set_Length_m3662637174(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = ___selected0;
		if (!L_7)
		{
			goto IL_0055;
		}
	}
	{
		Color_t2020392075  L_8 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		goto IL_006e;
	}

IL_0055:
	{
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (1.0f), (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		G_B4_0 = L_9;
	}

IL_006e:
	{
		Gizmos_set_color_m494992840(NULL /*static, unused*/, G_B4_0, /*hidden argument*/NULL);
		TransformU5BU5D_t3764228911* L_10 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = 0;
		Transform_t3275118058 * L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		bool L_14 = __this->get_smoothRoute_3();
		if (!L_14)
		{
			goto IL_00e2;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_00be;
	}

IL_0097:
	{
		float L_15 = V_1;
		Vector3_t2243707580  L_16 = WaypointCircuit_GetRoutePosition_m1847105981(__this, ((float)((float)L_15+(float)(1.0f))), /*hidden argument*/NULL);
		V_2 = L_16;
		Vector3_t2243707580  L_17 = V_0;
		Vector3_t2243707580  L_18 = V_2;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = V_2;
		V_0 = L_19;
		float L_20 = V_1;
		float L_21 = WaypointCircuit_get_Length_m1426297875(__this, /*hidden argument*/NULL);
		float L_22 = __this->get_editorVisualisationSubsteps_7();
		V_1 = ((float)((float)L_20+(float)((float)((float)L_21/(float)L_22))));
	}

IL_00be:
	{
		float L_23 = V_1;
		float L_24 = WaypointCircuit_get_Length_m1426297875(__this, /*hidden argument*/NULL);
		if ((((float)L_23) < ((float)L_24)))
		{
			goto IL_0097;
		}
	}
	{
		Vector3_t2243707580  L_25 = V_0;
		TransformU5BU5D_t3764228911* L_26 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = 0;
		Transform_t3275118058 * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = Transform_get_position_m1104419803(L_28, /*hidden argument*/NULL);
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_25, L_29, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_00e2:
	{
		V_3 = 0;
		goto IL_0112;
	}

IL_00e9:
	{
		TransformU5BU5D_t3764228911* L_30 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		int32_t L_31 = V_3;
		TransformU5BU5D_t3764228911* L_32 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		NullCheck(L_30);
		int32_t L_33 = ((int32_t)((int32_t)((int32_t)((int32_t)L_31+(int32_t)1))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))));
		Transform_t3275118058 * L_34 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_position_m1104419803(L_34, /*hidden argument*/NULL);
		V_4 = L_35;
		Vector3_t2243707580  L_36 = V_0;
		Vector3_t2243707580  L_37 = V_4;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = V_4;
		V_0 = L_38;
		int32_t L_39 = V_3;
		V_3 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0112:
	{
		int32_t L_40 = V_3;
		TransformU5BU5D_t3764228911* L_41 = WaypointCircuit_get_Waypoints_m1083332871(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00e9;
		}
	}

IL_0120:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void RoutePoint__ctor_m3125093342 (RoutePoint_t318924311 * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___position0;
		__this->set_position_0(L_0);
		Vector3_t2243707580  L_1 = ___direction1;
		__this->set_direction_1(L_1);
		return;
	}
}
extern "C"  void RoutePoint__ctor_m3125093342_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___position0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	RoutePoint_t318924311 * _thisAdjusted = reinterpret_cast<RoutePoint_t318924311 *>(__this + 1);
	RoutePoint__ctor_m3125093342(_thisAdjusted, ___position0, ___direction1, method);
}
// System.Void UnityStandardAssets.Utility.WaypointCircuit/WaypointList::.ctor()
extern Il2CppClass* TransformU5BU5D_t3764228911_il2cpp_TypeInfo_var;
extern const uint32_t WaypointList__ctor_m2332832734_MetadataUsageId;
extern "C"  void WaypointList__ctor_m2332832734 (WaypointList_t2985503331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointList__ctor_m2332832734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_items_1(((TransformU5BU5D_t3764228911*)SZArrayNew(TransformU5BU5D_t3764228911_il2cpp_TypeInfo_var, (uint32_t)0)));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern "C"  void WaypointProgressTracker__ctor_m553453252 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	{
		__this->set_lookAheadForTargetOffset_3((5.0f));
		__this->set_lookAheadForTargetFactor_4((0.1f));
		__this->set_lookAheadForSpeedOffset_5((10.0f));
		__this->set_lookAheadForSpeedFactor_6((0.2f));
		__this->set_pointToPointThreshold_8((4.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern "C"  RoutePoint_t318924311  WaypointProgressTracker_get_targetPoint_m4155551430 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	{
		RoutePoint_t318924311  L_0 = __this->get_U3CtargetPointU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C"  void WaypointProgressTracker_set_targetPoint_m2103277523 (WaypointProgressTracker_t2206407592 * __this, RoutePoint_t318924311  ___value0, const MethodInfo* method)
{
	{
		RoutePoint_t318924311  L_0 = ___value0;
		__this->set_U3CtargetPointU3Ek__BackingField_9(L_0);
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern "C"  RoutePoint_t318924311  WaypointProgressTracker_get_speedPoint_m2157394520 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	{
		RoutePoint_t318924311  L_0 = __this->get_U3CspeedPointU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C"  void WaypointProgressTracker_set_speedPoint_m921009273 (WaypointProgressTracker_t2206407592 * __this, RoutePoint_t318924311  ___value0, const MethodInfo* method)
{
	{
		RoutePoint_t318924311  L_0 = ___value0;
		__this->set_U3CspeedPointU3Ek__BackingField_10(L_0);
		return;
	}
}
// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern "C"  RoutePoint_t318924311  WaypointProgressTracker_get_progressPoint_m3502672404 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	{
		RoutePoint_t318924311  L_0 = __this->get_U3CprogressPointU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern "C"  void WaypointProgressTracker_set_progressPoint_m4022709851 (WaypointProgressTracker_t2206407592 * __this, RoutePoint_t318924311  ___value0, const MethodInfo* method)
{
	{
		RoutePoint_t318924311  L_0 = ___value0;
		__this->set_U3CprogressPointU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral598630370;
extern const uint32_t WaypointProgressTracker_Start_m3721510712_MetadataUsageId;
extern "C"  void WaypointProgressTracker_Start_m3721510712 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointProgressTracker_Start_m3721510712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_target_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_2 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, _stringLiteral598630370, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_4, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		__this->set_target_12(L_5);
	}

IL_0031:
	{
		WaypointProgressTracker_Reset_m571259055(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern "C"  void WaypointProgressTracker_Reset_m571259055 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	{
		__this->set_progressDistance_13((0.0f));
		__this->set_progressNum_14(0);
		int32_t L_0 = __this->get_progressStyle_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3275118058 * L_1 = __this->get_target_12();
		WaypointCircuit_t2375210762 * L_2 = __this->get_circuit_2();
		NullCheck(L_2);
		TransformU5BU5D_t3764228911* L_3 = WaypointCircuit_get_Waypoints_m1083332871(L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_progressNum_14();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Transform_t3275118058 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = __this->get_target_12();
		WaypointCircuit_t2375210762 * L_9 = __this->get_circuit_2();
		NullCheck(L_9);
		TransformU5BU5D_t3764228911* L_10 = WaypointCircuit_get_Waypoints_m1083332871(L_9, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_progressNum_14();
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Transform_t3275118058 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Quaternion_t4030073918  L_14 = Transform_get_rotation_m1033555130(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_rotation_m3411284563(L_8, L_14, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t WaypointProgressTracker_Update_m1654033775_MetadataUsageId;
extern "C"  void WaypointProgressTracker_Update_m1654033775 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointProgressTracker_Update_m1654033775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RoutePoint_t318924311  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RoutePoint_t318924311  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RoutePoint_t318924311  V_4;
	memset(&V_4, 0, sizeof(V_4));
	RoutePoint_t318924311  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	RoutePoint_t318924311  V_8;
	memset(&V_8, 0, sizeof(V_8));
	RoutePoint_t318924311  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		int32_t L_0 = __this->get_progressStyle_7();
		if (L_0)
		{
			goto IL_0151;
		}
	}
	{
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0054;
		}
	}
	{
		float L_2 = __this->get_speed_16();
		Vector3_t2243707580  L_3 = __this->get_lastPosition_15();
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_2, ((float)((float)L_7/(float)L_8)), L_9, /*hidden argument*/NULL);
		__this->set_speed_16(L_10);
	}

IL_0054:
	{
		Transform_t3275118058 * L_11 = __this->get_target_12();
		WaypointCircuit_t2375210762 * L_12 = __this->get_circuit_2();
		float L_13 = __this->get_progressDistance_13();
		float L_14 = __this->get_lookAheadForTargetOffset_3();
		float L_15 = __this->get_lookAheadForTargetFactor_4();
		float L_16 = __this->get_speed_16();
		NullCheck(L_12);
		RoutePoint_t318924311  L_17 = WaypointCircuit_GetRoutePoint_m1379232192(L_12, ((float)((float)((float)((float)L_13+(float)L_14))+(float)((float)((float)L_15*(float)L_16)))), /*hidden argument*/NULL);
		V_1 = L_17;
		Vector3_t2243707580  L_18 = (&V_1)->get_position_0();
		NullCheck(L_11);
		Transform_set_position_m2469242620(L_11, L_18, /*hidden argument*/NULL);
		Transform_t3275118058 * L_19 = __this->get_target_12();
		WaypointCircuit_t2375210762 * L_20 = __this->get_circuit_2();
		float L_21 = __this->get_progressDistance_13();
		float L_22 = __this->get_lookAheadForSpeedOffset_5();
		float L_23 = __this->get_lookAheadForSpeedFactor_6();
		float L_24 = __this->get_speed_16();
		NullCheck(L_20);
		RoutePoint_t318924311  L_25 = WaypointCircuit_GetRoutePoint_m1379232192(L_20, ((float)((float)((float)((float)L_21+(float)L_22))+(float)((float)((float)L_23*(float)L_24)))), /*hidden argument*/NULL);
		V_2 = L_25;
		Vector3_t2243707580  L_26 = (&V_2)->get_direction_1();
		Quaternion_t4030073918  L_27 = Quaternion_LookRotation_m633695927(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_rotation_m3411284563(L_19, L_27, /*hidden argument*/NULL);
		WaypointCircuit_t2375210762 * L_28 = __this->get_circuit_2();
		float L_29 = __this->get_progressDistance_13();
		NullCheck(L_28);
		RoutePoint_t318924311  L_30 = WaypointCircuit_GetRoutePoint_m1379232192(L_28, L_29, /*hidden argument*/NULL);
		WaypointProgressTracker_set_progressPoint_m4022709851(__this, L_30, /*hidden argument*/NULL);
		RoutePoint_t318924311  L_31 = WaypointProgressTracker_get_progressPoint_m3502672404(__this, /*hidden argument*/NULL);
		V_4 = L_31;
		Vector3_t2243707580  L_32 = (&V_4)->get_position_0();
		Transform_t3275118058 * L_33 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		V_3 = L_35;
		Vector3_t2243707580  L_36 = V_3;
		RoutePoint_t318924311  L_37 = WaypointProgressTracker_get_progressPoint_m3502672404(__this, /*hidden argument*/NULL);
		V_5 = L_37;
		Vector3_t2243707580  L_38 = (&V_5)->get_direction_1();
		float L_39 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		if ((!(((float)L_39) < ((float)(0.0f)))))
		{
			goto IL_013b;
		}
	}
	{
		float L_40 = __this->get_progressDistance_13();
		float L_41 = Vector3_get_magnitude_m860342598((&V_3), /*hidden argument*/NULL);
		__this->set_progressDistance_13(((float)((float)L_40+(float)((float)((float)L_41*(float)(0.5f))))));
	}

IL_013b:
	{
		Transform_t3275118058 * L_42 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector3_t2243707580  L_43 = Transform_get_position_m1104419803(L_42, /*hidden argument*/NULL);
		__this->set_lastPosition_15(L_43);
		goto IL_025d;
	}

IL_0151:
	{
		Transform_t3275118058 * L_44 = __this->get_target_12();
		NullCheck(L_44);
		Vector3_t2243707580  L_45 = Transform_get_position_m1104419803(L_44, /*hidden argument*/NULL);
		Transform_t3275118058 * L_46 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t2243707580  L_47 = Transform_get_position_m1104419803(L_46, /*hidden argument*/NULL);
		Vector3_t2243707580  L_48 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_45, L_47, /*hidden argument*/NULL);
		V_6 = L_48;
		float L_49 = Vector3_get_magnitude_m860342598((&V_6), /*hidden argument*/NULL);
		float L_50 = __this->get_pointToPointThreshold_8();
		if ((!(((float)L_49) < ((float)L_50))))
		{
			goto IL_019c;
		}
	}
	{
		int32_t L_51 = __this->get_progressNum_14();
		WaypointCircuit_t2375210762 * L_52 = __this->get_circuit_2();
		NullCheck(L_52);
		TransformU5BU5D_t3764228911* L_53 = WaypointCircuit_get_Waypoints_m1083332871(L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		__this->set_progressNum_14(((int32_t)((int32_t)((int32_t)((int32_t)L_51+(int32_t)1))%(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_53)->max_length)))))));
	}

IL_019c:
	{
		Transform_t3275118058 * L_54 = __this->get_target_12();
		WaypointCircuit_t2375210762 * L_55 = __this->get_circuit_2();
		NullCheck(L_55);
		TransformU5BU5D_t3764228911* L_56 = WaypointCircuit_get_Waypoints_m1083332871(L_55, /*hidden argument*/NULL);
		int32_t L_57 = __this->get_progressNum_14();
		NullCheck(L_56);
		int32_t L_58 = L_57;
		Transform_t3275118058 * L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_59);
		Vector3_t2243707580  L_60 = Transform_get_position_m1104419803(L_59, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_position_m2469242620(L_54, L_60, /*hidden argument*/NULL);
		Transform_t3275118058 * L_61 = __this->get_target_12();
		WaypointCircuit_t2375210762 * L_62 = __this->get_circuit_2();
		NullCheck(L_62);
		TransformU5BU5D_t3764228911* L_63 = WaypointCircuit_get_Waypoints_m1083332871(L_62, /*hidden argument*/NULL);
		int32_t L_64 = __this->get_progressNum_14();
		NullCheck(L_63);
		int32_t L_65 = L_64;
		Transform_t3275118058 * L_66 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck(L_66);
		Quaternion_t4030073918  L_67 = Transform_get_rotation_m1033555130(L_66, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_rotation_m3411284563(L_61, L_67, /*hidden argument*/NULL);
		WaypointCircuit_t2375210762 * L_68 = __this->get_circuit_2();
		float L_69 = __this->get_progressDistance_13();
		NullCheck(L_68);
		RoutePoint_t318924311  L_70 = WaypointCircuit_GetRoutePoint_m1379232192(L_68, L_69, /*hidden argument*/NULL);
		WaypointProgressTracker_set_progressPoint_m4022709851(__this, L_70, /*hidden argument*/NULL);
		RoutePoint_t318924311  L_71 = WaypointProgressTracker_get_progressPoint_m3502672404(__this, /*hidden argument*/NULL);
		V_8 = L_71;
		Vector3_t2243707580  L_72 = (&V_8)->get_position_0();
		Transform_t3275118058 * L_73 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_73);
		Vector3_t2243707580  L_74 = Transform_get_position_m1104419803(L_73, /*hidden argument*/NULL);
		Vector3_t2243707580  L_75 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_72, L_74, /*hidden argument*/NULL);
		V_7 = L_75;
		Vector3_t2243707580  L_76 = V_7;
		RoutePoint_t318924311  L_77 = WaypointProgressTracker_get_progressPoint_m3502672404(__this, /*hidden argument*/NULL);
		V_9 = L_77;
		Vector3_t2243707580  L_78 = (&V_9)->get_direction_1();
		float L_79 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_76, L_78, /*hidden argument*/NULL);
		if ((!(((float)L_79) < ((float)(0.0f)))))
		{
			goto IL_024c;
		}
	}
	{
		float L_80 = __this->get_progressDistance_13();
		float L_81 = Vector3_get_magnitude_m860342598((&V_7), /*hidden argument*/NULL);
		__this->set_progressDistance_13(((float)((float)L_80+(float)L_81)));
	}

IL_024c:
	{
		Transform_t3275118058 * L_82 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t2243707580  L_83 = Transform_get_position_m1104419803(L_82, /*hidden argument*/NULL);
		__this->set_lastPosition_15(L_83);
	}

IL_025d:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern "C"  void WaypointProgressTracker_OnDrawGizmos_m1674029918 (WaypointProgressTracker_t2206407592 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_007f;
		}
	}
	{
		Color_t2020392075  L_1 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = __this->get_target_12();
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		WaypointCircuit_t2375210762 * L_6 = __this->get_circuit_2();
		float L_7 = __this->get_progressDistance_13();
		NullCheck(L_6);
		Vector3_t2243707580  L_8 = WaypointCircuit_GetRoutePosition_m1847105981(L_6, L_7, /*hidden argument*/NULL);
		Gizmos_DrawWireSphere_m3395547818(NULL /*static, unused*/, L_8, (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_9 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = __this->get_target_12();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = __this->get_target_12();
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = __this->get_target_12();
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_forward_m1833488937(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_11, L_16, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void VideoControlsManager::.ctor()
extern "C"  void VideoControlsManager__ctor_m3766416489 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoControlsManager::set_Player(GvrVideoPlayerTexture)
extern "C"  void VideoControlsManager_set_Player_m3494613477 (VideoControlsManager_t3010523296 * __this, GvrVideoPlayerTexture_t673526704 * ___value0, const MethodInfo* method)
{
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = ___value0;
		__this->set_U3CPlayerU3Ek__BackingField_12(L_0);
		return;
	}
}
// GvrVideoPlayerTexture VideoControlsManager::get_Player()
extern "C"  GvrVideoPlayerTexture_t673526704 * VideoControlsManager_get_Player_m44360062 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = __this->get_U3CPlayerU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void VideoControlsManager::Awake()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisText_t356221433_m324169492_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRawImage_t2749640213_m652723431_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisSlider_t297367283_m1167155165_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisImage_t2042527209_m1009123198_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRectTransform_t3349966182_m1564438138_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3632474640;
extern Il2CppCodeGenString* _stringLiteral3378564200;
extern Il2CppCodeGenString* _stringLiteral3300430491;
extern Il2CppCodeGenString* _stringLiteral3808982001;
extern Il2CppCodeGenString* _stringLiteral3722207907;
extern Il2CppCodeGenString* _stringLiteral1667191731;
extern Il2CppCodeGenString* _stringLiteral3205191984;
extern Il2CppCodeGenString* _stringLiteral787298419;
extern Il2CppCodeGenString* _stringLiteral403189246;
extern const uint32_t VideoControlsManager_Awake_m1878278940_MetadataUsageId;
extern "C"  void VideoControlsManager_Awake_m1878278940 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_Awake_m1878278940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	TextU5BU5D_t4216439300* V_1 = NULL;
	int32_t V_2 = 0;
	RawImage_t2749640213 * V_3 = NULL;
	RawImageU5BU5D_t3856578040* V_4 = NULL;
	int32_t V_5 = 0;
	Slider_t297367283 * V_6 = NULL;
	SliderU5BU5D_t1144817634* V_7 = NULL;
	int32_t V_8 = 0;
	Image_t2042527209 * V_9 = NULL;
	ImageU5BU5D_t590162004* V_10 = NULL;
	int32_t V_11 = 0;
	RectTransform_t3349966182 * V_12 = NULL;
	RectTransformU5BU5D_t3948421699* V_13 = NULL;
	int32_t V_14 = 0;
	{
		TextU5BU5D_t4216439300* L_0 = Component_GetComponentsInChildren_TisText_t356221433_m324169492(__this, /*hidden argument*/Component_GetComponentsInChildren_TisText_t356221433_m324169492_MethodInfo_var);
		V_1 = L_0;
		V_2 = 0;
		goto IL_005d;
	}

IL_000e:
	{
		TextU5BU5D_t4216439300* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Text_t356221433 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		Text_t356221433 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m2079638459(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, _stringLiteral3632474640, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0038;
		}
	}
	{
		Text_t356221433 * L_9 = V_0;
		__this->set_videoPosition_10(L_9);
		goto IL_0059;
	}

IL_0038:
	{
		Text_t356221433 * L_10 = V_0;
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral3378564200, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0059;
		}
	}
	{
		Text_t356221433 * L_14 = V_0;
		__this->set_videoDuration_11(L_14);
	}

IL_0059:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_16 = V_2;
		TextU5BU5D_t4216439300* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		RawImageU5BU5D_t3856578040* L_18 = Component_GetComponentsInChildren_TisRawImage_t2749640213_m652723431(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRawImage_t2749640213_m652723431_MethodInfo_var);
		V_4 = L_18;
		V_5 = 0;
		goto IL_00d4;
	}

IL_0077:
	{
		RawImageU5BU5D_t3856578040* L_19 = V_4;
		int32_t L_20 = V_5;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		RawImage_t2749640213 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_3 = L_22;
		RawImage_t2749640213 * L_23 = V_3;
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		String_t* L_25 = Object_get_name_m2079638459(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_25, _stringLiteral3300430491, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00a8;
		}
	}
	{
		RawImage_t2749640213 * L_27 = V_3;
		NullCheck(L_27);
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(L_27, /*hidden argument*/NULL);
		__this->set_playSprite_3(L_28);
		goto IL_00ce;
	}

IL_00a8:
	{
		RawImage_t2749640213 * L_29 = V_3;
		NullCheck(L_29);
		GameObject_t1756533147 * L_30 = Component_get_gameObject_m3105766835(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = Object_get_name_m2079638459(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_31, _stringLiteral3808982001, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00ce;
		}
	}
	{
		RawImage_t2749640213 * L_33 = V_3;
		NullCheck(L_33);
		GameObject_t1756533147 * L_34 = Component_get_gameObject_m3105766835(L_33, /*hidden argument*/NULL);
		__this->set_pauseSprite_2(L_34);
	}

IL_00ce:
	{
		int32_t L_35 = V_5;
		V_5 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00d4:
	{
		int32_t L_36 = V_5;
		RawImageU5BU5D_t3856578040* L_37 = V_4;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		SliderU5BU5D_t1144817634* L_38 = Component_GetComponentsInChildren_TisSlider_t297367283_m1167155165(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisSlider_t297367283_m1167155165_MethodInfo_var);
		V_7 = L_38;
		V_8 = 0;
		goto IL_01bd;
	}

IL_00f0:
	{
		SliderU5BU5D_t1144817634* L_39 = V_7;
		int32_t L_40 = V_8;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		Slider_t297367283 * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_6 = L_42;
		Slider_t297367283 * L_43 = V_6;
		NullCheck(L_43);
		GameObject_t1756533147 * L_44 = Component_get_gameObject_m3105766835(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		String_t* L_45 = Object_get_name_m2079638459(L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_45, _stringLiteral3722207907, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0194;
		}
	}
	{
		Slider_t297367283 * L_47 = V_6;
		__this->set_videoScrubber_4(L_47);
		Slider_t297367283 * L_48 = __this->get_videoScrubber_4();
		NullCheck(L_48);
		Slider_set_maxValue_m2951480075(L_48, (100.0f), /*hidden argument*/NULL);
		Slider_t297367283 * L_49 = __this->get_videoScrubber_4();
		NullCheck(L_49);
		Slider_set_minValue_m1484509981(L_49, (0.0f), /*hidden argument*/NULL);
		Slider_t297367283 * L_50 = __this->get_videoScrubber_4();
		NullCheck(L_50);
		ImageU5BU5D_t590162004* L_51 = Component_GetComponentsInChildren_TisImage_t2042527209_m1009123198(L_50, /*hidden argument*/Component_GetComponentsInChildren_TisImage_t2042527209_m1009123198_MethodInfo_var);
		V_10 = L_51;
		V_11 = 0;
		goto IL_0184;
	}

IL_014f:
	{
		ImageU5BU5D_t590162004* L_52 = V_10;
		int32_t L_53 = V_11;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		Image_t2042527209 * L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		V_9 = L_55;
		Image_t2042527209 * L_56 = V_9;
		NullCheck(L_56);
		GameObject_t1756533147 * L_57 = Component_get_gameObject_m3105766835(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_58 = Object_get_name_m2079638459(L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_59 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_58, _stringLiteral1667191731, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_017e;
		}
	}
	{
		Image_t2042527209 * L_60 = V_9;
		NullCheck(L_60);
		GameObject_t1756533147 * L_61 = Component_get_gameObject_m3105766835(L_60, /*hidden argument*/NULL);
		__this->set_bufferedBackground_8(L_61);
	}

IL_017e:
	{
		int32_t L_62 = V_11;
		V_11 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_0184:
	{
		int32_t L_63 = V_11;
		ImageU5BU5D_t590162004* L_64 = V_10;
		NullCheck(L_64);
		if ((((int32_t)L_63) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_64)->max_length)))))))
		{
			goto IL_014f;
		}
	}
	{
		goto IL_01b7;
	}

IL_0194:
	{
		Slider_t297367283 * L_65 = V_6;
		NullCheck(L_65);
		GameObject_t1756533147 * L_66 = Component_get_gameObject_m3105766835(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		String_t* L_67 = Object_get_name_m2079638459(L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_68 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_67, _stringLiteral3205191984, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01b7;
		}
	}
	{
		Slider_t297367283 * L_69 = V_6;
		__this->set_volumeSlider_5(L_69);
	}

IL_01b7:
	{
		int32_t L_70 = V_8;
		V_8 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01bd:
	{
		int32_t L_71 = V_8;
		SliderU5BU5D_t1144817634* L_72 = V_7;
		NullCheck(L_72);
		if ((((int32_t)L_71) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_72)->max_length)))))))
		{
			goto IL_00f0;
		}
	}
	{
		RectTransformU5BU5D_t3948421699* L_73 = Component_GetComponentsInChildren_TisRectTransform_t3349966182_m1564438138(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRectTransform_t3349966182_m1564438138_MethodInfo_var);
		V_13 = L_73;
		V_14 = 0;
		goto IL_023b;
	}

IL_01d9:
	{
		RectTransformU5BU5D_t3948421699* L_74 = V_13;
		int32_t L_75 = V_14;
		NullCheck(L_74);
		int32_t L_76 = L_75;
		RectTransform_t3349966182 * L_77 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76));
		V_12 = L_77;
		RectTransform_t3349966182 * L_78 = V_12;
		NullCheck(L_78);
		GameObject_t1756533147 * L_79 = Component_get_gameObject_m3105766835(L_78, /*hidden argument*/NULL);
		NullCheck(L_79);
		String_t* L_80 = Object_get_name_m2079638459(L_79, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_81 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_80, _stringLiteral787298419, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_020d;
		}
	}
	{
		RectTransform_t3349966182 * L_82 = V_12;
		NullCheck(L_82);
		GameObject_t1756533147 * L_83 = Component_get_gameObject_m3105766835(L_82, /*hidden argument*/NULL);
		__this->set_volumeWidget_6(L_83);
		goto IL_0235;
	}

IL_020d:
	{
		RectTransform_t3349966182 * L_84 = V_12;
		NullCheck(L_84);
		GameObject_t1756533147 * L_85 = Component_get_gameObject_m3105766835(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		String_t* L_86 = Object_get_name_m2079638459(L_85, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_87 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_86, _stringLiteral403189246, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0235;
		}
	}
	{
		RectTransform_t3349966182 * L_88 = V_12;
		NullCheck(L_88);
		GameObject_t1756533147 * L_89 = Component_get_gameObject_m3105766835(L_88, /*hidden argument*/NULL);
		__this->set_settingsPanel_7(L_89);
	}

IL_0235:
	{
		int32_t L_90 = V_14;
		V_14 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_023b:
	{
		int32_t L_91 = V_14;
		RectTransformU5BU5D_t3948421699* L_92 = V_13;
		NullCheck(L_92);
		if ((((int32_t)L_91) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_92)->max_length)))))))
		{
			goto IL_01d9;
		}
	}
	{
		return;
	}
}
// System.Void VideoControlsManager::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisScrubberEvents_t2429506345_m429218110_MethodInfo_var;
extern const uint32_t VideoControlsManager_Start_m4093526529_MetadataUsageId;
extern "C"  void VideoControlsManager_Start_m4093526529 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_Start_m4093526529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ScrubberEvents_t2429506345 * V_0 = NULL;
	ScrubberEventsU5BU5D_t3322287636* V_1 = NULL;
	int32_t V_2 = 0;
	{
		ScrubberEventsU5BU5D_t3322287636* L_0 = Component_GetComponentsInChildren_TisScrubberEvents_t2429506345_m429218110(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisScrubberEvents_t2429506345_m429218110_MethodInfo_var);
		V_1 = L_0;
		V_2 = 0;
		goto IL_001e;
	}

IL_000f:
	{
		ScrubberEventsU5BU5D_t3322287636* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		ScrubberEvents_t2429506345 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		ScrubberEvents_t2429506345 * L_5 = V_0;
		NullCheck(L_5);
		ScrubberEvents_set_ControlManager_m4017509979(L_5, __this, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_7 = V_2;
		ScrubberEventsU5BU5D_t3322287636* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_000f;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_9 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0044;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_11 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		GvrVideoPlayerTexture_Init_m842832607(L_11, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void VideoControlsManager::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t VideoControlsManager_Update_m3205036906_MetadataUsageId;
extern "C"  void VideoControlsManager_Update_m3205036906 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_Update_m3205036906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GvrVideoPlayerTexture_get_VideoReady_m1713295662(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_2 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = GvrVideoPlayerTexture_get_IsPaused_m36619442(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}

IL_0020:
	{
		GameObject_t1756533147 * L_4 = __this->get_pauseSprite_2();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_playSprite_3();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		goto IL_0075;
	}

IL_003d:
	{
		GvrVideoPlayerTexture_t673526704 * L_6 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = GvrVideoPlayerTexture_get_VideoReady_m1713295662(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_8 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = GvrVideoPlayerTexture_get_IsPaused_m36619442(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0075;
		}
	}
	{
		GameObject_t1756533147 * L_10 = __this->get_pauseSprite_2();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_playSprite_3();
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)0, /*hidden argument*/NULL);
	}

IL_0075:
	{
		GvrVideoPlayerTexture_t673526704 * L_12 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = GvrVideoPlayerTexture_get_VideoReady_m1713295662(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_01e6;
		}
	}
	{
		Vector3_t2243707580  L_14 = __this->get_basePosition_9();
		Vector3_t2243707580  L_15 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_16 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b0;
		}
	}
	{
		Slider_t297367283 * L_17 = __this->get_videoScrubber_4();
		NullCheck(L_17);
		RectTransform_t3349966182 * L_18 = Slider_get_handleRect_m2416838927(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_localPosition_m2533925116(L_18, /*hidden argument*/NULL);
		__this->set_basePosition_9(L_19);
	}

IL_00b0:
	{
		Slider_t297367283 * L_20 = __this->get_videoScrubber_4();
		GvrVideoPlayerTexture_t673526704 * L_21 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		int64_t L_22 = GvrVideoPlayerTexture_get_VideoDuration_m1238201236(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Slider_set_maxValue_m2951480075(L_20, (((float)((float)L_22))), /*hidden argument*/NULL);
		Slider_t297367283 * L_23 = __this->get_videoScrubber_4();
		GvrVideoPlayerTexture_t673526704 * L_24 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		int64_t L_25 = GvrVideoPlayerTexture_get_CurrentPosition_m3562589837(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_23, (((float)((float)L_25))));
		GvrVideoPlayerTexture_t673526704 * L_26 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = GvrVideoPlayerTexture_get_BufferedPercentage_m1333771443(L_26, /*hidden argument*/NULL);
		V_0 = ((float)((float)(((float)((float)L_27)))/(float)(100.0f)));
		float L_28 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_29 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_28, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_29;
		GameObject_t1756533147 * L_30 = __this->get_bufferedBackground_8();
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		float L_32 = V_1;
		Vector3_t2243707580  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m2638739322(&L_33, L_32, (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localScale_m2325460848(L_31, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = __this->get_bufferedBackground_8();
		NullCheck(L_34);
		Transform_t3275118058 * L_35 = GameObject_get_transform_m909382139(L_34, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_36 = __this->get_address_of_basePosition_9();
		float L_37 = L_36->get_x_1();
		Vector3_t2243707580 * L_38 = __this->get_address_of_basePosition_9();
		float L_39 = L_38->get_x_1();
		float L_40 = V_1;
		Vector3_t2243707580  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector3__ctor_m2638739322(&L_41, ((float)((float)L_37-(float)((float)((float)L_39*(float)L_40)))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_set_localPosition_m1026930133(L_35, L_41, /*hidden argument*/NULL);
		Text_t356221433 * L_42 = __this->get_videoPosition_10();
		GvrVideoPlayerTexture_t673526704 * L_43 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		int64_t L_44 = GvrVideoPlayerTexture_get_CurrentPosition_m3562589837(L_43, /*hidden argument*/NULL);
		String_t* L_45 = VideoControlsManager_FormatTime_m1938589656(__this, L_44, /*hidden argument*/NULL);
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_42, L_45);
		Text_t356221433 * L_46 = __this->get_videoDuration_11();
		GvrVideoPlayerTexture_t673526704 * L_47 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		int64_t L_48 = GvrVideoPlayerTexture_get_VideoDuration_m1238201236(L_47, /*hidden argument*/NULL);
		String_t* L_49 = VideoControlsManager_FormatTime_m1938589656(__this, L_48, /*hidden argument*/NULL);
		NullCheck(L_46);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_46, L_49);
		Slider_t297367283 * L_50 = __this->get_volumeSlider_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_51 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_50, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01e1;
		}
	}
	{
		Slider_t297367283 * L_52 = __this->get_volumeSlider_5();
		NullCheck(L_52);
		Slider_set_minValue_m1484509981(L_52, (0.0f), /*hidden argument*/NULL);
		Slider_t297367283 * L_53 = __this->get_volumeSlider_5();
		GvrVideoPlayerTexture_t673526704 * L_54 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		int32_t L_55 = GvrVideoPlayerTexture_get_MaxVolume_m2331722398(L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		Slider_set_maxValue_m2951480075(L_53, (((float)((float)L_55))), /*hidden argument*/NULL);
		Slider_t297367283 * L_56 = __this->get_volumeSlider_5();
		GvrVideoPlayerTexture_t673526704 * L_57 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		int32_t L_58 = GvrVideoPlayerTexture_get_CurrentVolume_m2628199275(L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_56, (((float)((float)L_58))));
	}

IL_01e1:
	{
		goto IL_01f6;
	}

IL_01e6:
	{
		Slider_t297367283 * L_59 = __this->get_videoScrubber_4();
		NullCheck(L_59);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_59, (0.0f));
	}

IL_01f6:
	{
		return;
	}
}
// System.Void VideoControlsManager::OnVolumeUp()
extern "C"  void VideoControlsManager_OnVolumeUp_m2824371833 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = GvrVideoPlayerTexture_get_CurrentVolume_m2628199275(L_0, /*hidden argument*/NULL);
		GvrVideoPlayerTexture_t673526704 * L_2 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = GvrVideoPlayerTexture_get_MaxVolume_m2331722398(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_4 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		GvrVideoPlayerTexture_t673526704 * L_5 = L_4;
		NullCheck(L_5);
		int32_t L_6 = GvrVideoPlayerTexture_get_CurrentVolume_m2628199275(L_5, /*hidden argument*/NULL);
		NullCheck(L_5);
		GvrVideoPlayerTexture_set_CurrentVolume_m2576283326(L_5, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VideoControlsManager::OnVolumeDown()
extern "C"  void VideoControlsManager_OnVolumeDown_m1222797460 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = GvrVideoPlayerTexture_get_CurrentVolume_m2628199275(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_2 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		GvrVideoPlayerTexture_t673526704 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = GvrVideoPlayerTexture_get_CurrentVolume_m2628199275(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		GvrVideoPlayerTexture_set_CurrentVolume_m2576283326(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VideoControlsManager::OnToggleVolume()
extern "C"  void VideoControlsManager_OnToggleVolume_m1563209028 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	GameObject_t1756533147 * G_B2_0 = NULL;
	GameObject_t1756533147 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	GameObject_t1756533147 * G_B3_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_volumeWidget_6();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m313590879(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		GameObject_t1756533147 * L_2 = __this->get_volumeWidget_6();
		bool L_3 = V_0;
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_settingsPanel_7();
		GameObject_t1756533147 * L_5 = __this->get_settingsPanel_7();
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeSelf_m313590879(L_5, /*hidden argument*/NULL);
		G_B1_0 = L_4;
		if (!L_6)
		{
			G_B2_0 = L_4;
			goto IL_0037;
		}
	}
	{
		bool L_7 = V_0;
		G_B3_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0038;
	}

IL_0037:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0038:
	{
		NullCheck(G_B3_1);
		GameObject_SetActive_m2887581199(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoControlsManager::OnToggleSettings()
extern "C"  void VideoControlsManager_OnToggleSettings_m4156716355 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	GameObject_t1756533147 * G_B2_0 = NULL;
	GameObject_t1756533147 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	GameObject_t1756533147 * G_B3_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_settingsPanel_7();
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m313590879(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		GameObject_t1756533147 * L_2 = __this->get_settingsPanel_7();
		bool L_3 = V_0;
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_volumeWidget_6();
		GameObject_t1756533147 * L_5 = __this->get_volumeWidget_6();
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeSelf_m313590879(L_5, /*hidden argument*/NULL);
		G_B1_0 = L_4;
		if (!L_6)
		{
			G_B2_0 = L_4;
			goto IL_0037;
		}
	}
	{
		bool L_7 = V_0;
		G_B3_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0038;
	}

IL_0037:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0038:
	{
		NullCheck(G_B3_1);
		GameObject_SetActive_m2887581199(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoControlsManager::OnPlayPause()
extern "C"  void VideoControlsManager_OnPlayPause_m3031693768 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GvrVideoPlayerTexture_get_IsPaused_m36619442(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_3 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GvrVideoPlayerTexture_Play_m4164025381(L_3, /*hidden argument*/NULL);
		goto IL_002f;
	}

IL_0023:
	{
		GvrVideoPlayerTexture_t673526704 * L_4 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GvrVideoPlayerTexture_Pause_m933228683(L_4, /*hidden argument*/NULL);
	}

IL_002f:
	{
		GameObject_t1756533147 * L_5 = __this->get_pauseSprite_2();
		bool L_6 = V_0;
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_playSprite_3();
		bool L_8 = V_0;
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		VideoControlsManager_CloseSubPanels_m1135546338(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoControlsManager::OnVolumePositionChanged(System.Single)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1805523948;
extern const uint32_t VideoControlsManager_OnVolumePositionChanged_m173042244_MetadataUsageId;
extern "C"  void VideoControlsManager_OnVolumePositionChanged_m173042244 (VideoControlsManager_t3010523296 * __this, float ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_OnVolumePositionChanged_m173042244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GvrVideoPlayerTexture_t673526704 * L_0 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GvrVideoPlayerTexture_get_VideoReady_m1713295662(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		float L_2 = ___val0;
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1805523948, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		GvrVideoPlayerTexture_t673526704 * L_6 = VideoControlsManager_get_Player_m44360062(__this, /*hidden argument*/NULL);
		float L_7 = ___val0;
		NullCheck(L_6);
		GvrVideoPlayerTexture_set_CurrentVolume_m2576283326(L_6, (((int32_t)((int32_t)L_7))), /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void VideoControlsManager::CloseSubPanels()
extern "C"  void VideoControlsManager_CloseSubPanels_m1135546338 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_volumeWidget_6();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_settingsPanel_7();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoControlsManager::Fade(System.Boolean)
extern "C"  void VideoControlsManager_Fade_m242086732 (VideoControlsManager_t3010523296 * __this, bool ___show0, const MethodInfo* method)
{
	{
		bool L_0 = ___show0;
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = VideoControlsManager_DoAppear_m4271315463(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_0018:
	{
		Il2CppObject * L_2 = VideoControlsManager_DoFade_m4086002144(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator VideoControlsManager::DoAppear()
extern Il2CppClass* U3CDoAppearU3Ec__Iterator0_t2331568912_il2cpp_TypeInfo_var;
extern const uint32_t VideoControlsManager_DoAppear_m4271315463_MetadataUsageId;
extern "C"  Il2CppObject * VideoControlsManager_DoAppear_m4271315463 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_DoAppear_m4271315463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDoAppearU3Ec__Iterator0_t2331568912 * V_0 = NULL;
	{
		U3CDoAppearU3Ec__Iterator0_t2331568912 * L_0 = (U3CDoAppearU3Ec__Iterator0_t2331568912 *)il2cpp_codegen_object_new(U3CDoAppearU3Ec__Iterator0_t2331568912_il2cpp_TypeInfo_var);
		U3CDoAppearU3Ec__Iterator0__ctor_m2527038213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoAppearU3Ec__Iterator0_t2331568912 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CDoAppearU3Ec__Iterator0_t2331568912 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator VideoControlsManager::DoFade()
extern Il2CppClass* U3CDoFadeU3Ec__Iterator1_t4193299950_il2cpp_TypeInfo_var;
extern const uint32_t VideoControlsManager_DoFade_m4086002144_MetadataUsageId;
extern "C"  Il2CppObject * VideoControlsManager_DoFade_m4086002144 (VideoControlsManager_t3010523296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_DoFade_m4086002144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDoFadeU3Ec__Iterator1_t4193299950 * V_0 = NULL;
	{
		U3CDoFadeU3Ec__Iterator1_t4193299950 * L_0 = (U3CDoFadeU3Ec__Iterator1_t4193299950 *)il2cpp_codegen_object_new(U3CDoFadeU3Ec__Iterator1_t4193299950_il2cpp_TypeInfo_var);
		U3CDoFadeU3Ec__Iterator1__ctor_m20024509(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoFadeU3Ec__Iterator1_t4193299950 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CDoFadeU3Ec__Iterator1_t4193299950 * L_2 = V_0;
		return L_2;
	}
}
// System.String VideoControlsManager::FormatTime(System.Int64)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3993410431;
extern Il2CppCodeGenString* _stringLiteral2610385663;
extern const uint32_t VideoControlsManager_FormatTime_m1938589656_MetadataUsageId;
extern "C"  String_t* VideoControlsManager_FormatTime_m1938589656 (VideoControlsManager_t3010523296 * __this, int64_t ___ms0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoControlsManager_FormatTime_m1938589656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int64_t L_0 = ___ms0;
		V_0 = (((int32_t)((int32_t)((int64_t)((int64_t)L_0/(int64_t)(((int64_t)((int64_t)((int32_t)1000)))))))));
		int32_t L_1 = V_0;
		V_1 = ((int32_t)((int32_t)L_1/(int32_t)((int32_t)60)));
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2%(int32_t)((int32_t)60)));
		int32_t L_3 = V_1;
		V_2 = ((int32_t)((int32_t)L_3/(int32_t)((int32_t)60)));
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4%(int32_t)((int32_t)60)));
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_6 = V_2;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = V_1;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		int32_t L_12 = V_0;
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral3993410431, L_8, L_11, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0042:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
		int32_t L_19 = V_0;
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2610385663, L_18, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void VideoControlsManager/<DoAppear>c__Iterator0::.ctor()
extern "C"  void U3CDoAppearU3Ec__Iterator0__ctor_m2527038213 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VideoControlsManager/<DoAppear>c__Iterator0::MoveNext()
extern const MethodInfo* Component_GetComponent_TisCanvasGroup_t3296560743_m846564447_MethodInfo_var;
extern const uint32_t U3CDoAppearU3Ec__Iterator0_MoveNext_m3014764719_MetadataUsageId;
extern "C"  bool U3CDoAppearU3Ec__Iterator0_MoveNext_m3014764719 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoAppearU3Ec__Iterator0_MoveNext_m3014764719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_00a1;
	}

IL_0021:
	{
		VideoControlsManager_t3010523296 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		CanvasGroup_t3296560743 * L_3 = Component_GetComponent_TisCanvasGroup_t3296560743_m846564447(L_2, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t3296560743_m846564447_MethodInfo_var);
		__this->set_U3CcgU3E__0_0(L_3);
		goto IL_006f;
	}

IL_0037:
	{
		CanvasGroup_t3296560743 * L_4 = __this->get_U3CcgU3E__0_0();
		CanvasGroup_t3296560743 * L_5 = L_4;
		NullCheck(L_5);
		float L_6 = CanvasGroup_get_alpha_m1304564441(L_5, /*hidden argument*/NULL);
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		CanvasGroup_set_alpha_m3292984624(L_5, ((float)((float)L_6+(float)((float)((float)L_7*(float)(2.0f))))), /*hidden argument*/NULL);
		__this->set_U24current_2(NULL);
		bool L_8 = __this->get_U24disposing_3();
		if (L_8)
		{
			goto IL_006a;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_006a:
	{
		goto IL_00a3;
	}

IL_006f:
	{
		CanvasGroup_t3296560743 * L_9 = __this->get_U3CcgU3E__0_0();
		NullCheck(L_9);
		float L_10 = CanvasGroup_get_alpha_m1304564441(L_9, /*hidden argument*/NULL);
		if ((((double)(((double)((double)L_10)))) < ((double)(1.0))))
		{
			goto IL_0037;
		}
	}
	{
		CanvasGroup_t3296560743 * L_11 = __this->get_U3CcgU3E__0_0();
		NullCheck(L_11);
		CanvasGroup_set_interactable_m2046144056(L_11, (bool)1, /*hidden argument*/NULL);
		goto IL_00a1;
	}
	// Dead block : IL_009a: ldarg.0

IL_00a1:
	{
		return (bool)0;
	}

IL_00a3:
	{
		return (bool)1;
	}
}
// System.Object VideoControlsManager/<DoAppear>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAppearU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m93984395 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object VideoControlsManager/<DoAppear>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAppearU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1489616819 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void VideoControlsManager/<DoAppear>c__Iterator0::Dispose()
extern "C"  void U3CDoAppearU3Ec__Iterator0_Dispose_m1523471434 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void VideoControlsManager/<DoAppear>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoAppearU3Ec__Iterator0_Reset_m1434524216_MetadataUsageId;
extern "C"  void U3CDoAppearU3Ec__Iterator0_Reset_m1434524216 (U3CDoAppearU3Ec__Iterator0_t2331568912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoAppearU3Ec__Iterator0_Reset_m1434524216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VideoControlsManager/<DoFade>c__Iterator1::.ctor()
extern "C"  void U3CDoFadeU3Ec__Iterator1__ctor_m20024509 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VideoControlsManager/<DoFade>c__Iterator1::MoveNext()
extern const MethodInfo* Component_GetComponent_TisCanvasGroup_t3296560743_m846564447_MethodInfo_var;
extern const uint32_t U3CDoFadeU3Ec__Iterator1_MoveNext_m2806712515_MetadataUsageId;
extern "C"  bool U3CDoFadeU3Ec__Iterator1_MoveNext_m2806712515 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoFadeU3Ec__Iterator1_MoveNext_m2806712515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0069;
		}
	}
	{
		goto IL_00a1;
	}

IL_0021:
	{
		VideoControlsManager_t3010523296 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		CanvasGroup_t3296560743 * L_3 = Component_GetComponent_TisCanvasGroup_t3296560743_m846564447(L_2, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t3296560743_m846564447_MethodInfo_var);
		__this->set_U3CcgU3E__0_0(L_3);
		goto IL_0069;
	}

IL_0037:
	{
		CanvasGroup_t3296560743 * L_4 = __this->get_U3CcgU3E__0_0();
		CanvasGroup_t3296560743 * L_5 = L_4;
		NullCheck(L_5);
		float L_6 = CanvasGroup_get_alpha_m1304564441(L_5, /*hidden argument*/NULL);
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		CanvasGroup_set_alpha_m3292984624(L_5, ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		__this->set_U24current_2(NULL);
		bool L_8 = __this->get_U24disposing_3();
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0064:
	{
		goto IL_00a3;
	}

IL_0069:
	{
		CanvasGroup_t3296560743 * L_9 = __this->get_U3CcgU3E__0_0();
		NullCheck(L_9);
		float L_10 = CanvasGroup_get_alpha_m1304564441(L_9, /*hidden argument*/NULL);
		if ((((float)L_10) > ((float)(0.0f))))
		{
			goto IL_0037;
		}
	}
	{
		CanvasGroup_t3296560743 * L_11 = __this->get_U3CcgU3E__0_0();
		NullCheck(L_11);
		CanvasGroup_set_interactable_m2046144056(L_11, (bool)0, /*hidden argument*/NULL);
		VideoControlsManager_t3010523296 * L_12 = __this->get_U24this_1();
		NullCheck(L_12);
		VideoControlsManager_CloseSubPanels_m1135546338(L_12, /*hidden argument*/NULL);
		goto IL_00a1;
	}
	// Dead block : IL_009a: ldarg.0

IL_00a1:
	{
		return (bool)0;
	}

IL_00a3:
	{
		return (bool)1;
	}
}
// System.Object VideoControlsManager/<DoFade>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoFadeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2384802911 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object VideoControlsManager/<DoFade>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoFadeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m592968455 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void VideoControlsManager/<DoFade>c__Iterator1::Dispose()
extern "C"  void U3CDoFadeU3Ec__Iterator1_Dispose_m2743829128 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void VideoControlsManager/<DoFade>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoFadeU3Ec__Iterator1_Reset_m3352664634_MetadataUsageId;
extern "C"  void U3CDoFadeU3Ec__Iterator1_Reset_m3352664634 (U3CDoFadeU3Ec__Iterator1_t4193299950 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoFadeU3Ec__Iterator1_Reset_m3352664634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VideoPlayerReference::.ctor()
extern "C"  void VideoPlayerReference__ctor_m943420494 (VideoPlayerReference_t1150574547 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VideoPlayerReference::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisVideoControlsManager_t3010523296_m453748968_MethodInfo_var;
extern const uint32_t VideoPlayerReference_Awake_m2826416547_MetadataUsageId;
extern "C"  void VideoPlayerReference_Awake_m2826416547 (VideoPlayerReference_t1150574547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoPlayerReference_Awake_m2826416547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VideoControlsManager_t3010523296 * L_0 = Component_GetComponentInChildren_TisVideoControlsManager_t3010523296_m453748968(__this, (bool)1, /*hidden argument*/Component_GetComponentInChildren_TisVideoControlsManager_t3010523296_m453748968_MethodInfo_var);
		GvrVideoPlayerTexture_t673526704 * L_1 = __this->get_player_2();
		NullCheck(L_0);
		VideoControlsManager_set_Player_m3494613477(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
