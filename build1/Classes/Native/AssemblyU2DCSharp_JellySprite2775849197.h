﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t578636151;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t851691520;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Collections.Generic.List`1<JellySprite/ReferencePoint>
struct List_1_t3979789937;
// System.Single[0...,0...]
struct SingleU5BU2CU5D_t577127398;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// JellySprite/ReferencePoint
struct ReferencePoint_t315701509;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_JellySprite_PhysicsStyle136357596.h"
#include "AssemblyU2DCSharp_JellySprite_MassStyle479920749.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation993299413.h"
#include "UnityEngine_UnityEngine_RigidbodyInterpolation2D1367049267.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode841589752.h"
#include "UnityEngine_UnityEngine_CollisionDetectionMode2D3545846550.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellySprite
struct  JellySprite_t2775849197  : public MonoBehaviour_t1158329972
{
public:
	// JellySprite/PhysicsStyle JellySprite::m_Style
	int32_t ___m_Style_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> JellySprite::m_FreeModeBodyPositions
	List_1_t1612828712 * ___m_FreeModeBodyPositions_3;
	// System.Collections.Generic.List`1<System.Single> JellySprite::m_FreeModeBodyRadii
	List_1_t1445631064 * ___m_FreeModeBodyRadii_4;
	// System.Collections.Generic.List`1<System.Boolean> JellySprite::m_FreeModeBodyKinematic
	List_1_t3194695850 * ___m_FreeModeBodyKinematic_5;
	// JellySprite/MassStyle JellySprite::m_MassStyle
	int32_t ___m_MassStyle_6;
	// UnityEngine.PhysicMaterial JellySprite::m_PhysicsMaterial
	PhysicMaterial_t578636151 * ___m_PhysicsMaterial_7;
	// UnityEngine.PhysicsMaterial2D JellySprite::m_PhysicsMaterial2D
	PhysicsMaterial2D_t851691520 * ___m_PhysicsMaterial2D_8;
	// UnityEngine.RigidbodyInterpolation JellySprite::m_Interpolation
	int32_t ___m_Interpolation_9;
	// UnityEngine.RigidbodyInterpolation2D JellySprite::m_Interpolation2D
	int32_t ___m_Interpolation2D_10;
	// UnityEngine.CollisionDetectionMode JellySprite::m_CollisionDetectionMode
	int32_t ___m_CollisionDetectionMode_11;
	// UnityEngine.CollisionDetectionMode2D JellySprite::m_CollisionDetectionMode2D
	int32_t ___m_CollisionDetectionMode2D_12;
	// System.Boolean JellySprite::m_ManualPositioning
	bool ___m_ManualPositioning_13;
	// System.Int32 JellySprite::m_VertexDensity
	int32_t ___m_VertexDensity_14;
	// System.Single JellySprite::m_SphereRadius
	float ___m_SphereRadius_15;
	// System.Single JellySprite::m_DistanceExponent
	float ___m_DistanceExponent_16;
	// System.Single JellySprite::m_Stiffness
	float ___m_Stiffness_17;
	// System.Single JellySprite::m_Mass
	float ___m_Mass_18;
	// System.Boolean JellySprite::m_LockRotation
	bool ___m_LockRotation_19;
	// System.Single JellySprite::m_GravityScale
	float ___m_GravityScale_20;
	// System.Boolean JellySprite::m_UseGravity
	bool ___m_UseGravity_21;
	// System.Boolean JellySprite::m_CollideConnected
	bool ___m_CollideConnected_22;
	// System.Boolean JellySprite::m_CentralBodyKinematic
	bool ___m_CentralBodyKinematic_23;
	// System.Single JellySprite::m_Drag
	float ___m_Drag_24;
	// System.Single JellySprite::m_AngularDrag
	float ___m_AngularDrag_25;
	// System.Int32 JellySprite::m_RadiusPoints
	int32_t ___m_RadiusPoints_26;
	// System.Int32 JellySprite::m_GridColumns
	int32_t ___m_GridColumns_27;
	// System.Int32 JellySprite::m_GridRows
	int32_t ___m_GridRows_28;
	// System.Single JellySprite::m_DampingRatio
	float ___m_DampingRatio_29;
	// UnityEngine.Vector2 JellySprite::m_SoftBodyScale
	Vector2_t2243707579  ___m_SoftBodyScale_30;
	// System.Single JellySprite::m_SoftBodyRotation
	float ___m_SoftBodyRotation_31;
	// UnityEngine.Vector2 JellySprite::m_SoftBodyOffset
	Vector2_t2243707579  ___m_SoftBodyOffset_32;
	// UnityEngine.Vector2 JellySprite::m_CentralBodyOffset
	Vector2_t2243707579  ___m_CentralBodyOffset_33;
	// UnityEngine.Vector2 JellySprite::m_SpriteScale
	Vector2_t2243707579  ___m_SpriteScale_34;
	// System.Boolean JellySprite::m_2DMode
	bool ___m_2DMode_35;
	// System.Boolean JellySprite::m_AttachNeighbors
	bool ___m_AttachNeighbors_36;
	// System.Boolean JellySprite::m_FlipX
	bool ___m_FlipX_37;
	// System.Boolean JellySprite::m_FlipY
	bool ___m_FlipY_38;
	// System.Int32 JellySprite::m_NumAttachPoints
	int32_t ___m_NumAttachPoints_39;
	// UnityEngine.Transform[] JellySprite::m_AttachPoints
	TransformU5BU5D_t3764228911* ___m_AttachPoints_40;
	// UnityEngine.Color JellySprite::m_Color
	Color_t2020392075  ___m_Color_41;
	// UnityEngine.Vector3[] JellySprite::m_Vertices
	Vector3U5BU5D_t1172311765* ___m_Vertices_42;
	// UnityEngine.Vector3[] JellySprite::m_InitialVertexPositions
	Vector3U5BU5D_t1172311765* ___m_InitialVertexPositions_43;
	// UnityEngine.Color[] JellySprite::m_Colors
	ColorU5BU5D_t672350442* ___m_Colors_44;
	// UnityEngine.Vector2[] JellySprite::m_TexCoords
	Vector2U5BU5D_t686124026* ___m_TexCoords_45;
	// System.Int32[] JellySprite::m_Triangles
	Int32U5BU5D_t3030399641* ___m_Triangles_46;
	// UnityEngine.Mesh JellySprite::m_SpriteMesh
	Mesh_t1356156583 * ___m_SpriteMesh_47;
	// System.Collections.Generic.List`1<JellySprite/ReferencePoint> JellySprite::m_ReferencePoints
	List_1_t3979789937 * ___m_ReferencePoints_48;
	// System.Single[0...,0...] JellySprite::m_ReferencePointWeightings
	SingleU5BU2CU5D_t577127398* ___m_ReferencePointWeightings_49;
	// System.Single[0...,0...] JellySprite::m_AttachPointWeightings
	SingleU5BU2CU5D_t577127398* ___m_AttachPointWeightings_50;
	// UnityEngine.Vector3[] JellySprite::m_InitialAttachPointPositions
	Vector3U5BU5D_t1172311765* ___m_InitialAttachPointPositions_51;
	// System.Boolean[] JellySprite::m_IsAttachPointJellySprite
	BooleanU5BU5D_t3568034315* ___m_IsAttachPointJellySprite_52;
	// UnityEngine.GameObject JellySprite::m_ReferencePointParent
	GameObject_t1756533147 * ___m_ReferencePointParent_53;
	// JellySprite/ReferencePoint JellySprite::m_CentralPoint
	ReferencePoint_t315701509 * ___m_CentralPoint_54;
	// UnityEngine.Vector3[] JellySprite::m_ReferencePointOffsets
	Vector3U5BU5D_t1172311765* ___m_ReferencePointOffsets_55;
	// UnityEngine.Transform JellySprite::m_Transform
	Transform_t3275118058 * ___m_Transform_56;

public:
	inline static int32_t get_offset_of_m_Style_2() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Style_2)); }
	inline int32_t get_m_Style_2() const { return ___m_Style_2; }
	inline int32_t* get_address_of_m_Style_2() { return &___m_Style_2; }
	inline void set_m_Style_2(int32_t value)
	{
		___m_Style_2 = value;
	}

	inline static int32_t get_offset_of_m_FreeModeBodyPositions_3() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_FreeModeBodyPositions_3)); }
	inline List_1_t1612828712 * get_m_FreeModeBodyPositions_3() const { return ___m_FreeModeBodyPositions_3; }
	inline List_1_t1612828712 ** get_address_of_m_FreeModeBodyPositions_3() { return &___m_FreeModeBodyPositions_3; }
	inline void set_m_FreeModeBodyPositions_3(List_1_t1612828712 * value)
	{
		___m_FreeModeBodyPositions_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_FreeModeBodyPositions_3, value);
	}

	inline static int32_t get_offset_of_m_FreeModeBodyRadii_4() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_FreeModeBodyRadii_4)); }
	inline List_1_t1445631064 * get_m_FreeModeBodyRadii_4() const { return ___m_FreeModeBodyRadii_4; }
	inline List_1_t1445631064 ** get_address_of_m_FreeModeBodyRadii_4() { return &___m_FreeModeBodyRadii_4; }
	inline void set_m_FreeModeBodyRadii_4(List_1_t1445631064 * value)
	{
		___m_FreeModeBodyRadii_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_FreeModeBodyRadii_4, value);
	}

	inline static int32_t get_offset_of_m_FreeModeBodyKinematic_5() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_FreeModeBodyKinematic_5)); }
	inline List_1_t3194695850 * get_m_FreeModeBodyKinematic_5() const { return ___m_FreeModeBodyKinematic_5; }
	inline List_1_t3194695850 ** get_address_of_m_FreeModeBodyKinematic_5() { return &___m_FreeModeBodyKinematic_5; }
	inline void set_m_FreeModeBodyKinematic_5(List_1_t3194695850 * value)
	{
		___m_FreeModeBodyKinematic_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_FreeModeBodyKinematic_5, value);
	}

	inline static int32_t get_offset_of_m_MassStyle_6() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_MassStyle_6)); }
	inline int32_t get_m_MassStyle_6() const { return ___m_MassStyle_6; }
	inline int32_t* get_address_of_m_MassStyle_6() { return &___m_MassStyle_6; }
	inline void set_m_MassStyle_6(int32_t value)
	{
		___m_MassStyle_6 = value;
	}

	inline static int32_t get_offset_of_m_PhysicsMaterial_7() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_PhysicsMaterial_7)); }
	inline PhysicMaterial_t578636151 * get_m_PhysicsMaterial_7() const { return ___m_PhysicsMaterial_7; }
	inline PhysicMaterial_t578636151 ** get_address_of_m_PhysicsMaterial_7() { return &___m_PhysicsMaterial_7; }
	inline void set_m_PhysicsMaterial_7(PhysicMaterial_t578636151 * value)
	{
		___m_PhysicsMaterial_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_PhysicsMaterial_7, value);
	}

	inline static int32_t get_offset_of_m_PhysicsMaterial2D_8() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_PhysicsMaterial2D_8)); }
	inline PhysicsMaterial2D_t851691520 * get_m_PhysicsMaterial2D_8() const { return ___m_PhysicsMaterial2D_8; }
	inline PhysicsMaterial2D_t851691520 ** get_address_of_m_PhysicsMaterial2D_8() { return &___m_PhysicsMaterial2D_8; }
	inline void set_m_PhysicsMaterial2D_8(PhysicsMaterial2D_t851691520 * value)
	{
		___m_PhysicsMaterial2D_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_PhysicsMaterial2D_8, value);
	}

	inline static int32_t get_offset_of_m_Interpolation_9() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Interpolation_9)); }
	inline int32_t get_m_Interpolation_9() const { return ___m_Interpolation_9; }
	inline int32_t* get_address_of_m_Interpolation_9() { return &___m_Interpolation_9; }
	inline void set_m_Interpolation_9(int32_t value)
	{
		___m_Interpolation_9 = value;
	}

	inline static int32_t get_offset_of_m_Interpolation2D_10() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Interpolation2D_10)); }
	inline int32_t get_m_Interpolation2D_10() const { return ___m_Interpolation2D_10; }
	inline int32_t* get_address_of_m_Interpolation2D_10() { return &___m_Interpolation2D_10; }
	inline void set_m_Interpolation2D_10(int32_t value)
	{
		___m_Interpolation2D_10 = value;
	}

	inline static int32_t get_offset_of_m_CollisionDetectionMode_11() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_CollisionDetectionMode_11)); }
	inline int32_t get_m_CollisionDetectionMode_11() const { return ___m_CollisionDetectionMode_11; }
	inline int32_t* get_address_of_m_CollisionDetectionMode_11() { return &___m_CollisionDetectionMode_11; }
	inline void set_m_CollisionDetectionMode_11(int32_t value)
	{
		___m_CollisionDetectionMode_11 = value;
	}

	inline static int32_t get_offset_of_m_CollisionDetectionMode2D_12() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_CollisionDetectionMode2D_12)); }
	inline int32_t get_m_CollisionDetectionMode2D_12() const { return ___m_CollisionDetectionMode2D_12; }
	inline int32_t* get_address_of_m_CollisionDetectionMode2D_12() { return &___m_CollisionDetectionMode2D_12; }
	inline void set_m_CollisionDetectionMode2D_12(int32_t value)
	{
		___m_CollisionDetectionMode2D_12 = value;
	}

	inline static int32_t get_offset_of_m_ManualPositioning_13() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_ManualPositioning_13)); }
	inline bool get_m_ManualPositioning_13() const { return ___m_ManualPositioning_13; }
	inline bool* get_address_of_m_ManualPositioning_13() { return &___m_ManualPositioning_13; }
	inline void set_m_ManualPositioning_13(bool value)
	{
		___m_ManualPositioning_13 = value;
	}

	inline static int32_t get_offset_of_m_VertexDensity_14() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_VertexDensity_14)); }
	inline int32_t get_m_VertexDensity_14() const { return ___m_VertexDensity_14; }
	inline int32_t* get_address_of_m_VertexDensity_14() { return &___m_VertexDensity_14; }
	inline void set_m_VertexDensity_14(int32_t value)
	{
		___m_VertexDensity_14 = value;
	}

	inline static int32_t get_offset_of_m_SphereRadius_15() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_SphereRadius_15)); }
	inline float get_m_SphereRadius_15() const { return ___m_SphereRadius_15; }
	inline float* get_address_of_m_SphereRadius_15() { return &___m_SphereRadius_15; }
	inline void set_m_SphereRadius_15(float value)
	{
		___m_SphereRadius_15 = value;
	}

	inline static int32_t get_offset_of_m_DistanceExponent_16() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_DistanceExponent_16)); }
	inline float get_m_DistanceExponent_16() const { return ___m_DistanceExponent_16; }
	inline float* get_address_of_m_DistanceExponent_16() { return &___m_DistanceExponent_16; }
	inline void set_m_DistanceExponent_16(float value)
	{
		___m_DistanceExponent_16 = value;
	}

	inline static int32_t get_offset_of_m_Stiffness_17() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Stiffness_17)); }
	inline float get_m_Stiffness_17() const { return ___m_Stiffness_17; }
	inline float* get_address_of_m_Stiffness_17() { return &___m_Stiffness_17; }
	inline void set_m_Stiffness_17(float value)
	{
		___m_Stiffness_17 = value;
	}

	inline static int32_t get_offset_of_m_Mass_18() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Mass_18)); }
	inline float get_m_Mass_18() const { return ___m_Mass_18; }
	inline float* get_address_of_m_Mass_18() { return &___m_Mass_18; }
	inline void set_m_Mass_18(float value)
	{
		___m_Mass_18 = value;
	}

	inline static int32_t get_offset_of_m_LockRotation_19() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_LockRotation_19)); }
	inline bool get_m_LockRotation_19() const { return ___m_LockRotation_19; }
	inline bool* get_address_of_m_LockRotation_19() { return &___m_LockRotation_19; }
	inline void set_m_LockRotation_19(bool value)
	{
		___m_LockRotation_19 = value;
	}

	inline static int32_t get_offset_of_m_GravityScale_20() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_GravityScale_20)); }
	inline float get_m_GravityScale_20() const { return ___m_GravityScale_20; }
	inline float* get_address_of_m_GravityScale_20() { return &___m_GravityScale_20; }
	inline void set_m_GravityScale_20(float value)
	{
		___m_GravityScale_20 = value;
	}

	inline static int32_t get_offset_of_m_UseGravity_21() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_UseGravity_21)); }
	inline bool get_m_UseGravity_21() const { return ___m_UseGravity_21; }
	inline bool* get_address_of_m_UseGravity_21() { return &___m_UseGravity_21; }
	inline void set_m_UseGravity_21(bool value)
	{
		___m_UseGravity_21 = value;
	}

	inline static int32_t get_offset_of_m_CollideConnected_22() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_CollideConnected_22)); }
	inline bool get_m_CollideConnected_22() const { return ___m_CollideConnected_22; }
	inline bool* get_address_of_m_CollideConnected_22() { return &___m_CollideConnected_22; }
	inline void set_m_CollideConnected_22(bool value)
	{
		___m_CollideConnected_22 = value;
	}

	inline static int32_t get_offset_of_m_CentralBodyKinematic_23() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_CentralBodyKinematic_23)); }
	inline bool get_m_CentralBodyKinematic_23() const { return ___m_CentralBodyKinematic_23; }
	inline bool* get_address_of_m_CentralBodyKinematic_23() { return &___m_CentralBodyKinematic_23; }
	inline void set_m_CentralBodyKinematic_23(bool value)
	{
		___m_CentralBodyKinematic_23 = value;
	}

	inline static int32_t get_offset_of_m_Drag_24() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Drag_24)); }
	inline float get_m_Drag_24() const { return ___m_Drag_24; }
	inline float* get_address_of_m_Drag_24() { return &___m_Drag_24; }
	inline void set_m_Drag_24(float value)
	{
		___m_Drag_24 = value;
	}

	inline static int32_t get_offset_of_m_AngularDrag_25() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_AngularDrag_25)); }
	inline float get_m_AngularDrag_25() const { return ___m_AngularDrag_25; }
	inline float* get_address_of_m_AngularDrag_25() { return &___m_AngularDrag_25; }
	inline void set_m_AngularDrag_25(float value)
	{
		___m_AngularDrag_25 = value;
	}

	inline static int32_t get_offset_of_m_RadiusPoints_26() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_RadiusPoints_26)); }
	inline int32_t get_m_RadiusPoints_26() const { return ___m_RadiusPoints_26; }
	inline int32_t* get_address_of_m_RadiusPoints_26() { return &___m_RadiusPoints_26; }
	inline void set_m_RadiusPoints_26(int32_t value)
	{
		___m_RadiusPoints_26 = value;
	}

	inline static int32_t get_offset_of_m_GridColumns_27() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_GridColumns_27)); }
	inline int32_t get_m_GridColumns_27() const { return ___m_GridColumns_27; }
	inline int32_t* get_address_of_m_GridColumns_27() { return &___m_GridColumns_27; }
	inline void set_m_GridColumns_27(int32_t value)
	{
		___m_GridColumns_27 = value;
	}

	inline static int32_t get_offset_of_m_GridRows_28() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_GridRows_28)); }
	inline int32_t get_m_GridRows_28() const { return ___m_GridRows_28; }
	inline int32_t* get_address_of_m_GridRows_28() { return &___m_GridRows_28; }
	inline void set_m_GridRows_28(int32_t value)
	{
		___m_GridRows_28 = value;
	}

	inline static int32_t get_offset_of_m_DampingRatio_29() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_DampingRatio_29)); }
	inline float get_m_DampingRatio_29() const { return ___m_DampingRatio_29; }
	inline float* get_address_of_m_DampingRatio_29() { return &___m_DampingRatio_29; }
	inline void set_m_DampingRatio_29(float value)
	{
		___m_DampingRatio_29 = value;
	}

	inline static int32_t get_offset_of_m_SoftBodyScale_30() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_SoftBodyScale_30)); }
	inline Vector2_t2243707579  get_m_SoftBodyScale_30() const { return ___m_SoftBodyScale_30; }
	inline Vector2_t2243707579 * get_address_of_m_SoftBodyScale_30() { return &___m_SoftBodyScale_30; }
	inline void set_m_SoftBodyScale_30(Vector2_t2243707579  value)
	{
		___m_SoftBodyScale_30 = value;
	}

	inline static int32_t get_offset_of_m_SoftBodyRotation_31() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_SoftBodyRotation_31)); }
	inline float get_m_SoftBodyRotation_31() const { return ___m_SoftBodyRotation_31; }
	inline float* get_address_of_m_SoftBodyRotation_31() { return &___m_SoftBodyRotation_31; }
	inline void set_m_SoftBodyRotation_31(float value)
	{
		___m_SoftBodyRotation_31 = value;
	}

	inline static int32_t get_offset_of_m_SoftBodyOffset_32() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_SoftBodyOffset_32)); }
	inline Vector2_t2243707579  get_m_SoftBodyOffset_32() const { return ___m_SoftBodyOffset_32; }
	inline Vector2_t2243707579 * get_address_of_m_SoftBodyOffset_32() { return &___m_SoftBodyOffset_32; }
	inline void set_m_SoftBodyOffset_32(Vector2_t2243707579  value)
	{
		___m_SoftBodyOffset_32 = value;
	}

	inline static int32_t get_offset_of_m_CentralBodyOffset_33() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_CentralBodyOffset_33)); }
	inline Vector2_t2243707579  get_m_CentralBodyOffset_33() const { return ___m_CentralBodyOffset_33; }
	inline Vector2_t2243707579 * get_address_of_m_CentralBodyOffset_33() { return &___m_CentralBodyOffset_33; }
	inline void set_m_CentralBodyOffset_33(Vector2_t2243707579  value)
	{
		___m_CentralBodyOffset_33 = value;
	}

	inline static int32_t get_offset_of_m_SpriteScale_34() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_SpriteScale_34)); }
	inline Vector2_t2243707579  get_m_SpriteScale_34() const { return ___m_SpriteScale_34; }
	inline Vector2_t2243707579 * get_address_of_m_SpriteScale_34() { return &___m_SpriteScale_34; }
	inline void set_m_SpriteScale_34(Vector2_t2243707579  value)
	{
		___m_SpriteScale_34 = value;
	}

	inline static int32_t get_offset_of_m_2DMode_35() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_2DMode_35)); }
	inline bool get_m_2DMode_35() const { return ___m_2DMode_35; }
	inline bool* get_address_of_m_2DMode_35() { return &___m_2DMode_35; }
	inline void set_m_2DMode_35(bool value)
	{
		___m_2DMode_35 = value;
	}

	inline static int32_t get_offset_of_m_AttachNeighbors_36() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_AttachNeighbors_36)); }
	inline bool get_m_AttachNeighbors_36() const { return ___m_AttachNeighbors_36; }
	inline bool* get_address_of_m_AttachNeighbors_36() { return &___m_AttachNeighbors_36; }
	inline void set_m_AttachNeighbors_36(bool value)
	{
		___m_AttachNeighbors_36 = value;
	}

	inline static int32_t get_offset_of_m_FlipX_37() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_FlipX_37)); }
	inline bool get_m_FlipX_37() const { return ___m_FlipX_37; }
	inline bool* get_address_of_m_FlipX_37() { return &___m_FlipX_37; }
	inline void set_m_FlipX_37(bool value)
	{
		___m_FlipX_37 = value;
	}

	inline static int32_t get_offset_of_m_FlipY_38() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_FlipY_38)); }
	inline bool get_m_FlipY_38() const { return ___m_FlipY_38; }
	inline bool* get_address_of_m_FlipY_38() { return &___m_FlipY_38; }
	inline void set_m_FlipY_38(bool value)
	{
		___m_FlipY_38 = value;
	}

	inline static int32_t get_offset_of_m_NumAttachPoints_39() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_NumAttachPoints_39)); }
	inline int32_t get_m_NumAttachPoints_39() const { return ___m_NumAttachPoints_39; }
	inline int32_t* get_address_of_m_NumAttachPoints_39() { return &___m_NumAttachPoints_39; }
	inline void set_m_NumAttachPoints_39(int32_t value)
	{
		___m_NumAttachPoints_39 = value;
	}

	inline static int32_t get_offset_of_m_AttachPoints_40() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_AttachPoints_40)); }
	inline TransformU5BU5D_t3764228911* get_m_AttachPoints_40() const { return ___m_AttachPoints_40; }
	inline TransformU5BU5D_t3764228911** get_address_of_m_AttachPoints_40() { return &___m_AttachPoints_40; }
	inline void set_m_AttachPoints_40(TransformU5BU5D_t3764228911* value)
	{
		___m_AttachPoints_40 = value;
		Il2CppCodeGenWriteBarrier(&___m_AttachPoints_40, value);
	}

	inline static int32_t get_offset_of_m_Color_41() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Color_41)); }
	inline Color_t2020392075  get_m_Color_41() const { return ___m_Color_41; }
	inline Color_t2020392075 * get_address_of_m_Color_41() { return &___m_Color_41; }
	inline void set_m_Color_41(Color_t2020392075  value)
	{
		___m_Color_41 = value;
	}

	inline static int32_t get_offset_of_m_Vertices_42() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Vertices_42)); }
	inline Vector3U5BU5D_t1172311765* get_m_Vertices_42() const { return ___m_Vertices_42; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Vertices_42() { return &___m_Vertices_42; }
	inline void set_m_Vertices_42(Vector3U5BU5D_t1172311765* value)
	{
		___m_Vertices_42 = value;
		Il2CppCodeGenWriteBarrier(&___m_Vertices_42, value);
	}

	inline static int32_t get_offset_of_m_InitialVertexPositions_43() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_InitialVertexPositions_43)); }
	inline Vector3U5BU5D_t1172311765* get_m_InitialVertexPositions_43() const { return ___m_InitialVertexPositions_43; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_InitialVertexPositions_43() { return &___m_InitialVertexPositions_43; }
	inline void set_m_InitialVertexPositions_43(Vector3U5BU5D_t1172311765* value)
	{
		___m_InitialVertexPositions_43 = value;
		Il2CppCodeGenWriteBarrier(&___m_InitialVertexPositions_43, value);
	}

	inline static int32_t get_offset_of_m_Colors_44() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Colors_44)); }
	inline ColorU5BU5D_t672350442* get_m_Colors_44() const { return ___m_Colors_44; }
	inline ColorU5BU5D_t672350442** get_address_of_m_Colors_44() { return &___m_Colors_44; }
	inline void set_m_Colors_44(ColorU5BU5D_t672350442* value)
	{
		___m_Colors_44 = value;
		Il2CppCodeGenWriteBarrier(&___m_Colors_44, value);
	}

	inline static int32_t get_offset_of_m_TexCoords_45() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_TexCoords_45)); }
	inline Vector2U5BU5D_t686124026* get_m_TexCoords_45() const { return ___m_TexCoords_45; }
	inline Vector2U5BU5D_t686124026** get_address_of_m_TexCoords_45() { return &___m_TexCoords_45; }
	inline void set_m_TexCoords_45(Vector2U5BU5D_t686124026* value)
	{
		___m_TexCoords_45 = value;
		Il2CppCodeGenWriteBarrier(&___m_TexCoords_45, value);
	}

	inline static int32_t get_offset_of_m_Triangles_46() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Triangles_46)); }
	inline Int32U5BU5D_t3030399641* get_m_Triangles_46() const { return ___m_Triangles_46; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_Triangles_46() { return &___m_Triangles_46; }
	inline void set_m_Triangles_46(Int32U5BU5D_t3030399641* value)
	{
		___m_Triangles_46 = value;
		Il2CppCodeGenWriteBarrier(&___m_Triangles_46, value);
	}

	inline static int32_t get_offset_of_m_SpriteMesh_47() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_SpriteMesh_47)); }
	inline Mesh_t1356156583 * get_m_SpriteMesh_47() const { return ___m_SpriteMesh_47; }
	inline Mesh_t1356156583 ** get_address_of_m_SpriteMesh_47() { return &___m_SpriteMesh_47; }
	inline void set_m_SpriteMesh_47(Mesh_t1356156583 * value)
	{
		___m_SpriteMesh_47 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpriteMesh_47, value);
	}

	inline static int32_t get_offset_of_m_ReferencePoints_48() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_ReferencePoints_48)); }
	inline List_1_t3979789937 * get_m_ReferencePoints_48() const { return ___m_ReferencePoints_48; }
	inline List_1_t3979789937 ** get_address_of_m_ReferencePoints_48() { return &___m_ReferencePoints_48; }
	inline void set_m_ReferencePoints_48(List_1_t3979789937 * value)
	{
		___m_ReferencePoints_48 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReferencePoints_48, value);
	}

	inline static int32_t get_offset_of_m_ReferencePointWeightings_49() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_ReferencePointWeightings_49)); }
	inline SingleU5BU2CU5D_t577127398* get_m_ReferencePointWeightings_49() const { return ___m_ReferencePointWeightings_49; }
	inline SingleU5BU2CU5D_t577127398** get_address_of_m_ReferencePointWeightings_49() { return &___m_ReferencePointWeightings_49; }
	inline void set_m_ReferencePointWeightings_49(SingleU5BU2CU5D_t577127398* value)
	{
		___m_ReferencePointWeightings_49 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReferencePointWeightings_49, value);
	}

	inline static int32_t get_offset_of_m_AttachPointWeightings_50() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_AttachPointWeightings_50)); }
	inline SingleU5BU2CU5D_t577127398* get_m_AttachPointWeightings_50() const { return ___m_AttachPointWeightings_50; }
	inline SingleU5BU2CU5D_t577127398** get_address_of_m_AttachPointWeightings_50() { return &___m_AttachPointWeightings_50; }
	inline void set_m_AttachPointWeightings_50(SingleU5BU2CU5D_t577127398* value)
	{
		___m_AttachPointWeightings_50 = value;
		Il2CppCodeGenWriteBarrier(&___m_AttachPointWeightings_50, value);
	}

	inline static int32_t get_offset_of_m_InitialAttachPointPositions_51() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_InitialAttachPointPositions_51)); }
	inline Vector3U5BU5D_t1172311765* get_m_InitialAttachPointPositions_51() const { return ___m_InitialAttachPointPositions_51; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_InitialAttachPointPositions_51() { return &___m_InitialAttachPointPositions_51; }
	inline void set_m_InitialAttachPointPositions_51(Vector3U5BU5D_t1172311765* value)
	{
		___m_InitialAttachPointPositions_51 = value;
		Il2CppCodeGenWriteBarrier(&___m_InitialAttachPointPositions_51, value);
	}

	inline static int32_t get_offset_of_m_IsAttachPointJellySprite_52() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_IsAttachPointJellySprite_52)); }
	inline BooleanU5BU5D_t3568034315* get_m_IsAttachPointJellySprite_52() const { return ___m_IsAttachPointJellySprite_52; }
	inline BooleanU5BU5D_t3568034315** get_address_of_m_IsAttachPointJellySprite_52() { return &___m_IsAttachPointJellySprite_52; }
	inline void set_m_IsAttachPointJellySprite_52(BooleanU5BU5D_t3568034315* value)
	{
		___m_IsAttachPointJellySprite_52 = value;
		Il2CppCodeGenWriteBarrier(&___m_IsAttachPointJellySprite_52, value);
	}

	inline static int32_t get_offset_of_m_ReferencePointParent_53() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_ReferencePointParent_53)); }
	inline GameObject_t1756533147 * get_m_ReferencePointParent_53() const { return ___m_ReferencePointParent_53; }
	inline GameObject_t1756533147 ** get_address_of_m_ReferencePointParent_53() { return &___m_ReferencePointParent_53; }
	inline void set_m_ReferencePointParent_53(GameObject_t1756533147 * value)
	{
		___m_ReferencePointParent_53 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReferencePointParent_53, value);
	}

	inline static int32_t get_offset_of_m_CentralPoint_54() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_CentralPoint_54)); }
	inline ReferencePoint_t315701509 * get_m_CentralPoint_54() const { return ___m_CentralPoint_54; }
	inline ReferencePoint_t315701509 ** get_address_of_m_CentralPoint_54() { return &___m_CentralPoint_54; }
	inline void set_m_CentralPoint_54(ReferencePoint_t315701509 * value)
	{
		___m_CentralPoint_54 = value;
		Il2CppCodeGenWriteBarrier(&___m_CentralPoint_54, value);
	}

	inline static int32_t get_offset_of_m_ReferencePointOffsets_55() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_ReferencePointOffsets_55)); }
	inline Vector3U5BU5D_t1172311765* get_m_ReferencePointOffsets_55() const { return ___m_ReferencePointOffsets_55; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_ReferencePointOffsets_55() { return &___m_ReferencePointOffsets_55; }
	inline void set_m_ReferencePointOffsets_55(Vector3U5BU5D_t1172311765* value)
	{
		___m_ReferencePointOffsets_55 = value;
		Il2CppCodeGenWriteBarrier(&___m_ReferencePointOffsets_55, value);
	}

	inline static int32_t get_offset_of_m_Transform_56() { return static_cast<int32_t>(offsetof(JellySprite_t2775849197, ___m_Transform_56)); }
	inline Transform_t3275118058 * get_m_Transform_56() const { return ___m_Transform_56; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_56() { return &___m_Transform_56; }
	inline void set_m_Transform_56(Transform_t3275118058 * value)
	{
		___m_Transform_56 = value;
		Il2CppCodeGenWriteBarrier(&___m_Transform_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
