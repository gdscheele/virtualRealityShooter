﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlobEyes
struct BlobEyes_t1430531255;

#include "codegen/il2cpp-codegen.h"

// System.Void BlobEyes::.ctor()
extern "C"  void BlobEyes__ctor_m1168675476 (BlobEyes_t1430531255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlobEyes::Start()
extern "C"  void BlobEyes_Start_m3333402840 (BlobEyes_t1430531255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlobEyes::Update()
extern "C"  void BlobEyes_Update_m4146511707 (BlobEyes_t1430531255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
