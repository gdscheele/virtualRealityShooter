﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellySpriteDemoManager
struct  JellySpriteDemoManager_t3490816721  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean JellySpriteDemoManager::m_DrawPhysicsBodies
	bool ___m_DrawPhysicsBodies_2;
	// UnityEngine.GameObject[] JellySpriteDemoManager::m_BlobObjects
	GameObjectU5BU5D_t3057952154* ___m_BlobObjects_3;
	// System.Boolean JellySpriteDemoManager::m_DebugRenderersCreated
	bool ___m_DebugRenderersCreated_4;
	// UnityEngine.Material JellySpriteDemoManager::m_DebugRenderMaterial
	Material_t193706927 * ___m_DebugRenderMaterial_5;

public:
	inline static int32_t get_offset_of_m_DrawPhysicsBodies_2() { return static_cast<int32_t>(offsetof(JellySpriteDemoManager_t3490816721, ___m_DrawPhysicsBodies_2)); }
	inline bool get_m_DrawPhysicsBodies_2() const { return ___m_DrawPhysicsBodies_2; }
	inline bool* get_address_of_m_DrawPhysicsBodies_2() { return &___m_DrawPhysicsBodies_2; }
	inline void set_m_DrawPhysicsBodies_2(bool value)
	{
		___m_DrawPhysicsBodies_2 = value;
	}

	inline static int32_t get_offset_of_m_BlobObjects_3() { return static_cast<int32_t>(offsetof(JellySpriteDemoManager_t3490816721, ___m_BlobObjects_3)); }
	inline GameObjectU5BU5D_t3057952154* get_m_BlobObjects_3() const { return ___m_BlobObjects_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_BlobObjects_3() { return &___m_BlobObjects_3; }
	inline void set_m_BlobObjects_3(GameObjectU5BU5D_t3057952154* value)
	{
		___m_BlobObjects_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_BlobObjects_3, value);
	}

	inline static int32_t get_offset_of_m_DebugRenderersCreated_4() { return static_cast<int32_t>(offsetof(JellySpriteDemoManager_t3490816721, ___m_DebugRenderersCreated_4)); }
	inline bool get_m_DebugRenderersCreated_4() const { return ___m_DebugRenderersCreated_4; }
	inline bool* get_address_of_m_DebugRenderersCreated_4() { return &___m_DebugRenderersCreated_4; }
	inline void set_m_DebugRenderersCreated_4(bool value)
	{
		___m_DebugRenderersCreated_4 = value;
	}

	inline static int32_t get_offset_of_m_DebugRenderMaterial_5() { return static_cast<int32_t>(offsetof(JellySpriteDemoManager_t3490816721, ___m_DebugRenderMaterial_5)); }
	inline Material_t193706927 * get_m_DebugRenderMaterial_5() const { return ___m_DebugRenderMaterial_5; }
	inline Material_t193706927 ** get_address_of_m_DebugRenderMaterial_5() { return &___m_DebugRenderMaterial_5; }
	inline void set_m_DebugRenderMaterial_5(Material_t193706927 * value)
	{
		___m_DebugRenderMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_DebugRenderMaterial_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
