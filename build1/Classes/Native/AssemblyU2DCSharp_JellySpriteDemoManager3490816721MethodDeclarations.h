﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySpriteDemoManager
struct JellySpriteDemoManager_t3490816721;

#include "codegen/il2cpp-codegen.h"

// System.Void JellySpriteDemoManager::.ctor()
extern "C"  void JellySpriteDemoManager__ctor_m2203304140 (JellySpriteDemoManager_t3490816721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteDemoManager::Start()
extern "C"  void JellySpriteDemoManager_Start_m839711292 (JellySpriteDemoManager_t3490816721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteDemoManager::SetSpriteRenderersEnabled(System.Boolean)
extern "C"  void JellySpriteDemoManager_SetSpriteRenderersEnabled_m3095000203 (JellySpriteDemoManager_t3490816721 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteDemoManager::OnGUI()
extern "C"  void JellySpriteDemoManager_OnGUI_m460754540 (JellySpriteDemoManager_t3490816721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
