﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct Collection_1_t838059958;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>[]
struct KeyValuePair_2U5BU5D_t3257983789;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerator_1_t3066806327;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IList_1_t1837255805;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor()
extern "C"  void Collection_1__ctor_m18639995_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18639995(__this, method) ((  void (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1__ctor_m18639995_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1355555354_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1355555354(__this, method) ((  bool (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1355555354_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1565411791_gshared (Collection_1_t838059958 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1565411791(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t838059958 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1565411791_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m809539206_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m809539206(__this, method) ((  Il2CppObject * (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m809539206_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1971663463_gshared (Collection_1_t838059958 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1971663463(__this, ___value0, method) ((  int32_t (*) (Collection_1_t838059958 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1971663463_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1792621447_gshared (Collection_1_t838059958 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1792621447(__this, ___value0, method) ((  bool (*) (Collection_1_t838059958 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1792621447_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3886281469_gshared (Collection_1_t838059958 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3886281469(__this, ___value0, method) ((  int32_t (*) (Collection_1_t838059958 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3886281469_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1669361276_gshared (Collection_1_t838059958 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1669361276(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t838059958 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1669361276_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3523501178_gshared (Collection_1_t838059958 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3523501178(__this, ___value0, method) ((  void (*) (Collection_1_t838059958 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3523501178_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m826373759_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m826373759(__this, method) ((  bool (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m826373759_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1577574659_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1577574659(__this, method) ((  Il2CppObject * (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1577574659_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4021947728_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4021947728(__this, method) ((  bool (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4021947728_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2618072635_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2618072635(__this, method) ((  bool (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2618072635_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m409200086_gshared (Collection_1_t838059958 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m409200086(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t838059958 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m409200086_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2997059465_gshared (Collection_1_t838059958 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2997059465(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t838059958 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2997059465_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Add(T)
extern "C"  void Collection_1_Add_m399168464_gshared (Collection_1_t838059958 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define Collection_1_Add_m399168464(__this, ___item0, method) ((  void (*) (Collection_1_t838059958 *, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_Add_m399168464_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Clear()
extern "C"  void Collection_1_Clear_m1569397836_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1569397836(__this, method) ((  void (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_Clear_m1569397836_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2806845942_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2806845942(__this, method) ((  void (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_ClearItems_m2806845942_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Contains(T)
extern "C"  bool Collection_1_Contains_m1099999706_gshared (Collection_1_t838059958 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1099999706(__this, ___item0, method) ((  bool (*) (Collection_1_t838059958 *, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_Contains_m1099999706_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2458059812_gshared (Collection_1_t838059958 * __this, KeyValuePair_2U5BU5D_t3257983789* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2458059812(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t838059958 *, KeyValuePair_2U5BU5D_t3257983789*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2458059812_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2904489487_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2904489487(__this, method) ((  Il2CppObject* (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_GetEnumerator_m2904489487_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m879207770_gshared (Collection_1_t838059958 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m879207770(__this, ___item0, method) ((  int32_t (*) (Collection_1_t838059958 *, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_IndexOf_m879207770_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1100070429_gshared (Collection_1_t838059958 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1100070429(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t838059958 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_Insert_m1100070429_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m320497190_gshared (Collection_1_t838059958 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m320497190(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t838059958 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_InsertItem_m320497190_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Remove(T)
extern "C"  bool Collection_1_Remove_m1818694069_gshared (Collection_1_t838059958 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1818694069(__this, ___item0, method) ((  bool (*) (Collection_1_t838059958 *, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_Remove_m1818694069_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3369499353_gshared (Collection_1_t838059958 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3369499353(__this, ___index0, method) ((  void (*) (Collection_1_t838059958 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3369499353_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3846254349_gshared (Collection_1_t838059958 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3846254349(__this, ___index0, method) ((  void (*) (Collection_1_t838059958 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3846254349_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3383896215_gshared (Collection_1_t838059958 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3383896215(__this, method) ((  int32_t (*) (Collection_1_t838059958 *, const MethodInfo*))Collection_1_get_Count_m3383896215_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t1296315204  Collection_1_get_Item_m1887209613_gshared (Collection_1_t838059958 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1887209613(__this, ___index0, method) ((  KeyValuePair_2_t1296315204  (*) (Collection_1_t838059958 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1887209613_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2757089734_gshared (Collection_1_t838059958 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2757089734(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t838059958 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_set_Item_m2757089734_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1153147113_gshared (Collection_1_t838059958 * __this, int32_t ___index0, KeyValuePair_2_t1296315204  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1153147113(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t838059958 *, int32_t, KeyValuePair_2_t1296315204 , const MethodInfo*))Collection_1_SetItem_m1153147113_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m497415692_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m497415692(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m497415692_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t1296315204  Collection_1_ConvertItem_m47990380_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m47990380(__this /* static, unused */, ___item0, method) ((  KeyValuePair_2_t1296315204  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m47990380_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3894388776_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3894388776(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3894388776_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1708275622_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1708275622(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1708275622_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3671711005_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3671711005(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3671711005_gshared)(__this /* static, unused */, ___list0, method)
