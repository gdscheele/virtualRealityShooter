﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityJellySprite
struct UnityJellySprite_t1914834834;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void UnityJellySprite::.ctor()
extern "C"  void UnityJellySprite__ctor_m2921099543 (UnityJellySprite_t1914834834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityJellySprite::ClearMaterials()
extern "C"  void UnityJellySprite_ClearMaterials_m2895776592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityJellySprite::GetSpriteBounds()
extern "C"  Bounds_t3033363703  UnityJellySprite_GetSpriteBounds_m1281440482 (UnityJellySprite_t1914834834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityJellySprite::IsSpriteValid()
extern "C"  bool UnityJellySprite_IsSpriteValid_m3636200702 (UnityJellySprite_t1914834834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityJellySprite::IsSourceSpriteRotated()
extern "C"  bool UnityJellySprite_IsSourceSpriteRotated_m2535271706 (UnityJellySprite_t1914834834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityJellySprite::GetMinMaxTextureRect(UnityEngine.Vector2&,UnityEngine.Vector2&)
extern "C"  void UnityJellySprite_GetMinMaxTextureRect_m586426000 (UnityJellySprite_t1914834834 * __this, Vector2_t2243707579 * ___min0, Vector2_t2243707579 * ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityJellySprite::InitMaterial()
extern "C"  void UnityJellySprite_InitMaterial_m2640738906 (UnityJellySprite_t1914834834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityJellySprite::.cctor()
extern "C"  void UnityJellySprite__cctor_m1129202308 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
