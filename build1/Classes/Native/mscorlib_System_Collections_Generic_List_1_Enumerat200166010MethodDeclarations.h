﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct List_1_t665436336;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat200166010.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1052161637_gshared (Enumerator_t200166010 * __this, List_1_t665436336 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1052161637(__this, ___l0, method) ((  void (*) (Enumerator_t200166010 *, List_1_t665436336 *, const MethodInfo*))Enumerator__ctor_m1052161637_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3289791189_gshared (Enumerator_t200166010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3289791189(__this, method) ((  void (*) (Enumerator_t200166010 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3289791189_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m901040161_gshared (Enumerator_t200166010 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m901040161(__this, method) ((  Il2CppObject * (*) (Enumerator_t200166010 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m901040161_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m395154078_gshared (Enumerator_t200166010 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m395154078(__this, method) ((  void (*) (Enumerator_t200166010 *, const MethodInfo*))Enumerator_Dispose_m395154078_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m68255719_gshared (Enumerator_t200166010 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m68255719(__this, method) ((  void (*) (Enumerator_t200166010 *, const MethodInfo*))Enumerator_VerifyState_m68255719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1944927533_gshared (Enumerator_t200166010 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1944927533(__this, method) ((  bool (*) (Enumerator_t200166010 *, const MethodInfo*))Enumerator_MoveNext_m1944927533_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1296315204  Enumerator_get_Current_m1712489382_gshared (Enumerator_t200166010 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1712489382(__this, method) ((  KeyValuePair_2_t1296315204  (*) (Enumerator_t200166010 *, const MethodInfo*))Enumerator_get_Current_m1712489382_gshared)(__this, method)
