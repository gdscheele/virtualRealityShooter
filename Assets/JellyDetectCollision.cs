﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyDetectCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnJellyCollisionEnter(JellySprite.JellyCollision collision) {
		if (gameObject != null) {
			if (collision.Collision.gameObject.name.Contains("Projectile")) {
				Destroy (gameObject);
			}
		}
	}
}
