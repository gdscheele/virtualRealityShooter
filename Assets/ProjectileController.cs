﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {

	public GameObject projectile;
	private float projectileSpeed = 500;
	private float timeOfLastProjectile;
	private int timeUntilDestruction = 5;

	// Use this for initialization
	void Start () {
		timeOfLastProjectile = Time.time;
	}

	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown && !Input.GetKeyDown("left alt") && !Input.GetKeyDown("left ctrl")) {
			if (Time.time - timeOfLastProjectile > 0) {
				timeOfLastProjectile = Time.time;
				fireProjectile ();
			}
		}
	}

	private void fireProjectile() {
		//Create projectile clone
		GameObject clone;
		clone = Instantiate(projectile, Camera.main.transform.position, Camera.main.transform.rotation);

		// Add force to the cloned object in the object's forward direction
		clone.transform.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * projectileSpeed);
		Destroy (clone, timeUntilDestruction);
	}


}
