﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndEffects : MonoBehaviour {

	private Vector3 donePosition;
	private PointController pc;

	public GameObject endCanvas;
	public GameObject endTextObject;
	private Text endText;

	public GameObject scoreTextObject;

	// Use this for initialization
	void Start () {
		donePosition = new Vector3 ((float) -110.78, (float) -17.85, (float) 0.43);
		pc = Camera.main.GetComponent<PointController>();

		this.endText = endTextObject.GetComponent<Text> ();
		endCanvas.SetActive (false);
	}

	void Update () {
		if (Camera.main.transform.position.Equals (donePosition)) {
			endCanvas.SetActive (true);
			this.endText.text = "FINAL SCORE: " + pc.getTotalPoints() + "/470";
			scoreTextObject.SetActive (false);
		}
		
	}

}
