﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointController : MonoBehaviour {

	public GameObject scoreTextObject;
	private Text scoreText;
	private int totalPoints = 0;

	// Use this for initialization
	void Start () {
		this.scoreText = scoreTextObject.GetComponent<Text> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void addPoint() {
		totalPoints = totalPoints+10;
		this.scoreText.text = "Score: " + totalPoints;
		print ("Total points = " + totalPoints);
	}

	public int getTotalPoints() {
		return totalPoints;
	}
}
