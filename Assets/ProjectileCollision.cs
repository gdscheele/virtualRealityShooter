﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCollision : MonoBehaviour {

	public GameObject camera;
	private PointController pointController;
	private bool deleted = false;

	// Use this for initialization
	void Start () {
		this.pointController = (PointController) camera.GetComponent (typeof(PointController));

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter (Collision col) {
		if (col.gameObject.name.Contains ("JellySprite") && !deleted) {
			var index = col.gameObject.name.IndexOf (")");
			var jellySpriteName = col.gameObject.name.Substring (0, index+1);
			GameObject jellySprite = GameObject.Find (jellySpriteName);
			if (jellySprite != null) {
				deleted = true;
				pointController.addPoint ();
				Destroy (jellySprite);
				Destroy (this.gameObject);
			}
		}
	}
}