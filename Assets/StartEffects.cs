﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StartEffects : MonoBehaviour {

	public GameObject warp;
	public SplineController splineController;
	// Use this for initialization
	void Start()
	{
		warp.SetActive (false);
		splineController = Camera.main.GetComponent<SplineController>();
	}

	void OnCollisionEnter (Collision col) {
		this.gameObject.SetActive (false);  //canvas and text disappear
		warp.SetActive (true); //turn warp on
		splineController.FollowSpline (); //start camera
		Destroy (col.gameObject); //delete projectile that hit it
	}
}