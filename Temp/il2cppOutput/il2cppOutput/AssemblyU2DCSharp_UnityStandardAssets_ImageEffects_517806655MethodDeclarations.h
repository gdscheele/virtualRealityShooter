﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.ImageEffects.ImageEffectBase
struct ImageEffectBase_t517806655;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern "C"  void ImageEffectBase__ctor_m2057110790 (ImageEffectBase_t517806655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern "C"  void ImageEffectBase_Start_m1844470210 (ImageEffectBase_t517806655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern "C"  Material_t193706927 * ImageEffectBase_get_material_m4218440043 (ImageEffectBase_t517806655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern "C"  void ImageEffectBase_OnDisable_m126847887 (ImageEffectBase_t517806655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
