﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_893614575MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::.ctor()
#define SortedDictionary_2__ctor_m1811695571(__this, method) ((  void (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2__ctor_m4272885381_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define SortedDictionary_2__ctor_m1343695189(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t848404470 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m168882300_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m575911121(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t848404470 *, KeyValuePair_2_t1251105099 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443128540_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1388902203(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t848404470 *, KeyValuePair_2_t1251105099 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m428329100_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1683676182(__this, method) ((  bool (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1185129343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4140669672(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t848404470 *, KeyValuePair_2_t1251105099 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2552204537_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1002302766(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t848404470 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1174244715_gshared)(__this, ___key0, ___value1, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.IDictionary.GetEnumerator()
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1053893201(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1463738588_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.IDictionary.Remove(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m982156881(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t848404470 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m1302414550_gshared)(__this, ___key0, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.IDictionary.get_Item(System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m300017630(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t848404470 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m1643017117_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m765532669(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t848404470 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m208457114_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m1070315152(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t848404470 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m943240533_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.ICollection.get_IsSynchronized()
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m250414916(__this, method) ((  bool (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m3410106573_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.ICollection.get_SyncRoot()
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m2250144186(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m4291614277_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.IEnumerable.GetEnumerator()
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2830914795(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1530534042_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m584396716(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4085059655_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::get_Count()
#define SortedDictionary_2_get_Count_m261042827(__this, method) ((  int32_t (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_get_Count_m3421650989_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::get_Item(TKey)
#define SortedDictionary_2_get_Item_m348313885(__this, ___key0, method) ((  List_1_t2644239190 * (*) (SortedDictionary_2_t848404470 *, float, const MethodInfo*))SortedDictionary_2_get_Item_m3202656644_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::set_Item(TKey,TValue)
#define SortedDictionary_2_set_Item_m3273628938(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t848404470 *, float, List_1_t2644239190 *, const MethodInfo*))SortedDictionary_2_set_Item_m455198517_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::Add(TKey,TValue)
#define SortedDictionary_2_Add_m3393533651(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t848404470 *, float, List_1_t2644239190 *, const MethodInfo*))SortedDictionary_2_Add_m224572588_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::Clear()
#define SortedDictionary_2_Clear_m2223853939(__this, method) ((  void (*) (SortedDictionary_2_t848404470 *, const MethodInfo*))SortedDictionary_2_Clear_m1406209816_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SortedDictionary_2_CopyTo_m1956700646(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t848404470 *, KeyValuePair_2U5BU5D_t3471956266*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m3355616249_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::Remove(TKey)
#define SortedDictionary_2_Remove_m2498269619(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t848404470 *, float, const MethodInfo*))SortedDictionary_2_Remove_m1650665524_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::TryGetValue(TKey,TValue&)
#define SortedDictionary_2_TryGetValue_m2196827702(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t848404470 *, float, List_1_t2644239190 **, const MethodInfo*))SortedDictionary_2_TryGetValue_m247917393_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::ToKey(System.Object)
#define SortedDictionary_2_ToKey_m3231411384(__this, ___key0, method) ((  float (*) (SortedDictionary_2_t848404470 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m3085476615_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::ToValue(System.Object)
#define SortedDictionary_2_ToValue_m2558220648(__this, ___value0, method) ((  List_1_t2644239190 * (*) (SortedDictionary_2_t848404470 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m1263474151_gshared)(__this, ___value0, method)
