﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Object>
struct NodeHelper_t1846780193;
// System.Collections.Generic.IComparer`1<System.Single>
struct IComparer_1_t30973054;
// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"

// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void NodeHelper__ctor_m3658842787_gshared (NodeHelper_t1846780193 * __this, Il2CppObject* ___cmp0, const MethodInfo* method);
#define NodeHelper__ctor_m3658842787(__this, ___cmp0, method) ((  void (*) (NodeHelper_t1846780193 *, Il2CppObject*, const MethodInfo*))NodeHelper__ctor_m3658842787_gshared)(__this, ___cmp0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Object>::.cctor()
extern "C"  void NodeHelper__cctor_m4163778587_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NodeHelper__cctor_m4163778587(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NodeHelper__cctor_m4163778587_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Object>::Compare(TKey,System.Collections.Generic.RBTree/Node)
extern "C"  int32_t NodeHelper_Compare_m852104552_gshared (NodeHelper_t1846780193 * __this, float ___key0, Node_t2499136326 * ___node1, const MethodInfo* method);
#define NodeHelper_Compare_m852104552(__this, ___key0, ___node1, method) ((  int32_t (*) (NodeHelper_t1846780193 *, float, Node_t2499136326 *, const MethodInfo*))NodeHelper_Compare_m852104552_gshared)(__this, ___key0, ___node1, method)
// System.Collections.Generic.RBTree/Node System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Object>::CreateNode(TKey)
extern "C"  Node_t2499136326 * NodeHelper_CreateNode_m4169738924_gshared (NodeHelper_t1846780193 * __this, float ___key0, const MethodInfo* method);
#define NodeHelper_CreateNode_m4169738924(__this, ___key0, method) ((  Node_t2499136326 * (*) (NodeHelper_t1846780193 *, float, const MethodInfo*))NodeHelper_CreateNode_m4169738924_gshared)(__this, ___key0, method)
// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Single,System.Object>::GetHelper(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  NodeHelper_t1846780193 * NodeHelper_GetHelper_m1812397633_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___cmp0, const MethodInfo* method);
#define NodeHelper_GetHelper_m1812397633(__this /* static, unused */, ___cmp0, method) ((  NodeHelper_t1846780193 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))NodeHelper_GetHelper_m1812397633_gshared)(__this /* static, unused */, ___cmp0, method)
