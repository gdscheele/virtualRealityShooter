﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MathUtils
struct MathUtils_t4163796929;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void MathUtils::.ctor()
extern "C"  void MathUtils__ctor_m378669622 (MathUtils_t4163796929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathUtils::GetQuatLength(UnityEngine.Quaternion)
extern "C"  float MathUtils_GetQuatLength_m4186010974 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MathUtils::GetQuatConjugate(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  MathUtils_GetQuatConjugate_m1754587108 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MathUtils::GetQuatLog(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  MathUtils_GetQuatLog_m2776604280 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MathUtils::GetQuatExp(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  MathUtils_GetQuatExp_m1486123925 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MathUtils::GetQuatSquad(System.Single,UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  MathUtils_GetQuatSquad_m1337847938 (Il2CppObject * __this /* static, unused */, float ___t0, Quaternion_t4030073918  ___q01, Quaternion_t4030073918  ___q12, Quaternion_t4030073918  ___a03, Quaternion_t4030073918  ___a14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MathUtils::GetSquadIntermediate(UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  MathUtils_GetSquadIntermediate_m2765737822 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___q00, Quaternion_t4030073918  ___q11, Quaternion_t4030073918  ___q22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MathUtils::Ease(System.Single,System.Single,System.Single)
extern "C"  float MathUtils_Ease_m2964578217 (Il2CppObject * __this /* static, unused */, float ___t0, float ___k11, float ___k22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion MathUtils::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  MathUtils_Slerp_m2208538353 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___p0, Quaternion_t4030073918  ___q1, float ___t2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
