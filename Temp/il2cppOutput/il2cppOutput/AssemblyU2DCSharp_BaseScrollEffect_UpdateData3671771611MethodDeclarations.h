﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseScrollEffect/UpdateData
struct UpdateData_t3671771611;
struct UpdateData_t3671771611_marshaled_pinvoke;
struct UpdateData_t3671771611_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct UpdateData_t3671771611;
struct UpdateData_t3671771611_marshaled_pinvoke;

extern "C" void UpdateData_t3671771611_marshal_pinvoke(const UpdateData_t3671771611& unmarshaled, UpdateData_t3671771611_marshaled_pinvoke& marshaled);
extern "C" void UpdateData_t3671771611_marshal_pinvoke_back(const UpdateData_t3671771611_marshaled_pinvoke& marshaled, UpdateData_t3671771611& unmarshaled);
extern "C" void UpdateData_t3671771611_marshal_pinvoke_cleanup(UpdateData_t3671771611_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct UpdateData_t3671771611;
struct UpdateData_t3671771611_marshaled_com;

extern "C" void UpdateData_t3671771611_marshal_com(const UpdateData_t3671771611& unmarshaled, UpdateData_t3671771611_marshaled_com& marshaled);
extern "C" void UpdateData_t3671771611_marshal_com_back(const UpdateData_t3671771611_marshaled_com& marshaled, UpdateData_t3671771611& unmarshaled);
extern "C" void UpdateData_t3671771611_marshal_com_cleanup(UpdateData_t3671771611_marshaled_com& marshaled);
