﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct DefaultComparer_t2918245365;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2386846562_gshared (DefaultComparer_t2918245365 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2386846562(__this, method) ((  void (*) (DefaultComparer_t2918245365 *, const MethodInfo*))DefaultComparer__ctor_m2386846562_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2768782617_gshared (DefaultComparer_t2918245365 * __this, KeyValuePair_2_t1296315204  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2768782617(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2918245365 *, KeyValuePair_2_t1296315204 , const MethodInfo*))DefaultComparer_GetHashCode_m2768782617_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m4133699301_gshared (DefaultComparer_t2918245365 * __this, KeyValuePair_2_t1296315204  ___x0, KeyValuePair_2_t1296315204  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m4133699301(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2918245365 *, KeyValuePair_2_t1296315204 , KeyValuePair_2_t1296315204 , const MethodInfo*))DefaultComparer_Equals_m4133699301_gshared)(__this, ___x0, ___y1, method)
