﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void JellySpriteReferencePoint::.ctor()
extern "C"  void JellySpriteReferencePoint__ctor_m1383098303 (JellySpriteReferencePoint_t291100992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject JellySpriteReferencePoint::get_ParentJellySprite()
extern "C"  GameObject_t1756533147 * JellySpriteReferencePoint_get_ParentJellySprite_m808904112 (JellySpriteReferencePoint_t291100992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::set_ParentJellySprite(UnityEngine.GameObject)
extern "C"  void JellySpriteReferencePoint_set_ParentJellySprite_m2694165905 (JellySpriteReferencePoint_t291100992 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JellySpriteReferencePoint::get_SendCollisionMessages()
extern "C"  bool JellySpriteReferencePoint_get_SendCollisionMessages_m524664508 (JellySpriteReferencePoint_t291100992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::set_SendCollisionMessages(System.Boolean)
extern "C"  void JellySpriteReferencePoint_set_SendCollisionMessages_m625481459 (JellySpriteReferencePoint_t291100992 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JellySpriteReferencePoint::get_Index()
extern "C"  int32_t JellySpriteReferencePoint_get_Index_m178753290 (JellySpriteReferencePoint_t291100992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::set_Index(System.Int32)
extern "C"  void JellySpriteReferencePoint_set_Index_m2148147417 (JellySpriteReferencePoint_t291100992 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void JellySpriteReferencePoint_OnCollisionEnter_m4250096761 (JellySpriteReferencePoint_t291100992 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void JellySpriteReferencePoint_OnCollisionEnter2D_m3434768217 (JellySpriteReferencePoint_t291100992 * __this, Collision2D_t1539500754 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnCollisionExit(UnityEngine.Collision)
extern "C"  void JellySpriteReferencePoint_OnCollisionExit_m691873449 (JellySpriteReferencePoint_t291100992 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void JellySpriteReferencePoint_OnCollisionExit2D_m2199725833 (JellySpriteReferencePoint_t291100992 * __this, Collision2D_t1539500754 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnCollisionStay(UnityEngine.Collision)
extern "C"  void JellySpriteReferencePoint_OnCollisionStay_m3746370898 (JellySpriteReferencePoint_t291100992 * __this, Collision_t2876846408 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void JellySpriteReferencePoint_OnCollisionStay2D_m1319275570 (JellySpriteReferencePoint_t291100992 * __this, Collision2D_t1539500754 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void JellySpriteReferencePoint_OnTriggerEnter_m2295282499 (JellySpriteReferencePoint_t291100992 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void JellySpriteReferencePoint_OnTriggerEnter2D_m1376931875 (JellySpriteReferencePoint_t291100992 * __this, Collider2D_t646061738 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnTriggerExit(UnityEngine.Collider)
extern "C"  void JellySpriteReferencePoint_OnTriggerExit_m1739580623 (JellySpriteReferencePoint_t291100992 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void JellySpriteReferencePoint_OnTriggerExit2D_m3485143855 (JellySpriteReferencePoint_t291100992 * __this, Collider2D_t646061738 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnTriggerStay(UnityEngine.Collider)
extern "C"  void JellySpriteReferencePoint_OnTriggerStay_m1098721664 (JellySpriteReferencePoint_t291100992 * __this, Collider_t3497673348 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySpriteReferencePoint::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void JellySpriteReferencePoint_OnTriggerStay2D_m64228804 (JellySpriteReferencePoint_t291100992 * __this, Collider2D_t646061738 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
