﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointController
struct  PointController_t705534758  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PointController::scoreTextObject
	GameObject_t1756533147 * ___scoreTextObject_2;
	// UnityEngine.UI.Text PointController::scoreText
	Text_t356221433 * ___scoreText_3;
	// System.Int32 PointController::totalPoints
	int32_t ___totalPoints_4;

public:
	inline static int32_t get_offset_of_scoreTextObject_2() { return static_cast<int32_t>(offsetof(PointController_t705534758, ___scoreTextObject_2)); }
	inline GameObject_t1756533147 * get_scoreTextObject_2() const { return ___scoreTextObject_2; }
	inline GameObject_t1756533147 ** get_address_of_scoreTextObject_2() { return &___scoreTextObject_2; }
	inline void set_scoreTextObject_2(GameObject_t1756533147 * value)
	{
		___scoreTextObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___scoreTextObject_2, value);
	}

	inline static int32_t get_offset_of_scoreText_3() { return static_cast<int32_t>(offsetof(PointController_t705534758, ___scoreText_3)); }
	inline Text_t356221433 * get_scoreText_3() const { return ___scoreText_3; }
	inline Text_t356221433 ** get_address_of_scoreText_3() { return &___scoreText_3; }
	inline void set_scoreText_3(Text_t356221433 * value)
	{
		___scoreText_3 = value;
		Il2CppCodeGenWriteBarrier(&___scoreText_3, value);
	}

	inline static int32_t get_offset_of_totalPoints_4() { return static_cast<int32_t>(offsetof(PointController_t705534758, ___totalPoints_4)); }
	inline int32_t get_totalPoints_4() const { return ___totalPoints_4; }
	inline int32_t* get_address_of_totalPoints_4() { return &___totalPoints_4; }
	inline void set_totalPoints_4(int32_t value)
	{
		___totalPoints_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
