﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// UnityEngine.SphereCollider
struct SphereCollider_t1662511355;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellySprite/ReferencePoint
struct  ReferencePoint_t315701509  : public Il2CppObject
{
public:
	// UnityEngine.Transform JellySprite/ReferencePoint::m_Transform
	Transform_t3275118058 * ___m_Transform_0;
	// UnityEngine.Rigidbody2D JellySprite/ReferencePoint::m_RigidBody2D
	Rigidbody2D_t502193897 * ___m_RigidBody2D_1;
	// UnityEngine.Rigidbody JellySprite/ReferencePoint::m_RigidBody3D
	Rigidbody_t4233889191 * ___m_RigidBody3D_2;
	// UnityEngine.CircleCollider2D JellySprite/ReferencePoint::m_CircleCollider2D
	CircleCollider2D_t13116344 * ___m_CircleCollider2D_3;
	// UnityEngine.SphereCollider JellySprite/ReferencePoint::m_SphereCollider
	SphereCollider_t1662511355 * ___m_SphereCollider_4;
	// UnityEngine.Vector3 JellySprite/ReferencePoint::m_InitialOffset
	Vector3_t2243707580  ___m_InitialOffset_5;
	// System.Boolean JellySprite/ReferencePoint::m_IsDummy
	bool ___m_IsDummy_6;

public:
	inline static int32_t get_offset_of_m_Transform_0() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_Transform_0)); }
	inline Transform_t3275118058 * get_m_Transform_0() const { return ___m_Transform_0; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_0() { return &___m_Transform_0; }
	inline void set_m_Transform_0(Transform_t3275118058 * value)
	{
		___m_Transform_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Transform_0, value);
	}

	inline static int32_t get_offset_of_m_RigidBody2D_1() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_RigidBody2D_1)); }
	inline Rigidbody2D_t502193897 * get_m_RigidBody2D_1() const { return ___m_RigidBody2D_1; }
	inline Rigidbody2D_t502193897 ** get_address_of_m_RigidBody2D_1() { return &___m_RigidBody2D_1; }
	inline void set_m_RigidBody2D_1(Rigidbody2D_t502193897 * value)
	{
		___m_RigidBody2D_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_RigidBody2D_1, value);
	}

	inline static int32_t get_offset_of_m_RigidBody3D_2() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_RigidBody3D_2)); }
	inline Rigidbody_t4233889191 * get_m_RigidBody3D_2() const { return ___m_RigidBody3D_2; }
	inline Rigidbody_t4233889191 ** get_address_of_m_RigidBody3D_2() { return &___m_RigidBody3D_2; }
	inline void set_m_RigidBody3D_2(Rigidbody_t4233889191 * value)
	{
		___m_RigidBody3D_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_RigidBody3D_2, value);
	}

	inline static int32_t get_offset_of_m_CircleCollider2D_3() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_CircleCollider2D_3)); }
	inline CircleCollider2D_t13116344 * get_m_CircleCollider2D_3() const { return ___m_CircleCollider2D_3; }
	inline CircleCollider2D_t13116344 ** get_address_of_m_CircleCollider2D_3() { return &___m_CircleCollider2D_3; }
	inline void set_m_CircleCollider2D_3(CircleCollider2D_t13116344 * value)
	{
		___m_CircleCollider2D_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CircleCollider2D_3, value);
	}

	inline static int32_t get_offset_of_m_SphereCollider_4() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_SphereCollider_4)); }
	inline SphereCollider_t1662511355 * get_m_SphereCollider_4() const { return ___m_SphereCollider_4; }
	inline SphereCollider_t1662511355 ** get_address_of_m_SphereCollider_4() { return &___m_SphereCollider_4; }
	inline void set_m_SphereCollider_4(SphereCollider_t1662511355 * value)
	{
		___m_SphereCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_SphereCollider_4, value);
	}

	inline static int32_t get_offset_of_m_InitialOffset_5() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_InitialOffset_5)); }
	inline Vector3_t2243707580  get_m_InitialOffset_5() const { return ___m_InitialOffset_5; }
	inline Vector3_t2243707580 * get_address_of_m_InitialOffset_5() { return &___m_InitialOffset_5; }
	inline void set_m_InitialOffset_5(Vector3_t2243707580  value)
	{
		___m_InitialOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_IsDummy_6() { return static_cast<int32_t>(offsetof(ReferencePoint_t315701509, ___m_IsDummy_6)); }
	inline bool get_m_IsDummy_6() const { return ___m_IsDummy_6; }
	inline bool* get_address_of_m_IsDummy_6() { return &___m_IsDummy_6; }
	inline void set_m_IsDummy_6(bool value)
	{
		___m_IsDummy_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
