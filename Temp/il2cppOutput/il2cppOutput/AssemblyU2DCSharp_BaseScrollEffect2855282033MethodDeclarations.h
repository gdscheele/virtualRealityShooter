﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseScrollEffect
struct BaseScrollEffect_t2855282033;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseScrollEffect::.ctor()
extern "C"  void BaseScrollEffect__ctor_m560937132 (BaseScrollEffect_t2855282033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
