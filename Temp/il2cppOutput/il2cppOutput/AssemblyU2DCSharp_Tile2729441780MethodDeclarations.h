﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tile
struct Tile_t2729441780;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// TiledPage
struct TiledPage_t4183784445;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Tile::.ctor()
extern "C"  void Tile__ctor_m3293075927 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform Tile::get_Cell()
extern "C"  RectTransform_t3349966182 * Tile_get_Cell_m1439498426 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tile::get_IsInteractable()
extern "C"  bool Tile_get_IsInteractable_m2069796222 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::set_IsInteractable(System.Boolean)
extern "C"  void Tile_set_IsInteractable_m3435349787 (Tile_t2729441780 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::Awake()
extern "C"  void Tile_Awake_m3924417876 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Tile_OnPointerEnter_m1648883309 (Tile_t2729441780 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Tile_OnPointerExit_m1757481905 (Tile_t2729441780 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::OnGvrPointerHover(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Tile_OnGvrPointerHover_m1310128628 (Tile_t2729441780 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::Update()
extern "C"  void Tile_Update_m3434087094 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tile::UpdateDesiredRotation(UnityEngine.Vector3)
extern "C"  void Tile_UpdateDesiredRotation_m3355773067 (Tile_t2729441780 * __this, Vector3_t2243707580  ___pointerIntersectionWorldPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Tile::CalculateLocalSize()
extern "C"  Vector2_t2243707579  Tile_CalculateLocalSize_m1136322974 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Tile::CalculateLocalCenter()
extern "C"  Vector3_t2243707580  Tile_CalculateLocalCenter_m85181063 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TiledPage Tile::GetPage()
extern "C"  TiledPage_t4183784445 * Tile_GetPage_m1179628508 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tile::GetMetersToCanvasScale()
extern "C"  float Tile_GetMetersToCanvasScale_m4089707950 (Tile_t2729441780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Tile::clampEuler(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Tile_clampEuler_m4203085409 (Tile_t2729441780 * __this, Vector3_t2243707580  ___rotation0, float ___maxDegrees1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Tile::clampDegrees(System.Single,System.Single)
extern "C"  float Tile_clampDegrees_m2135874777 (Tile_t2729441780 * __this, float ___degrees0, float ___maxDegrees1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
