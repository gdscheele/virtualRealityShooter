﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m352643098(__this, ___l0, method) ((  void (*) (Enumerator_t777527636 *, List_1_t1242797962 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3932607176(__this, method) ((  void (*) (Enumerator_t777527636 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4244044992(__this, method) ((  Il2CppObject * (*) (Enumerator_t777527636 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m1430227501(__this, method) ((  void (*) (Enumerator_t777527636 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m1051607588(__this, method) ((  void (*) (Enumerator_t777527636 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m1246978084(__this, method) ((  bool (*) (Enumerator_t777527636 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Action`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m810353445(__this, method) ((  Action_1_t1873676830 * (*) (Enumerator_t777527636 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
