﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1489064018(__this, ___l0, method) ((  void (*) (Enumerator_t3514519611 *, List_1_t3979789937 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1272393664(__this, method) ((  void (*) (Enumerator_t3514519611 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3706261374(__this, method) ((  Il2CppObject * (*) (Enumerator_t3514519611 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::Dispose()
#define Enumerator_Dispose_m852892585(__this, method) ((  void (*) (Enumerator_t3514519611 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::VerifyState()
#define Enumerator_VerifyState_m3549877124(__this, method) ((  void (*) (Enumerator_t3514519611 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::MoveNext()
#define Enumerator_MoveNext_m3195451723(__this, method) ((  bool (*) (Enumerator_t3514519611 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<JellySprite/ReferencePoint>::get_Current()
#define Enumerator_get_Current_m1198644367(__this, method) ((  ReferencePoint_t315701509 * (*) (Enumerator_t3514519611 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
