﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>
struct SortedDictionary_2_t893614575;
// System.Collections.Generic.IComparer`1<System.Single>
struct IComparer_1_t30973054;
// System.Object
struct Il2CppObject;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>>
struct IEnumerator_1_t3066806327;
// System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>[]
struct KeyValuePair_2U5BU5D_t3257983789;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::.ctor()
extern "C"  void SortedDictionary_2__ctor_m4272885381_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2__ctor_m4272885381(__this, method) ((  void (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2__ctor_m4272885381_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
extern "C"  void SortedDictionary_2__ctor_m168882300_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define SortedDictionary_2__ctor_m168882300(__this, ___comparer0, method) ((  void (*) (SortedDictionary_2_t893614575 *, Il2CppObject*, const MethodInfo*))SortedDictionary_2__ctor_m168882300_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443128540_gshared (SortedDictionary_2_t893614575 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443128540(__this, ___item0, method) ((  void (*) (SortedDictionary_2_t893614575 *, KeyValuePair_2_t1296315204 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443128540_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m428329100_gshared (SortedDictionary_2_t893614575 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m428329100(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t893614575 *, KeyValuePair_2_t1296315204 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m428329100_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1185129343_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1185129343(__this, method) ((  bool (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1185129343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2552204537_gshared (SortedDictionary_2_t893614575 * __this, KeyValuePair_2_t1296315204  ___item0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2552204537(__this, ___item0, method) ((  bool (*) (SortedDictionary_2_t893614575 *, KeyValuePair_2_t1296315204 , const MethodInfo*))SortedDictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2552204537_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Add_m1174244715_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Add_m1174244715(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t893614575 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Add_m1174244715_gshared)(__this, ___key0, ___value1, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1463738588_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1463738588(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_GetEnumerator_m1463738588_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_Remove_m1302414550_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_Remove_m1302414550(__this, ___key0, method) ((  void (*) (SortedDictionary_2_t893614575 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_Remove_m1302414550_gshared)(__this, ___key0, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IDictionary_get_Item_m1643017117_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_get_Item_m1643017117(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t893614575 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_get_Item_m1643017117_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void SortedDictionary_2_System_Collections_IDictionary_set_Item_m208457114_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IDictionary_set_Item_m208457114(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t893614575 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_System_Collections_IDictionary_set_Item_m208457114_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void SortedDictionary_2_System_Collections_ICollection_CopyTo_m943240533_gshared (SortedDictionary_2_t893614575 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_CopyTo_m943240533(__this, ___array0, ___index1, method) ((  void (*) (SortedDictionary_2_t893614575 *, Il2CppArray *, int32_t, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_CopyTo_m943240533_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m3410106573_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m3410106573(__this, method) ((  bool (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_IsSynchronized_m3410106573_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m4291614277_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m4291614277(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_System_Collections_ICollection_get_SyncRoot_m4291614277_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1530534042_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1530534042(__this, method) ((  Il2CppObject * (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1530534042_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4085059655_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4085059655(__this, method) ((  Il2CppObject* (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4085059655_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::get_Count()
extern "C"  int32_t SortedDictionary_2_get_Count_m3421650989_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_get_Count_m3421650989(__this, method) ((  int32_t (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_get_Count_m3421650989_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SortedDictionary_2_get_Item_m3202656644_gshared (SortedDictionary_2_t893614575 * __this, float ___key0, const MethodInfo* method);
#define SortedDictionary_2_get_Item_m3202656644(__this, ___key0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t893614575 *, float, const MethodInfo*))SortedDictionary_2_get_Item_m3202656644_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::set_Item(TKey,TValue)
extern "C"  void SortedDictionary_2_set_Item_m455198517_gshared (SortedDictionary_2_t893614575 * __this, float ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_set_Item_m455198517(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t893614575 *, float, Il2CppObject *, const MethodInfo*))SortedDictionary_2_set_Item_m455198517_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::Add(TKey,TValue)
extern "C"  void SortedDictionary_2_Add_m224572588_gshared (SortedDictionary_2_t893614575 * __this, float ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SortedDictionary_2_Add_m224572588(__this, ___key0, ___value1, method) ((  void (*) (SortedDictionary_2_t893614575 *, float, Il2CppObject *, const MethodInfo*))SortedDictionary_2_Add_m224572588_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::Clear()
extern "C"  void SortedDictionary_2_Clear_m1406209816_gshared (SortedDictionary_2_t893614575 * __this, const MethodInfo* method);
#define SortedDictionary_2_Clear_m1406209816(__this, method) ((  void (*) (SortedDictionary_2_t893614575 *, const MethodInfo*))SortedDictionary_2_Clear_m1406209816_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SortedDictionary_2_CopyTo_m3355616249_gshared (SortedDictionary_2_t893614575 * __this, KeyValuePair_2U5BU5D_t3257983789* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SortedDictionary_2_CopyTo_m3355616249(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SortedDictionary_2_t893614575 *, KeyValuePair_2U5BU5D_t3257983789*, int32_t, const MethodInfo*))SortedDictionary_2_CopyTo_m3355616249_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::Remove(TKey)
extern "C"  bool SortedDictionary_2_Remove_m1650665524_gshared (SortedDictionary_2_t893614575 * __this, float ___key0, const MethodInfo* method);
#define SortedDictionary_2_Remove_m1650665524(__this, ___key0, method) ((  bool (*) (SortedDictionary_2_t893614575 *, float, const MethodInfo*))SortedDictionary_2_Remove_m1650665524_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SortedDictionary_2_TryGetValue_m247917393_gshared (SortedDictionary_2_t893614575 * __this, float ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SortedDictionary_2_TryGetValue_m247917393(__this, ___key0, ___value1, method) ((  bool (*) (SortedDictionary_2_t893614575 *, float, Il2CppObject **, const MethodInfo*))SortedDictionary_2_TryGetValue_m247917393_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::ToKey(System.Object)
extern "C"  float SortedDictionary_2_ToKey_m3085476615_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SortedDictionary_2_ToKey_m3085476615(__this, ___key0, method) ((  float (*) (SortedDictionary_2_t893614575 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToKey_m3085476615_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.SortedDictionary`2<System.Single,System.Object>::ToValue(System.Object)
extern "C"  Il2CppObject * SortedDictionary_2_ToValue_m1263474151_gshared (SortedDictionary_2_t893614575 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SortedDictionary_2_ToValue_m1263474151(__this, ___value0, method) ((  Il2CppObject * (*) (SortedDictionary_2_t893614575 *, Il2CppObject *, const MethodInfo*))SortedDictionary_2_ToValue_m1263474151_gshared)(__this, ___value0, method)
