﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SplineController
struct SplineController_t2578011787;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartEffects
struct  StartEffects_t4276436398  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject StartEffects::warp
	GameObject_t1756533147 * ___warp_2;
	// SplineController StartEffects::splineController
	SplineController_t2578011787 * ___splineController_3;

public:
	inline static int32_t get_offset_of_warp_2() { return static_cast<int32_t>(offsetof(StartEffects_t4276436398, ___warp_2)); }
	inline GameObject_t1756533147 * get_warp_2() const { return ___warp_2; }
	inline GameObject_t1756533147 ** get_address_of_warp_2() { return &___warp_2; }
	inline void set_warp_2(GameObject_t1756533147 * value)
	{
		___warp_2 = value;
		Il2CppCodeGenWriteBarrier(&___warp_2, value);
	}

	inline static int32_t get_offset_of_splineController_3() { return static_cast<int32_t>(offsetof(StartEffects_t4276436398, ___splineController_3)); }
	inline SplineController_t2578011787 * get_splineController_3() const { return ___splineController_3; }
	inline SplineController_t2578011787 ** get_address_of_splineController_3() { return &___splineController_3; }
	inline void set_splineController_3(SplineController_t2578011787 * value)
	{
		___splineController_3 = value;
		Il2CppCodeGenWriteBarrier(&___splineController_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
