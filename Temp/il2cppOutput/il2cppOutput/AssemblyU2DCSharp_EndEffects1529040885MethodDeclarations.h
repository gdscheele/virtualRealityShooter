﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EndEffects
struct EndEffects_t1529040885;

#include "codegen/il2cpp-codegen.h"

// System.Void EndEffects::.ctor()
extern "C"  void EndEffects__ctor_m727265900 (EndEffects_t1529040885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EndEffects::Start()
extern "C"  void EndEffects_Start_m3650498740 (EndEffects_t1529040885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EndEffects::Update()
extern "C"  void EndEffects_Update_m1907002429 (EndEffects_t1529040885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
