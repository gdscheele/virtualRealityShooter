﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointController
struct PointController_t705534758;

#include "codegen/il2cpp-codegen.h"

// System.Void PointController::.ctor()
extern "C"  void PointController__ctor_m2036480003 (PointController_t705534758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointController::Start()
extern "C"  void PointController_Start_m3100225495 (PointController_t705534758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointController::Update()
extern "C"  void PointController_Update_m1349391472 (PointController_t705534758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PointController::addPoint()
extern "C"  void PointController_addPoint_m2212753670 (PointController_t705534758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PointController::getTotalPoints()
extern "C"  int32_t PointController_getTotalPoints_m342237230 (PointController_t705534758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
