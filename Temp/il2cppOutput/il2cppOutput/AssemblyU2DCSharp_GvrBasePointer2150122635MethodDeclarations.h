﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GvrBasePointer
struct GvrBasePointer_t2150122635;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"

// System.Void GvrBasePointer::.ctor()
extern "C"  void GvrBasePointer__ctor_m677865000 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GvrBasePointer::Start()
extern "C"  void GvrBasePointer_Start_m2039571436 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform GvrBasePointer::GetPointerTransform()
extern "C"  Transform_t3275118058 * GvrBasePointer_GetPointerTransform_m3271288187 (GvrBasePointer_t2150122635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
