﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CollisionDetector
struct CollisionDetector_t933686602;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void CollisionDetector::.ctor()
extern "C"  void CollisionDetector__ctor_m1987305471 (CollisionDetector_t933686602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CollisionDetector::Start()
extern "C"  void CollisionDetector_Start_m3784443323 (CollisionDetector_t933686602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CollisionDetector::Update()
extern "C"  void CollisionDetector_Update_m3858456404 (CollisionDetector_t933686602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CollisionDetector::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void CollisionDetector_OnCollisionEnter_m2894015013 (CollisionDetector_t933686602 * __this, Collision_t2876846408 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
