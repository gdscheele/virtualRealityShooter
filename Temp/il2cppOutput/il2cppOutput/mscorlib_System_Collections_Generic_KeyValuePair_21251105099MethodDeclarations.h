﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m817990433(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1251105099 *, float, List_1_t2644239190 *, const MethodInfo*))KeyValuePair_2__ctor_m303434200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::get_Key()
#define KeyValuePair_2_get_Key_m2746953336(__this, method) ((  float (*) (KeyValuePair_2_t1251105099 *, const MethodInfo*))KeyValuePair_2_get_Key_m4249127398_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2909115656(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1251105099 *, float, const MethodInfo*))KeyValuePair_2_set_Key_m448823159_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::get_Value()
#define KeyValuePair_2_get_Value_m864429715(__this, method) ((  List_1_t2644239190 * (*) (KeyValuePair_2_t1251105099 *, const MethodInfo*))KeyValuePair_2_get_Value_m1014938790_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m688445176(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1251105099 *, List_1_t2644239190 *, const MethodInfo*))KeyValuePair_2_set_Value_m1070856647_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Single,System.Collections.Generic.List`1<UnityEngine.Transform>>::ToString()
#define KeyValuePair_2_ToString_m761175938(__this, method) ((  String_t* (*) (KeyValuePair_2_t1251105099 *, const MethodInfo*))KeyValuePair_2_ToString_m4220912861_gshared)(__this, method)
