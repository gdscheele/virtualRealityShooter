﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<SplineInterpolator/SplineNode>
struct List_1_t1509007384;
// System.String
struct String_t;
// OnEndCallback
struct OnEndCallback_t2203472397;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_eEndPointsMode2085807520.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineInterpolator
struct  SplineInterpolator_t4279526764  : public MonoBehaviour_t1158329972
{
public:
	// eEndPointsMode SplineInterpolator::mEndPointsMode
	int32_t ___mEndPointsMode_2;
	// System.Collections.Generic.List`1<SplineInterpolator/SplineNode> SplineInterpolator::mNodes
	List_1_t1509007384 * ___mNodes_3;
	// System.String SplineInterpolator::mState
	String_t* ___mState_4;
	// System.Boolean SplineInterpolator::mRotations
	bool ___mRotations_5;
	// OnEndCallback SplineInterpolator::mOnEndCallback
	OnEndCallback_t2203472397 * ___mOnEndCallback_6;
	// System.Single SplineInterpolator::mCurrentTime
	float ___mCurrentTime_7;
	// System.Int32 SplineInterpolator::mCurrentIdx
	int32_t ___mCurrentIdx_8;

public:
	inline static int32_t get_offset_of_mEndPointsMode_2() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mEndPointsMode_2)); }
	inline int32_t get_mEndPointsMode_2() const { return ___mEndPointsMode_2; }
	inline int32_t* get_address_of_mEndPointsMode_2() { return &___mEndPointsMode_2; }
	inline void set_mEndPointsMode_2(int32_t value)
	{
		___mEndPointsMode_2 = value;
	}

	inline static int32_t get_offset_of_mNodes_3() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mNodes_3)); }
	inline List_1_t1509007384 * get_mNodes_3() const { return ___mNodes_3; }
	inline List_1_t1509007384 ** get_address_of_mNodes_3() { return &___mNodes_3; }
	inline void set_mNodes_3(List_1_t1509007384 * value)
	{
		___mNodes_3 = value;
		Il2CppCodeGenWriteBarrier(&___mNodes_3, value);
	}

	inline static int32_t get_offset_of_mState_4() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mState_4)); }
	inline String_t* get_mState_4() const { return ___mState_4; }
	inline String_t** get_address_of_mState_4() { return &___mState_4; }
	inline void set_mState_4(String_t* value)
	{
		___mState_4 = value;
		Il2CppCodeGenWriteBarrier(&___mState_4, value);
	}

	inline static int32_t get_offset_of_mRotations_5() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mRotations_5)); }
	inline bool get_mRotations_5() const { return ___mRotations_5; }
	inline bool* get_address_of_mRotations_5() { return &___mRotations_5; }
	inline void set_mRotations_5(bool value)
	{
		___mRotations_5 = value;
	}

	inline static int32_t get_offset_of_mOnEndCallback_6() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mOnEndCallback_6)); }
	inline OnEndCallback_t2203472397 * get_mOnEndCallback_6() const { return ___mOnEndCallback_6; }
	inline OnEndCallback_t2203472397 ** get_address_of_mOnEndCallback_6() { return &___mOnEndCallback_6; }
	inline void set_mOnEndCallback_6(OnEndCallback_t2203472397 * value)
	{
		___mOnEndCallback_6 = value;
		Il2CppCodeGenWriteBarrier(&___mOnEndCallback_6, value);
	}

	inline static int32_t get_offset_of_mCurrentTime_7() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mCurrentTime_7)); }
	inline float get_mCurrentTime_7() const { return ___mCurrentTime_7; }
	inline float* get_address_of_mCurrentTime_7() { return &___mCurrentTime_7; }
	inline void set_mCurrentTime_7(float value)
	{
		___mCurrentTime_7 = value;
	}

	inline static int32_t get_offset_of_mCurrentIdx_8() { return static_cast<int32_t>(offsetof(SplineInterpolator_t4279526764, ___mCurrentIdx_8)); }
	inline int32_t get_mCurrentIdx_8() const { return ___mCurrentIdx_8; }
	inline int32_t* get_address_of_mCurrentIdx_8() { return &___mCurrentIdx_8; }
	inline void set_mCurrentIdx_8(int32_t value)
	{
		___mCurrentIdx_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
