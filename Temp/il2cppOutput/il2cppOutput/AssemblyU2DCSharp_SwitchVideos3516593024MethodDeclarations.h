﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwitchVideos
struct SwitchVideos_t3516593024;

#include "codegen/il2cpp-codegen.h"

// System.Void SwitchVideos::.ctor()
extern "C"  void SwitchVideos__ctor_m349987595 (SwitchVideos_t3516593024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos::Awake()
extern "C"  void SwitchVideos_Awake_m2235960080 (SwitchVideos_t3516593024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos::ShowMainMenu()
extern "C"  void SwitchVideos_ShowMainMenu_m3410239584 (SwitchVideos_t3516593024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos::OnFlatLocal()
extern "C"  void SwitchVideos_OnFlatLocal_m622637550 (SwitchVideos_t3516593024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos::OnDash()
extern "C"  void SwitchVideos_OnDash_m3873506270 (SwitchVideos_t3516593024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos::On360Video()
extern "C"  void SwitchVideos_On360Video_m2809663580 (SwitchVideos_t3516593024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchVideos::ShowSample(System.Int32)
extern "C"  void SwitchVideos_ShowSample_m1156836627 (SwitchVideos_t3516593024 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
