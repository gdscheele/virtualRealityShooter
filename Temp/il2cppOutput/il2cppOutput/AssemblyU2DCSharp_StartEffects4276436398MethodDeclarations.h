﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartEffects
struct StartEffects_t4276436398;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void StartEffects::.ctor()
extern "C"  void StartEffects__ctor_m3641480147 (StartEffects_t4276436398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartEffects::Start()
extern "C"  void StartEffects_Start_m1655512143 (StartEffects_t4276436398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartEffects::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void StartEffects_OnCollisionEnter_m3317840289 (StartEffects_t4276436398 * __this, Collision_t2876846408 * ___col0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
