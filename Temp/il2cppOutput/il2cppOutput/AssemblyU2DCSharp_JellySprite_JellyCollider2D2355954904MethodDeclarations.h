﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySprite/JellyCollider2D
struct JellyCollider2D_t2355954904;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "AssemblyU2DCSharp_JellySpriteReferencePoint291100992.h"

// System.Void JellySprite/JellyCollider2D::.ctor()
extern "C"  void JellyCollider2D__ctor_m1750273905 (JellyCollider2D_t2355954904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D JellySprite/JellyCollider2D::get_Collider2D()
extern "C"  Collider2D_t646061738 * JellyCollider2D_get_Collider2D_m4133065612 (JellyCollider2D_t2355954904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollider2D::set_Collider2D(UnityEngine.Collider2D)
extern "C"  void JellyCollider2D_set_Collider2D_m3900720827 (JellyCollider2D_t2355954904 * __this, Collider2D_t646061738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JellySpriteReferencePoint JellySprite/JellyCollider2D::get_ReferencePoint()
extern "C"  JellySpriteReferencePoint_t291100992 * JellyCollider2D_get_ReferencePoint_m3616813848 (JellyCollider2D_t2355954904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollider2D::set_ReferencePoint(JellySpriteReferencePoint)
extern "C"  void JellyCollider2D_set_ReferencePoint_m2235258807 (JellyCollider2D_t2355954904 * __this, JellySpriteReferencePoint_t291100992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
