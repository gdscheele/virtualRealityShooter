﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiledPage/<CalculateTilesByDistance>c__AnonStorey0
struct  U3CCalculateTilesByDistanceU3Ec__AnonStorey0_t2727860256  : public Il2CppObject
{
public:
	// System.Single TiledPage/<CalculateTilesByDistance>c__AnonStorey0::distanceFromLeft
	float ___distanceFromLeft_0;

public:
	inline static int32_t get_offset_of_distanceFromLeft_0() { return static_cast<int32_t>(offsetof(U3CCalculateTilesByDistanceU3Ec__AnonStorey0_t2727860256, ___distanceFromLeft_0)); }
	inline float get_distanceFromLeft_0() const { return ___distanceFromLeft_0; }
	inline float* get_address_of_distanceFromLeft_0() { return &___distanceFromLeft_0; }
	inline void set_distanceFromLeft_0(float value)
	{
		___distanceFromLeft_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
