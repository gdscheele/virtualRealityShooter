﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>
struct Node_t3770910803;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21296315204.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>::.ctor(TKey)
extern "C"  void Node__ctor_m2098402281_gshared (Node_t3770910803 * __this, float ___key0, const MethodInfo* method);
#define Node__ctor_m2098402281(__this, ___key0, method) ((  void (*) (Node_t3770910803 *, float, const MethodInfo*))Node__ctor_m2098402281_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>::.ctor(TKey,TValue)
extern "C"  void Node__ctor_m2443083872_gshared (Node_t3770910803 * __this, float ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Node__ctor_m2443083872(__this, ___key0, ___value1, method) ((  void (*) (Node_t3770910803 *, float, Il2CppObject *, const MethodInfo*))Node__ctor_m2443083872_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>::SwapValue(System.Collections.Generic.RBTree/Node)
extern "C"  void Node_SwapValue_m3768093148_gshared (Node_t3770910803 * __this, Node_t2499136326 * ___other0, const MethodInfo* method);
#define Node_SwapValue_m3768093148(__this, ___other0, method) ((  void (*) (Node_t3770910803 *, Node_t2499136326 *, const MethodInfo*))Node_SwapValue_m3768093148_gshared)(__this, ___other0, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>::AsKV()
extern "C"  KeyValuePair_2_t1296315204  Node_AsKV_m3302788359_gshared (Node_t3770910803 * __this, const MethodInfo* method);
#define Node_AsKV_m3302788359(__this, method) ((  KeyValuePair_2_t1296315204  (*) (Node_t3770910803 *, const MethodInfo*))Node_AsKV_m3302788359_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Node<System.Single,System.Object>::AsDE()
extern "C"  DictionaryEntry_t3048875398  Node_AsDE_m957383724_gshared (Node_t3770910803 * __this, const MethodInfo* method);
#define Node_AsDE_m957383724(__this, method) ((  DictionaryEntry_t3048875398  (*) (Node_t3770910803 *, const MethodInfo*))Node_AsDE_m957383724_gshared)(__this, method)
