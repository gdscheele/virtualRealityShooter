﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GazeInputModule
struct GazeInputModule_t197612175;

#include "codegen/il2cpp-codegen.h"

// System.Void GazeInputModule::.ctor()
extern "C"  void GazeInputModule__ctor_m3495712250 (GazeInputModule_t197612175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
