﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// pointsScript
struct pointsScript_t3201420336;

#include "codegen/il2cpp-codegen.h"

// System.Void pointsScript::.ctor()
extern "C"  void pointsScript__ctor_m2837125787 (pointsScript_t3201420336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pointsScript::Start()
extern "C"  void pointsScript_Start_m1317282031 (pointsScript_t3201420336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pointsScript::Update()
extern "C"  void pointsScript_Update_m2734718194 (pointsScript_t3201420336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
