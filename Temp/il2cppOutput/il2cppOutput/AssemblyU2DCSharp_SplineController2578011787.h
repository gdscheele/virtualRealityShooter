﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// SplineInterpolator
struct SplineInterpolator_t4279526764;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// System.Converter`2<UnityEngine.Component,UnityEngine.Transform>
struct Converter_2_t1214379198;
// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t241889613;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_eOrientationMode1001868672.h"
#include "AssemblyU2DCSharp_eWrapMode2466495092.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineController
struct  SplineController_t2578011787  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject SplineController::SplineRoot
	GameObject_t1756533147 * ___SplineRoot_2;
	// System.Single SplineController::Duration
	float ___Duration_3;
	// eOrientationMode SplineController::OrientationMode
	int32_t ___OrientationMode_4;
	// eWrapMode SplineController::WrapMode
	int32_t ___WrapMode_5;
	// System.Boolean SplineController::AutoStart
	bool ___AutoStart_6;
	// System.Boolean SplineController::AutoClose
	bool ___AutoClose_7;
	// System.Boolean SplineController::HideOnExecute
	bool ___HideOnExecute_8;
	// SplineInterpolator SplineController::mSplineInterp
	SplineInterpolator_t4279526764 * ___mSplineInterp_9;
	// UnityEngine.Transform[] SplineController::mTransforms
	TransformU5BU5D_t3764228911* ___mTransforms_10;

public:
	inline static int32_t get_offset_of_SplineRoot_2() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___SplineRoot_2)); }
	inline GameObject_t1756533147 * get_SplineRoot_2() const { return ___SplineRoot_2; }
	inline GameObject_t1756533147 ** get_address_of_SplineRoot_2() { return &___SplineRoot_2; }
	inline void set_SplineRoot_2(GameObject_t1756533147 * value)
	{
		___SplineRoot_2 = value;
		Il2CppCodeGenWriteBarrier(&___SplineRoot_2, value);
	}

	inline static int32_t get_offset_of_Duration_3() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___Duration_3)); }
	inline float get_Duration_3() const { return ___Duration_3; }
	inline float* get_address_of_Duration_3() { return &___Duration_3; }
	inline void set_Duration_3(float value)
	{
		___Duration_3 = value;
	}

	inline static int32_t get_offset_of_OrientationMode_4() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___OrientationMode_4)); }
	inline int32_t get_OrientationMode_4() const { return ___OrientationMode_4; }
	inline int32_t* get_address_of_OrientationMode_4() { return &___OrientationMode_4; }
	inline void set_OrientationMode_4(int32_t value)
	{
		___OrientationMode_4 = value;
	}

	inline static int32_t get_offset_of_WrapMode_5() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___WrapMode_5)); }
	inline int32_t get_WrapMode_5() const { return ___WrapMode_5; }
	inline int32_t* get_address_of_WrapMode_5() { return &___WrapMode_5; }
	inline void set_WrapMode_5(int32_t value)
	{
		___WrapMode_5 = value;
	}

	inline static int32_t get_offset_of_AutoStart_6() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___AutoStart_6)); }
	inline bool get_AutoStart_6() const { return ___AutoStart_6; }
	inline bool* get_address_of_AutoStart_6() { return &___AutoStart_6; }
	inline void set_AutoStart_6(bool value)
	{
		___AutoStart_6 = value;
	}

	inline static int32_t get_offset_of_AutoClose_7() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___AutoClose_7)); }
	inline bool get_AutoClose_7() const { return ___AutoClose_7; }
	inline bool* get_address_of_AutoClose_7() { return &___AutoClose_7; }
	inline void set_AutoClose_7(bool value)
	{
		___AutoClose_7 = value;
	}

	inline static int32_t get_offset_of_HideOnExecute_8() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___HideOnExecute_8)); }
	inline bool get_HideOnExecute_8() const { return ___HideOnExecute_8; }
	inline bool* get_address_of_HideOnExecute_8() { return &___HideOnExecute_8; }
	inline void set_HideOnExecute_8(bool value)
	{
		___HideOnExecute_8 = value;
	}

	inline static int32_t get_offset_of_mSplineInterp_9() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___mSplineInterp_9)); }
	inline SplineInterpolator_t4279526764 * get_mSplineInterp_9() const { return ___mSplineInterp_9; }
	inline SplineInterpolator_t4279526764 ** get_address_of_mSplineInterp_9() { return &___mSplineInterp_9; }
	inline void set_mSplineInterp_9(SplineInterpolator_t4279526764 * value)
	{
		___mSplineInterp_9 = value;
		Il2CppCodeGenWriteBarrier(&___mSplineInterp_9, value);
	}

	inline static int32_t get_offset_of_mTransforms_10() { return static_cast<int32_t>(offsetof(SplineController_t2578011787, ___mTransforms_10)); }
	inline TransformU5BU5D_t3764228911* get_mTransforms_10() const { return ___mTransforms_10; }
	inline TransformU5BU5D_t3764228911** get_address_of_mTransforms_10() { return &___mTransforms_10; }
	inline void set_mTransforms_10(TransformU5BU5D_t3764228911* value)
	{
		___mTransforms_10 = value;
		Il2CppCodeGenWriteBarrier(&___mTransforms_10, value);
	}
};

struct SplineController_t2578011787_StaticFields
{
public:
	// System.Converter`2<UnityEngine.Component,UnityEngine.Transform> SplineController::<>f__am$cache0
	Converter_2_t1214379198 * ___U3CU3Ef__amU24cache0_11;
	// System.Comparison`1<UnityEngine.Transform> SplineController::<>f__am$cache1
	Comparison_1_t241889613 * ___U3CU3Ef__amU24cache1_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(SplineController_t2578011787_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Converter_2_t1214379198 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Converter_2_t1214379198 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Converter_2_t1214379198 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(SplineController_t2578011787_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Comparison_1_t241889613 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Comparison_1_t241889613 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Comparison_1_t241889613 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
