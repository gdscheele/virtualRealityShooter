﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TiledPage
struct TiledPage_t4183784445;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"

// System.Void TiledPage::.ctor()
extern "C"  void TiledPage__ctor_m855545878 (TiledPage_t4183784445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiledPage::FlushLayoutCache()
extern "C"  void TiledPage_FlushLayoutCache_m824305338 (TiledPage_t4183784445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiledPage::ApplyScrollEffect(System.Single,System.Single,System.Boolean)
extern "C"  void TiledPage_ApplyScrollEffect_m61883629 (TiledPage_t4183784445 * __this, float ___scrollDistance0, float ___scrollSpacing1, bool ___isInteractable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiledPage::UpdateTile(UnityEngine.Transform,UnityEngine.Vector3,System.Boolean)
extern "C"  void TiledPage_UpdateTile_m1973689176 (TiledPage_t4183784445 * __this, Transform_t3275118058 * ___tile0, Vector3_t2243707580  ___position1, bool ___isInteractable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiledPage::CalculateTilesByDistance()
extern "C"  void TiledPage_CalculateTilesByDistance_m2576735839 (TiledPage_t4183784445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TiledPage::GetTilePoint(UnityEngine.RectTransform)
extern "C"  Vector3_t2243707580  TiledPage_GetTilePoint_m362629319 (TiledPage_t4183784445 * __this, RectTransform_t3349966182 * ___tileRect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform TiledPage::GetTileCell(UnityEngine.Transform)
extern "C"  RectTransform_t3349966182 * TiledPage_GetTileCell_m1549467871 (TiledPage_t4183784445 * __this, Transform_t3275118058 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
