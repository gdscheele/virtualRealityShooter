﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// JellySprite/JellyCollision
struct JellyCollision_t1959843398;
// JellySprite/JellyCollision2D
struct JellyCollision2D_t2954473184;
// JellySprite/JellyCollider
struct JellyCollider_t1548403138;
// JellySprite/JellyCollider2D
struct JellyCollider2D_t2355954904;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JellySpriteReferencePoint
struct  JellySpriteReferencePoint_t291100992  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject JellySpriteReferencePoint::<ParentJellySprite>k__BackingField
	GameObject_t1756533147 * ___U3CParentJellySpriteU3Ek__BackingField_2;
	// System.Int32 JellySpriteReferencePoint::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_3;
	// JellySprite/JellyCollision JellySpriteReferencePoint::m_JellyCollision
	JellyCollision_t1959843398 * ___m_JellyCollision_4;
	// JellySprite/JellyCollision2D JellySpriteReferencePoint::m_JellyCollision2D
	JellyCollision2D_t2954473184 * ___m_JellyCollision2D_5;
	// JellySprite/JellyCollider JellySpriteReferencePoint::m_JellyCollider
	JellyCollider_t1548403138 * ___m_JellyCollider_6;
	// JellySprite/JellyCollider2D JellySpriteReferencePoint::m_JellyCollider2D
	JellyCollider2D_t2355954904 * ___m_JellyCollider2D_7;
	// System.Boolean JellySpriteReferencePoint::m_SendCollisionMessages
	bool ___m_SendCollisionMessages_8;

public:
	inline static int32_t get_offset_of_U3CParentJellySpriteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___U3CParentJellySpriteU3Ek__BackingField_2)); }
	inline GameObject_t1756533147 * get_U3CParentJellySpriteU3Ek__BackingField_2() const { return ___U3CParentJellySpriteU3Ek__BackingField_2; }
	inline GameObject_t1756533147 ** get_address_of_U3CParentJellySpriteU3Ek__BackingField_2() { return &___U3CParentJellySpriteU3Ek__BackingField_2; }
	inline void set_U3CParentJellySpriteU3Ek__BackingField_2(GameObject_t1756533147 * value)
	{
		___U3CParentJellySpriteU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CParentJellySpriteU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___U3CIndexU3Ek__BackingField_3)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_3() const { return ___U3CIndexU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_3() { return &___U3CIndexU3Ek__BackingField_3; }
	inline void set_U3CIndexU3Ek__BackingField_3(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_JellyCollision_4() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___m_JellyCollision_4)); }
	inline JellyCollision_t1959843398 * get_m_JellyCollision_4() const { return ___m_JellyCollision_4; }
	inline JellyCollision_t1959843398 ** get_address_of_m_JellyCollision_4() { return &___m_JellyCollision_4; }
	inline void set_m_JellyCollision_4(JellyCollision_t1959843398 * value)
	{
		___m_JellyCollision_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_JellyCollision_4, value);
	}

	inline static int32_t get_offset_of_m_JellyCollision2D_5() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___m_JellyCollision2D_5)); }
	inline JellyCollision2D_t2954473184 * get_m_JellyCollision2D_5() const { return ___m_JellyCollision2D_5; }
	inline JellyCollision2D_t2954473184 ** get_address_of_m_JellyCollision2D_5() { return &___m_JellyCollision2D_5; }
	inline void set_m_JellyCollision2D_5(JellyCollision2D_t2954473184 * value)
	{
		___m_JellyCollision2D_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_JellyCollision2D_5, value);
	}

	inline static int32_t get_offset_of_m_JellyCollider_6() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___m_JellyCollider_6)); }
	inline JellyCollider_t1548403138 * get_m_JellyCollider_6() const { return ___m_JellyCollider_6; }
	inline JellyCollider_t1548403138 ** get_address_of_m_JellyCollider_6() { return &___m_JellyCollider_6; }
	inline void set_m_JellyCollider_6(JellyCollider_t1548403138 * value)
	{
		___m_JellyCollider_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_JellyCollider_6, value);
	}

	inline static int32_t get_offset_of_m_JellyCollider2D_7() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___m_JellyCollider2D_7)); }
	inline JellyCollider2D_t2355954904 * get_m_JellyCollider2D_7() const { return ___m_JellyCollider2D_7; }
	inline JellyCollider2D_t2355954904 ** get_address_of_m_JellyCollider2D_7() { return &___m_JellyCollider2D_7; }
	inline void set_m_JellyCollider2D_7(JellyCollider2D_t2355954904 * value)
	{
		___m_JellyCollider2D_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_JellyCollider2D_7, value);
	}

	inline static int32_t get_offset_of_m_SendCollisionMessages_8() { return static_cast<int32_t>(offsetof(JellySpriteReferencePoint_t291100992, ___m_SendCollisionMessages_8)); }
	inline bool get_m_SendCollisionMessages_8() const { return ___m_SendCollisionMessages_8; }
	inline bool* get_address_of_m_SendCollisionMessages_8() { return &___m_SendCollisionMessages_8; }
	inline void set_m_SendCollisionMessages_8(bool value)
	{
		___m_SendCollisionMessages_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
