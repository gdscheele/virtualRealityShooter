﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellySprite/JellyCollision2D
struct JellyCollision2D_t2954473184;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// JellySpriteReferencePoint
struct JellySpriteReferencePoint_t291100992;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "AssemblyU2DCSharp_JellySpriteReferencePoint291100992.h"

// System.Void JellySprite/JellyCollision2D::.ctor()
extern "C"  void JellyCollision2D__ctor_m3000928629 (JellyCollision2D_t2954473184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collision2D JellySprite/JellyCollision2D::get_Collision2D()
extern "C"  Collision2D_t1539500754 * JellyCollision2D_get_Collision2D_m2285837174 (JellyCollision2D_t2954473184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollision2D::set_Collision2D(UnityEngine.Collision2D)
extern "C"  void JellyCollision2D_set_Collision2D_m1751724995 (JellyCollision2D_t2954473184 * __this, Collision2D_t1539500754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JellySpriteReferencePoint JellySprite/JellyCollision2D::get_ReferencePoint()
extern "C"  JellySpriteReferencePoint_t291100992 * JellyCollision2D_get_ReferencePoint_m2025265034 (JellyCollision2D_t2954473184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellySprite/JellyCollision2D::set_ReferencePoint(JellySpriteReferencePoint)
extern "C"  void JellyCollision2D_set_ReferencePoint_m3237861003 (JellyCollision2D_t2954473184 * __this, JellySpriteReferencePoint_t291100992 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
