﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JellyCameraFollow
struct JellyCameraFollow_t4240209918;

#include "codegen/il2cpp-codegen.h"

// System.Void JellyCameraFollow::.ctor()
extern "C"  void JellyCameraFollow__ctor_m1697006137 (JellyCameraFollow_t4240209918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JellyCameraFollow::Update()
extern "C"  void JellyCameraFollow_Update_m4028540860 (JellyCameraFollow_t4240209918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
