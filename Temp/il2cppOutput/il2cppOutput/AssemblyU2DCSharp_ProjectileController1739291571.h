﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileController
struct  ProjectileController_t1739291571  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ProjectileController::projectile
	GameObject_t1756533147 * ___projectile_2;
	// System.Single ProjectileController::projectileSpeed
	float ___projectileSpeed_3;
	// System.Single ProjectileController::timeOfLastProjectile
	float ___timeOfLastProjectile_4;
	// System.Int32 ProjectileController::timeUntilDestruction
	int32_t ___timeUntilDestruction_5;

public:
	inline static int32_t get_offset_of_projectile_2() { return static_cast<int32_t>(offsetof(ProjectileController_t1739291571, ___projectile_2)); }
	inline GameObject_t1756533147 * get_projectile_2() const { return ___projectile_2; }
	inline GameObject_t1756533147 ** get_address_of_projectile_2() { return &___projectile_2; }
	inline void set_projectile_2(GameObject_t1756533147 * value)
	{
		___projectile_2 = value;
		Il2CppCodeGenWriteBarrier(&___projectile_2, value);
	}

	inline static int32_t get_offset_of_projectileSpeed_3() { return static_cast<int32_t>(offsetof(ProjectileController_t1739291571, ___projectileSpeed_3)); }
	inline float get_projectileSpeed_3() const { return ___projectileSpeed_3; }
	inline float* get_address_of_projectileSpeed_3() { return &___projectileSpeed_3; }
	inline void set_projectileSpeed_3(float value)
	{
		___projectileSpeed_3 = value;
	}

	inline static int32_t get_offset_of_timeOfLastProjectile_4() { return static_cast<int32_t>(offsetof(ProjectileController_t1739291571, ___timeOfLastProjectile_4)); }
	inline float get_timeOfLastProjectile_4() const { return ___timeOfLastProjectile_4; }
	inline float* get_address_of_timeOfLastProjectile_4() { return &___timeOfLastProjectile_4; }
	inline void set_timeOfLastProjectile_4(float value)
	{
		___timeOfLastProjectile_4 = value;
	}

	inline static int32_t get_offset_of_timeUntilDestruction_5() { return static_cast<int32_t>(offsetof(ProjectileController_t1739291571, ___timeUntilDestruction_5)); }
	inline int32_t get_timeUntilDestruction_5() const { return ___timeUntilDestruction_5; }
	inline int32_t* get_address_of_timeUntilDestruction_5() { return &___timeUntilDestruction_5; }
	inline void set_timeUntilDestruction_5(int32_t value)
	{
		___timeUntilDestruction_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
